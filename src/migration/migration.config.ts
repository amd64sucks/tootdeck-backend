import { DataSource } from 'typeorm';

import { getConfig } from './datasource.config';

import checkEnv from '../env/check';

checkEnv();
const datasource = new DataSource(getConfig());
datasource.initialize();
export default datasource;
