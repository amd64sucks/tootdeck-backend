import { DataSourceOptions } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

export function getConfig() {
	return {
		type: 'postgres',
		host: process.env['PSQL_HOST'],
		port: +(process.env['PSQL_PORT'] ?? 0),
		username: process.env['PSQL_USERNAME'],
		password: process.env['PSQL_PASSWORD'],
		database: process.env['PSQL_DATABASE'],
		synchronize: false,
		namingStrategy: new SnakeNamingStrategy(),
		migrations: [__dirname + '/../../migrations/*.ts'],
		entities: [__dirname + '/../**/*.entity.ts'],
	} as DataSourceOptions;
}
