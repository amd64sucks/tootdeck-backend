import { Test, TestingModule } from '@nestjs/testing';

import { UserCacheService } from './user.cache.service';
import { DBApplicationService } from '../../database/services/database.application.service';
import { DBAccountService } from '../../database/services/database.account.service';
import { DBUserService } from '../../database/services/database.user.service';

import { UserStore } from '../../database/entities/user.entity';

import { UserCache } from '../types/cache.types';
import { InstanceType } from '../../database/types';
import { FullAppModule } from '../../app/app.module';

describe('UserCacheService', () => {
	let service: UserCacheService;
	let ApplicationDB: DBApplicationService;
	let AccountDB: DBAccountService;
	let UserDB: DBUserService;
	let user_1: UserStore;
	let user_2: UserStore;
	let entry_1: UserCache;
	let entry_2: UserCache;

	const test_domain_1 = 'UserCacheService1'.toLocaleLowerCase();
	const test_username_1 = 'UserCacheService1'.toLocaleLowerCase();
	const test_username_2 = 'UserCacheService2'.toLocaleLowerCase();

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		service = app.get(UserCacheService);
		ApplicationDB = app.get(DBApplicationService);
		AccountDB = app.get(DBAccountService);
		UserDB = app.get(DBUserService);

		await ApplicationDB.create(test_domain_1, InstanceType.Mastodon, 'key', 'secret');

		const account_1 = await AccountDB.create(test_username_1, test_domain_1, 'token');
		const account_2 = await AccountDB.create(test_username_2, test_domain_1, 'token');
		user_1 = (await UserDB.create(account_1!))!;
		user_2 = (await UserDB.create(account_2!))!;
	});

	afterAll(async () => {
		const account_1 = await AccountDB.get(test_username_1, test_domain_1);
		if (account_1) {
			await UserDB.delete(account_1);
			await AccountDB.delete(test_username_1, test_domain_1);
		}

		const account_2 = await AccountDB.get(test_username_2, test_domain_1);
		if (account_2) {
			await UserDB.delete(account_2);
			await AccountDB.delete(test_username_2, test_domain_1);
		}

		await ApplicationDB.delete(test_domain_1);
	});

	describe('create', () => {
		it('creating valid entry, should return true', async () => {
			const result = await service.create(user_1.uuid);
			if (!result) {
				expect(!!result).toBe(true);
				return;
			}

			expect(result).toMatchObject({
				uuid: user_1.uuid,
				main: {
					username: test_username_1,
					application: {
						domain: test_domain_1,
						type: InstanceType.Mastodon,
					},
				},
				secondary: [],
			});

			entry_1 = result;
		});

		it('creating valid entry, should return true', async () => {
			const result = await service.create(user_2);
			if (!result) {
				expect(!!result).toBe(true);
				return;
			}

			expect(result).toMatchObject({
				uuid: user_2.uuid,
				main: {
					username: test_username_2,
					application: {
						domain: test_domain_1,
						type: InstanceType.Mastodon,
					},
				},
				secondary: [],
			});

			entry_2 = result;

			expect(!!(await service.delete(user_2.uuid))).toBe(true);
		});

		it('creating invalid entry, should return null', async () => {
			const result = await service.create('nothing');

			expect(result).toBe(null);
		});
	});

	describe('get', () => {
		it('getting valid uuid, should return entry', async () => {
			expect(await service.get(user_1.uuid)).toMatchObject(entry_1);
		});

		it('getting non existing uuid, should return entry', async () => {
			expect(await service.get(user_2.uuid)).toMatchObject(entry_2);
		});

		it('getting invalid uuid, should return null', async () => {
			expect(await service.get('nothing')).toBe(null);
		});
	});

	describe('delete', () => {
		it('deleting valid entry, should return true', async () => {
			expect(await service.delete(user_1.uuid)).toBe(true);
			expect(await service.delete(user_2.uuid)).toBe(true);
		});

		it('deleting invalid entry, should return false', async () => {
			expect(await service.delete('nothing')).toBe(false);
		});
	});
});
