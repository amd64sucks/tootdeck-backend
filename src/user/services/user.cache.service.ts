import { InjectRedis } from '@liaoliaots/nestjs-redis';
import { Injectable, forwardRef } from '@nestjs/common';
import { Redis } from 'ioredis';
import { deserialize, serialize } from 'v8';

import { DBUserService } from '../../database/services/database.user.service';

import { AccountStore } from '../../database/entities/account.entity';
import { UserStore } from '../../database/entities/user.entity';

import { Logger } from '../../utils/logger';

import { RedisNamespace } from '../../app/redis.config';
import { UserCache } from '../types/cache.types';

@Injectable()
export class UserCacheService {
	private readonly logger = new Logger(UserCacheService.name);

	constructor(
		@InjectRedis(RedisNamespace.UserCache) private readonly redis: Redis,
		private readonly DBUserService: DBUserService,
	) {}

	/**
	 * Utils
	 */
	//#region

	private obfuscateAccount(acc: AccountStore) {
		const { type, domain } = acc.application;
		const application = { type, domain };

		const { username, token } = acc;
		const account = { username, token };

		return { ...account, application };
	}

	private obfuscateData(data: UserStore) {
		return {
			uuid: data.uuid,
			main: this.obfuscateAccount(data.main),
			secondary: data.secondary?.map(this.obfuscateAccount) ?? [],
		};
	}

	private async seralize(user_uuid: string, data: UserCache): Promise<UserCache | null> {
		try {
			const result = await this.redis.set(user_uuid, serialize(data), 'EX', 60 * 60);
			if (!result) {
				return null;
			}
			return data;
		} catch (e) {
			this.logger.error(`seralize`, e);
			return null;
		}
	}

	private async deseralize(user_uuid: string): Promise<UserCache | null> {
		try {
			const buffer = await this.redis.getBuffer(user_uuid);
			if (!buffer) {
				return null;
			}
			return deserialize(buffer);
		} catch (e) {
			this.logger.error(`deseralize`, e);
			return null;
		}
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	async get(user_uuid: string): Promise<UserCache | null> {
		const session = await this.deseralize(user_uuid);
		if (!session) {
			return this.create(user_uuid);
		}

		return session;
	}

	async create(user_uuid: string): Promise<UserCache | null>;
	async create(user: UserStore): Promise<UserCache | null>;
	async create(user_uuid_or_store: string | UserStore): Promise<UserCache | null> {
		let user: UserStore | null = null;
		if (user_uuid_or_store instanceof UserStore) {
			user = user_uuid_or_store;
		} else if (typeof user_uuid_or_store === 'string') {
			user = await this.DBUserService.getByUUID(user_uuid_or_store);
		}
		if (!user) {
			return null;
		}

		const obfuscated = this.obfuscateData(user);

		return this.seralize(user.uuid, obfuscated);
	}

	async update(user: UserStore): Promise<UserCache | null> {
		const session = await this.deseralize(user.uuid);
		if (!session) {
			return null;
		}

		const obfuscated = this.obfuscateData(user);

		return this.seralize(user.uuid, obfuscated);
	}

	async delete(user_uuid: string): Promise<boolean> {
		try {
			return !!(await this.redis.del(user_uuid));
		} catch (e) {
			this.logger.error(`delete`, e);
			return false;
		}
	}
	//#endregion
}
