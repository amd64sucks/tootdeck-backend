import { Test, TestingModule } from '@nestjs/testing';

import { DBApplicationService } from '../../database/services/database.application.service';
import { DBAccountService } from '../../database/services/database.account.service';
import { DBUserService } from '../../database/services/database.user.service';
import { UserDeleteService } from './user.delete.service';
import { UserService } from './user.service';

import { InstanceType } from '../../database/types';
import { UserDeleteSession } from '../../auth/types/sessions.types';
import { FullAppModule } from '../../app/app.module';

describe('UserDeleteService', () => {
	let service: UserDeleteService;
	let userService: UserService;
	let ApplicationDB: DBApplicationService;
	let AccountDB: DBAccountService;
	let UserDB: DBUserService;
	let result_uuid: string;
	let result_session: UserDeleteSession;

	const test_domain_1 = 'AccountService1'.toLocaleLowerCase();
	const test_domain_2 = 'AccountService2'.toLocaleLowerCase();
	const test_username_1 = 'AccountService1'.toLocaleLowerCase();
	const test_username_2 = 'AccountService2'.toLocaleLowerCase();

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		service = app.get(UserDeleteService);
		userService = app.get(UserService);
		ApplicationDB = app.get(DBApplicationService);
		AccountDB = app.get(DBAccountService);
		UserDB = app.get(DBUserService);

		await ApplicationDB.create(test_domain_1, InstanceType.Mastodon, 'key', 'secret');
		await ApplicationDB.create(test_domain_2, InstanceType.Mastodon, 'key', 'secret');

		const account_1 = await AccountDB.create(test_username_1, test_domain_1, 'token');
		const account_2 = await AccountDB.create(test_username_2, test_domain_1, 'token');
		const account_3 = await AccountDB.create('test_username_3', test_domain_1, 'token');
		const account_4 = await AccountDB.create('test_username_4', test_domain_1, 'token');
		const account_5 = await AccountDB.create('test_username_5', test_domain_1, 'token');
		const user_1 = await UserDB.create(account_1!);
		await UserDB.addAccount(account_1!, account_2!);
		await UserDB.addAccount(account_1!, account_3!);
		await UserDB.addAccount(account_1!, account_4!);
		await UserDB.addAccount(account_1!, account_5!);

		result_uuid = user_1!.uuid;
	});

	afterAll(async () => {
		const account = await AccountDB.get(test_username_1, test_domain_1);
		if (account) {
			await UserDB.delete(account);
			await AccountDB.delete(test_username_1, test_domain_1);
			await AccountDB.delete(test_username_2, test_domain_1);
			await AccountDB.delete('test_username_3', test_domain_1);
			await AccountDB.delete('test_username_4', test_domain_1);
			await AccountDB.delete('test_username_5', test_domain_1);
		}
		await ApplicationDB.delete(test_domain_1);
		await ApplicationDB.delete(test_domain_2);
	});

	describe('createRequest', () => {
		const TryCatch = async (user_uuid: string) => {
			let result: any;
			let has_throw: boolean = false;

			try {
				result = await service.createRequest(user_uuid);
			} catch (e) {
				has_throw = true;
			}

			return { result, has_throw };
		};

		it('create request, should return entry', async () => {
			const { result, has_throw } = await TryCatch(result_uuid);

			expect(result).toMatchObject({
				user_uuid: result_uuid,
			} as UserDeleteSession);
			expect(!!result.code).toBe(true);
			expect(has_throw).toBe(false);

			result_session = result;
		});
	});

	describe('validate', () => {
		const TryCatch = async (raw_token: string) => {
			let result: any;
			let has_throw: boolean = false;

			try {
				result = await service.validate(raw_token);
			} catch (e) {
				has_throw = true;
			}

			return { result, has_throw };
		};

		it('valid token, should return entry', async () => {
			const { result, has_throw } = await TryCatch(result_session.token);

			expect(result).toMatchObject(result_session);
			expect(has_throw).toBe(false);
		});

		it('invalid token, should throw', async () => {
			const { result, has_throw } = await TryCatch('');

			expect(has_throw).toBe(true);
		});
	});

	describe('delete', () => {
		const TryCatch = async (user_uuid: string) => {
			let result: any;
			let has_throw: boolean = false;

			try {
				result = await service.delete(user_uuid);
			} catch (e) {
				has_throw = true;
			}

			return { result, has_throw };
		};

		it('delete valid user, should return entry', async () => {
			const { result, has_throw } = await TryCatch(result_session.user_uuid);

			expect(result).toMatchObject({
				user: 'OK',
				token: 'OK',
			});
			expect(has_throw).toBe(false);
		});

		it('delete invalid user, should throw', async () => {
			const { result, has_throw } = await TryCatch('invalid');

			expect(has_throw).toBe(true);
		});
	});
});
