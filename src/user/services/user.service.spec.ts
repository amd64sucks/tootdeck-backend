import { Test, TestingModule } from '@nestjs/testing';
import { randomUUID } from 'crypto';
import { isUUID } from 'class-validator';

import { UserService } from './user.service';
import { DBApplicationService } from '../../database/services/database.application.service';
import { DBAccountService } from '../../database/services/database.account.service';
import { DBUserService } from '../../database/services/database.user.service';

import { AuthGetResponse } from '../../auth/properties/auth.get.property';

import { InstanceType } from '../../database/types';
import { OAuthSessionToken } from '../../auth/types/oauth.types';
import { FullAppModule } from '../../app/app.module';

describe('UserService', () => {
	let service: UserService;
	let ApplicationDB: DBApplicationService;
	let AccountDB: DBAccountService;
	let UserDB: DBUserService;
	let user_uuid: string;
	let account_uuid: string;

	const test_domain_1 = 'AccountService1'.toLocaleLowerCase();
	const test_domain_2 = 'AccountService2'.toLocaleLowerCase();
	const test_username_1 = 'AccountService1'.toLocaleLowerCase();
	const test_username_2 = 'AccountService2'.toLocaleLowerCase();

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		service = app.get(UserService);
		ApplicationDB = app.get(DBApplicationService);
		AccountDB = app.get(DBAccountService);
		UserDB = app.get(DBUserService);
		await ApplicationDB.create(test_domain_1, InstanceType.Mastodon, 'key', 'secret');
		await ApplicationDB.create(test_domain_2, InstanceType.Mastodon, 'key', 'secret');
	});

	afterAll(async () => {
		const account = await AccountDB.get(test_username_1, test_domain_1);
		if (account) {
			await UserDB.delete(account);
			await AccountDB.delete(test_username_1, test_domain_1);
			await AccountDB.delete(test_username_1, test_domain_2);
		}
		await ApplicationDB.delete(test_domain_1);
		await ApplicationDB.delete(test_domain_2);
	});

	describe('login', () => {
		const TryCatch = async (session_token: OAuthSessionToken) => {
			let result: any;
			let has_throw: boolean = false;

			try {
				result = await service.login(session_token);
			} catch (e) {
				has_throw = true;
			}

			return { result, has_throw };
		};

		it('login valid user, should return uuid', async () => {
			const { result, has_throw } = await TryCatch({
				username: test_username_1,
				domain: test_domain_1,
				token: 'token',
			});

			expect(isUUID(result)).toBe(true);
			expect(has_throw).toBe(false);
		});

		it('login not existing acccount, should return uuid', async () => {
			const { result, has_throw } = await TryCatch({
				username: test_username_2,
				domain: test_domain_2,
				token: 'token',
			});

			expect(isUUID(result)).toBe(true);
			expect(has_throw).toBe(false);
		});

		it('login not existing user, should return uuid', async () => {
			const account = await AccountDB.get(test_username_1, test_domain_1);
			if (!account) {
				expect(!!account).toBe(true);
				return;
			}
			expect(!!(await UserDB.delete(account))).toBe(true);

			const { result, has_throw } = await TryCatch({
				username: test_username_1,
				domain: test_domain_1,
				token: 'token',
			});

			expect(isUUID(result)).toBe(true);
			expect(has_throw).toBe(false);

			user_uuid = result;
		});
	});

	describe('add', () => {
		const TryCatch = async (session: OAuthSessionToken, user_uuid: string) => {
			let result: any;
			let has_throw: boolean = false;

			try {
				result = await service.add(session, user_uuid);
			} catch (e) {
				has_throw = true;
			}

			return { result, has_throw };
		};

		it('add valid unexisting account, should return uuid', async () => {
			const { result, has_throw } = await TryCatch(
				{
					username: test_username_1,
					domain: test_domain_2,
					token: '',
				},
				user_uuid,
			);

			expect(result).toBe(user_uuid);
			expect(has_throw).toBe(false);
		});

		it('add valid existing account, should return uuid', async () => {
			const { result, has_throw } = await TryCatch(
				{
					username: test_username_2,
					domain: test_domain_2,
					token: 'token',
				},
				user_uuid,
			);

			expect(result).toBe(user_uuid);
			expect(has_throw).toBe(false);
		});

		it('add same account, should throw', async () => {
			const { result, has_throw } = await TryCatch(
				{
					username: test_username_2,
					domain: test_domain_2,
					token: 'token',
				},
				user_uuid,
			);

			expect(has_throw).toBe(true);
		});

		it('add main account, should throw', async () => {
			const { result, has_throw } = await TryCatch(
				{
					username: test_username_1,
					domain: test_domain_1,
					token: 'token',
				},
				user_uuid,
			);

			expect(has_throw).toBe(true);
		});
	});

	describe('refresh', () => {
		const TryCatch = async (session: OAuthSessionToken, user_uuid: string) => {
			let result: any;
			let has_throw: boolean = false;

			try {
				result = await service.refresh(session, user_uuid);
			} catch (e) {
				has_throw = true;
			}

			return { result, has_throw };
		};

		it('Refresh valid account, should return uuid', async () => {
			const user = await UserDB.getByUUID(user_uuid);
			if (!user) {
				expect(!!user).toBe(true);
				return;
			}

			account_uuid = user.secondary[0]?.uuid!;

			const { result, has_throw } = await TryCatch(
				{
					username: test_username_1,
					domain: test_domain_2,
					token: 'token',
				},
				account_uuid,
			);

			expect(result).toBe(account_uuid);
			expect(has_throw).toBe(false);
		});

		it('Refresh invalid account, should throw', async () => {
			const { result, has_throw } = await TryCatch(
				{
					username: test_username_1,
					domain: test_domain_2,
					token: 'token',
				},
				'token',
			);

			expect(has_throw).toBe(true);
		});
	});

	describe('get', () => {
		const TryCatch = async (user_uuid: string) => {
			let result: any;
			let has_throw: boolean = false;
			try {
				result = await service.get(user_uuid);
			} catch (e) {
				has_throw = true;
			}

			return {
				result,
				has_throw,
			};
		};

		it('get valid user, should return entry', async () => {
			const { result, has_throw } = await TryCatch(user_uuid);

			expect(result).toMatchObject({
				main: {
					username: test_username_1,
					domain: test_domain_1,
				},
				secondary: [
					{
						username: test_username_1,
						domain: test_domain_2,
						valid: true,
					},
					{
						username: test_username_2,
						domain: test_domain_2,
						valid: true,
					},
				],
			} as AuthGetResponse);
			expect(has_throw).toBe(false);
		});

		it('get valid user, should return entry', async () => {
			const { result, has_throw } = await TryCatch(randomUUID());

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});
	});
});
