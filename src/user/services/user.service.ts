import {
	BadRequestException,
	Injectable,
	InternalServerErrorException,
	NotFoundException,
} from '@nestjs/common';

import { AccountStore } from '../../database/entities/account.entity';

import { DBAccountService } from '../../database/services/database.account.service';
import { DBUserService } from '../../database/services/database.user.service';
import { UserCacheService } from './user.cache.service';
import { WsService } from '../../websocket/ws.service';

import { Logger } from '../../utils/logger';

import { AuthGetResponse } from '../../auth/properties/auth.get.property';

import { OAuthSessionToken } from '../../auth/types/oauth.types';
import { ApiResponseError } from '../../auth/types/api.types';
import { DeleteUser } from '../../database/types';
import { InstanceHandle } from '../../utils/handle.types';
import { ResponseType, WebsocketAPI } from '../../websocket/types';

@Injectable()
export class UserService {
	private readonly logger = new Logger(UserService.name);

	constructor(
		private readonly DBAccountService: DBAccountService,
		private readonly DBUserService: DBUserService,
		private readonly UserCacheService: UserCacheService,
		private readonly WsService: WsService,
	) {}

	/**
	 * Utils
	 */
	//#region

	/**
	 * @throws InternalServerErrorException
	 */
	private async createAll(session_token: OAuthSessionToken) {
		const account = await this.DBAccountService.create(
			session_token.username,
			session_token.domain,
			session_token.token,
		);
		if (!account) {
			this.logger.error('create', 'Unable to create account');
			throw new InternalServerErrorException();
		}

		const user = await this.DBUserService.create(account);
		if (!user) {
			this.logger.error('create', 'Unable to create user');
			throw new InternalServerErrorException();
		}

		return user.uuid;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	private async create(account: AccountStore) {
		const user = await this.DBUserService.create(account);
		if (!user) {
			this.logger.error('create', 'Unable to create user');
			throw new InternalServerErrorException();
		}

		return user.uuid;
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	/**
	 * @throws InternalServerErrorException
	 */
	async get(user_uuid: string): Promise<AuthGetResponse> {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('get', 'Unable to get existing user');
			throw new InternalServerErrorException();
		}

		const main = {
			username: user.main.username,
			domain: user.main.application.domain,
			type: user.main.application.type,
			valid: user.main.token !== '',
		};

		const secondary = [];
		for (const account of user.secondary) {
			secondary.push({
				username: account.username,
				domain: account.application.domain,
				type: account.application.type,
				valid: account.token !== '',
			});
		}

		return {
			main,
			secondary,
		};
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async login(session_token: OAuthSessionToken) {
		const account = await this.DBAccountService.get(
			session_token.username,
			session_token.domain,
		);
		if (!account) {
			return this.createAll(session_token);
		}

		const user = await this.DBUserService.get(account);
		if (!user) {
			return this.create(account);
		}

		// Dispatch info to connected user
		this.WsService.send(user.uuid, {
			type: ResponseType.API,
			data: WebsocketAPI.ResponseData.NewSession,
		});

		return user.uuid;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async add(session: OAuthSessionToken, user_uuid: string) {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('add', 'Unable to get existing user');
			throw new InternalServerErrorException();
		}

		if (
			(user.main.username === session.username &&
				user.main.application.domain === session.domain) ||
			user.secondary.find(
				(x) => x.username === session.username && x.application.domain === session.domain,
			)
		) {
			throw new BadRequestException(ApiResponseError.AccountAlreadyExist);
		}

		let account: AccountStore;
		const exist = await this.DBAccountService.get(session.username, session.domain);
		if (exist) {
			account = exist;
		} else {
			const created = await this.DBAccountService.create(
				session.username,
				session.domain,
				session.token,
			);
			if (!created) {
				this.logger.error('add', 'Unable to create account');
				throw new InternalServerErrorException();
			}

			account = created;
		}

		const updated = await this.DBUserService.addAccount(user, account);
		if (!updated) {
			this.logger.error('add', 'Unable to update user');
			throw new InternalServerErrorException();
		}

		const update_cache = await this.UserCacheService.create(updated);
		if (!update_cache) {
			this.logger.error('add', 'Unable to set user cache');
			throw new InternalServerErrorException();
		}

		return updated.uuid;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async refresh(session: OAuthSessionToken, account_uuid: string) {
		const account = await this.DBAccountService.getByUUID(account_uuid);
		if (!account) {
			this.logger.error('refresh', 'Unable to get account');
			throw new InternalServerErrorException();
		}

		const updated = await this.DBAccountService.update(
			account.username,
			account.application.domain,
			session.token,
		);
		if (!updated) {
			this.logger.error('refresh', 'Unable to refresh account');
			throw new InternalServerErrorException();
		}

		return updated.uuid;
	}

	/**
	 * @throws InternalServerErrorException
	 * @throws BadRequestException
	 */
	async removeAccount(user_uuid: string, handle_to_remove: InstanceHandle) {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('removeAccount', 'Unable to get user');
			throw new InternalServerErrorException();
		}

		const user_to_remove = user.secondary.find(
			(x) =>
				x.username === handle_to_remove.username &&
				x.application.domain === handle_to_remove.domain,
		);
		if (!user_to_remove) {
			throw new BadRequestException();
		}

		const removed = await this.DBUserService.deleteAccount(user.main, user_to_remove);
		switch (removed) {
			case DeleteUser.True:
				return;
			case DeleteUser.False:
				throw new BadRequestException();
			case DeleteUser.RemoveAccount:
				const del = await this.DBAccountService.delete(
					user_to_remove.username,
					user_to_remove.application.domain,
				);
				if (!del) {
					throw new InternalServerErrorException();
				}
		}
	}

	/**
	 * @throws InternalServerErrorException
	 * @throws NotFoundException
	 */
	async getConfig(user_uuid: string) {
		const user = await this.DBUserService.getConfig(user_uuid);
		if (!user) {
			this.logger.error('getConfig', 'Unable to get user');
			throw new InternalServerErrorException();
		}

		if (!user.config) {
			throw new NotFoundException();
		}

		const { uuid, ...config } = user.config;

		return config;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async addConfig(user_uuid: string, config: string, client_uuid?: string) {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('addConfig', 'Unable to get user');
			throw new InternalServerErrorException();
		}

		const ret = await this.DBUserService.addConfig(user, config);
		if (ret === null) {
			throw new InternalServerErrorException();
		}

		this.WsService.send(user.uuid, {
			type: ResponseType.API,
			data: WebsocketAPI.ResponseData.ConfigUpdate,
			uuid: client_uuid,
		} as any);
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async deleteConfig(user_uuid: string) {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('deleteConfig', 'Unable to get user');
			throw new InternalServerErrorException();
		}

		const ret = await this.DBUserService.deleteConfig(user);
		if (ret === null) {
			throw new InternalServerErrorException();
		}

		return !!ret;
	}

	//#endregion
}
