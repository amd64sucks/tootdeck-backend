import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CacheBody {
	@ApiPropertyOptional()
	uuid: string;

	@ApiProperty()
	config: string;
}
