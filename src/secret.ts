import { webcrypto } from 'crypto';
import { existsSync, statSync, writeFileSync } from 'fs';

import { Logger } from './utils/logger';

export async function generateSecret(crash_test?: {
	regen?: boolean;
	exist?: boolean;
	write?: boolean;
}) {
	const logger = new Logger('generateSecret');

	const filename = process.env['NODE_ENV'] === 'test' ? 'test.pem' : 'secret.pem';

	if (!crash_test?.regen) {
		try {
			if (crash_test?.exist) {
				throw new Error();
			}
			if (existsSync('crypto/' + filename) && statSync('crypto/' + filename).size) {
				return true;
			}
		} catch (e) {
			logger.error('Unable to determine if secret already exist', e);
			return false;
		}
	}

	const key = await webcrypto.subtle
		.generateKey(
			{
				name: 'RSA-PSS',
				modulusLength: 4096,
				publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
				hash: { name: 'SHA-512' },
			},
			true,
			['sign'],
		)
		.then(async (k) => await webcrypto.subtle.exportKey('pkcs8', k.privateKey))
		.catch((e) => {
			logger.error('Unable to export private key', e);
			return null;
		});

	if (!key) {
		return false;
	}

	try {
		if (crash_test?.write) {
			throw new Error();
		}
		writeFileSync('crypto/' + filename, Buffer.from(key).toString('base64'), {
			encoding: 'utf8',
		});
		logger.log(null, 'Secret generated');
	} catch (e) {
		logger.error('Unable to write private key', e);
		return false;
	}

	return true;
}
