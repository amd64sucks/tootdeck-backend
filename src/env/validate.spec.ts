import ValidateEnv from './validate';

function test_check(env: ValidateEnv, env_name: string, type: any, value: string, result: any) {
	it((value || '""') + ' should return ' + result, () => {
		process.env[env_name] = value;
		expect(env.check(env_name, type)).toBe(result);
	});
}

describe('Validate Env', () => {
	const env = new ValidateEnv();

	describe('error state', () => {
		it('should return ' + false, () => {
			expect(env.getErrorState()).toBe(false);
			expect(env.printErrorMessage()).toBe(false);
		});

		it('should return ' + true, () => {
			env.check('NOEXIST', 0);
			expect(env.getErrorState()).toBe(true);
			expect(env.printErrorMessage()).toBe(true);
		});
	});

	describe('fallback', () => {
		test_check(env, 'TEST', {}, 'value', false);
	});

	describe('exist', () => {
		test_check(env, 'EXIST', 'string', 'true', true);
		test_check(env, 'NOEXIST', 'string', '', false);
	});

	describe('NaN', () => {
		const test_env = 'NaN';

		test_check(env, test_env, 0, 'dsffsd', false);
		test_check(env, test_env, 0, '{}', false);
		test_check(env, test_env, 0, '-10d50', false);
		test_check(env, test_env, 0, '10', true);
		test_check(env, test_env, 0, '-10', true);
	});

	describe('boolean', () => {
		const test_env = 'BOOL';

		test_check(env, test_env, true, 'dsffsd', false);
		test_check(env, test_env, true, '{}', false);
		test_check(env, test_env, true, '-10d50', false);
		test_check(env, test_env, true, 'true', true);
		test_check(env, test_env, true, 'false', true);
	});

	describe('protocol', () => {
		const test_env = 'PROTOCOL';

		test_check(env, test_env, 'string', 'http', true);
		test_check(env, test_env, 'string', 'https', true);
		test_check(env, test_env, 'string', 'xml', false);
	});

	describe('port', () => {
		const test_env = 'PORT';

		test_check(env, test_env, 0, '-1', false);
		test_check(env, test_env, 0, '100000', false);
		test_check(env, test_env, 0, '', false);
		test_check(env, test_env, 0, '40000', true);
	});
});
