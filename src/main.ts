import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { WsAdapter } from '@nestjs/platform-ws';
import fastifyCookie from '@fastify/cookie';
import { contentParser } from 'fastify-multer';
import helmet from 'helmet';

import { AppModule } from './app/app.module';

import checkEnv from './env/check';
import { generateSecret } from './secret';

async function bootstrap() {
	checkEnv();

	const secret = await generateSecret();
	if (!secret) {
		process.exit(1);
	}

	const app = await NestFactory.create<NestFastifyApplication>(
		AppModule,
		new FastifyAdapter({ bodyLimit: 40 * 1024 * 1024 }),
	);

	app.enableCors();
	app.setGlobalPrefix('api');

	app.useWebSocketAdapter(new WsAdapter(app));

	if (!process.env['PROD']) {
		app.use(
			helmet({
				contentSecurityPolicy: {
					directives: {
						defaultSrc: [`'self'`],
						styleSrc: [`'self'`, `'unsafe-inline'`],
						imgSrc: [`'self'`, 'data:', 'validator.swagger.io'],
						scriptSrc: [`'self'`, `https: 'unsafe-inline'`],
					},
				},
			}),
		);

		const config = new DocumentBuilder()
			.setTitle('TootDeck API')
			.setVersion('0.9')
			.addSecurity('InstanceHandle', {
				type: 'http',
				in: 'header',
				name: 'Authorization',
				description: 'Instance handle',
				scheme: 'bearer',
			})
			.build();
		const document = SwaggerModule.createDocument(app, config);
		SwaggerModule.setup('api', app, document, {
			swaggerOptions: {
				tagsSorter: 'alpha',
			},
		});
	}

	app.use(helmet({ contentSecurityPolicy: { useDefaults: true } }));
	// @ts-ignore
	await app.register(fastifyCookie);
	// @ts-ignore
	await app.register(contentParser);

	await app.listen(3000, '0.0.0.0');
}

bootstrap();
