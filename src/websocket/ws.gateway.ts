import { Request } from '@nestjs/common';
import {
	ConnectedSocket,
	OnGatewayConnection,
	OnGatewayDisconnect,
	OnGatewayInit,
	WebSocketGateway,
	WebSocketServer,
} from '@nestjs/websockets';
import * as cookieParser from 'cookie-parser';
import { FastifyRequest } from 'fastify';
import { Server, WebSocket } from 'ws';

import { WsService } from './ws.service';
import { JWEService } from '../auth/services/jwe.service';
import { BrowserFingerprintService } from '../auth/services/utils/fingerprint.service';
import { UserCacheService } from '../user/services/user.cache.service';

import { Logger } from '../utils/logger';

import { Fingerprint } from '../auth/types/fingerprint.types';
import { JWEType } from '../auth/types/jwe.types';
import { JWESession } from '../auth/types/sessions.types';
import { ResponseType, WebSocketUser, WebsocketAPI } from './types';

@WebSocketGateway({
	path: '/api/streaming',
	cors: true,
})
export class WsGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
	private readonly logger = new Logger(WsGateway.name);

	@WebSocketServer()
	private readonly server: Server;

	constructor(
		private readonly BrowserFingerprint: BrowserFingerprintService,
		private readonly JWEService: JWEService,
		private readonly UserCacheService: UserCacheService,
		private readonly WsService: WsService,
	) {}

	/**
	 * Utils
	 */
	//#region

	private async validate(
		cookies: { [key: string]: string | undefined },
		fingerprint: Fingerprint,
	): Promise<JWESession | null> {
		if (!cookies['a'] || !cookies['r']) {
			return null;
		}

		const jwts = await Promise.all([
			this.JWEService.validate(JWEType.Access, cookies['a']!, fingerprint).catch((e) => null),
			this.JWEService.validate(JWEType.Refresh, cookies['r']!, fingerprint).catch(
				(e) => null,
			),
		]);

		return jwts[0] ?? null;
	}

	//#endregion

	async handleConnection(
		@ConnectedSocket() client: WebSocket,
		@Request() request: FastifyRequest,
	) {
		cookieParser()(request as any, undefined as any, () => {});
		// Get fingerprint
		const user_agent = request.headers['user-agent'];
		const ip = request.headers['x-real-ip'] as string | undefined;
		const fingerprint = this.BrowserFingerprint.get(user_agent, ip);

		const session = await this.validate(request.cookies, fingerprint);
		if (!session) {
			const response: WebsocketAPI.Response = {
				type: ResponseType.API,
				data: WebsocketAPI.ResponseData.Refresh,
			};
			client.send(JSON.stringify(response));
			client.close();
			return;
		}

		const user = await this.UserCacheService.get(session.user_uuid);
		if (!user) {
			this.logger.error('handleConnection', 'Failed to get user from cache');
			const response: WebsocketAPI.Response = {
				type: ResponseType.API,
				data: WebsocketAPI.ResponseData.InternalServerError,
			};
			client.send(JSON.stringify(response));
			client.close();
			return;
		}

		await this.WsService.handleConnect(user, client);

		this.logger.debug(
			'handleConnection',
			this.server.clients.size + ' currently connected clients.',
		);
	}

	handleDisconnect(client: WebSocketUser) {
		this.WsService.handleDisconnect(client);
		this.logger.debug(
			'handleDisconnect',
			this.server.clients.size + ' currently connected clients.',
		);
	}

	afterInit(server: Server) {
		this.logger.log('afterInit', 'Websocket is live');
	}
}
