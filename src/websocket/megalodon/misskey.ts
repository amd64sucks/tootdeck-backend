import MisskeyAPI from './misskey/api_client';
import { DEFAULT_UA } from './default';
import { MegalodonInterface, WebSocketInterface } from './megalodon';
import { Logger } from '../../utils/logger';

export default class Misskey implements MegalodonInterface {
	public client: MisskeyAPI.Interface;
	public baseUrl: string;

	/**
	 * @param baseUrl hostname or base URL
	 * @param accessToken access token from OAuth2 authorization
	 * @param userAgent UserAgent is specified in header on request.
	 */
	constructor(
		baseUrl: string,
		accessToken: string | null = null,
		userAgent: string | null = DEFAULT_UA,
	) {
		let token: string = '';
		if (accessToken) {
			token = accessToken;
		}
		let agent: string = DEFAULT_UA;
		if (userAgent) {
			agent = userAgent;
		}
		this.client = new MisskeyAPI.Client(baseUrl, token, agent);
		this.baseUrl = baseUrl;
	}

	public socket(logger: Logger): WebSocketInterface {
		return this.client.socket(logger);
	}
}
