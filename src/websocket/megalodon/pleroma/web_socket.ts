import { ClientOptions, Data, WebSocket as WS } from 'ws';
import { Dayjs } from 'dayjs';
const dayjs = require('dayjs');
import { EventEmitter } from 'events';

import { WebSocketInterface } from '../megalodon';
import PleromaAPI from './api_client';
import { UnknownNotificationTypeError } from 'megalodon/lib/src/notification';
import { Logger } from '../../../utils/logger';

/**
 * WebSocket
 * Pleroma is not support streaming. It is support websocket instead of streaming.
 * So this class connect to Phoenix websocket for Pleroma.
 */
export default class WebSocket extends EventEmitter implements WebSocketInterface {
	public url: string;
	public stream: string;
	public parser: Parser;
	public headers: { [key: string]: string };
	private _accessToken: string;
	private _reconnectInterval: number;
	private _reconnectMaxAttempts: number;
	private _reconnectCurrentAttempts: number;
	private _connectionClosed: boolean;
	private _client: WS | null;
	private _pongReceivedTimestamp: Dayjs;
	private _heartbeatInterval: number = 60000;
	private _pongWaiting: boolean = false;

	/**
	 * @param url Full url of websocket: e.g. https://pleroma.io/api/v1/streaming
	 * @param stream Stream name, please refer: https://git.pleroma.social/pleroma/pleroma/blob/develop/lib/pleroma/web/mastodon_api/mastodon_socket.ex#L19-28
	 * @param accessToken The access token.
	 * @param userAgent The specified User Agent.
	 */
	constructor(
		private readonly logger: Logger,
		url: string,
		stream: string,
		accessToken: string,
		userAgent: string,
	) {
		super();
		this.url = url;
		this.stream = stream;
		this.parser = new Parser();
		this.headers = {
			'User-Agent': userAgent,
		};
		this._accessToken = accessToken;
		this._reconnectInterval = 10000;
		this._reconnectMaxAttempts = Infinity;
		this._reconnectCurrentAttempts = 0;
		this._connectionClosed = false;
		this._client = null;
		this._pongReceivedTimestamp = dayjs();
	}

	/**
	 * Start websocket connection.
	 */
	public start() {
		this._connectionClosed = false;
		this._resetRetryParams();
		this._startWebSocketConnection();
	}

	/**
	 * Reset connection and start new websocket connection.
	 */
	private _startWebSocketConnection() {
		this._resetConnection();
		this._setupParser();
		this._client = this._connect(this.url, this._accessToken, this.headers);
		this._bindSocket(this._client);
	}

	/**
	 * Stop current connection.
	 */
	public stop() {
		this._connectionClosed = true;
		this._resetConnection();
		this._resetRetryParams();
	}

	/**
	 * Clean up current connection, and listeners.
	 */
	private _resetConnection() {
		if (this._client) {
			this._client.close(1000);
			this._client.removeAllListeners();
			this._client = null;
		}

		if (this.parser) {
			this.parser.removeAllListeners();
		}
	}

	/**
	 * Resets the parameters used in reconnect.
	 */
	private _resetRetryParams() {
		this._reconnectCurrentAttempts = 0;
	}

	/**
	 * Reconnects to the same endpoint.
	 */
	private _reconnect() {
		setTimeout(() => {
			// Skip reconnect when client is closed or when it's connecting.
			// https://github.com/websockets/ws/blob/7.2.1/lib/websocket.js#L365
			if (!this._client || (this._client && this._client.readyState === WS.CONNECTING)) {
				return;
			}

			if (this._reconnectCurrentAttempts < this._reconnectMaxAttempts) {
				this._reconnectCurrentAttempts++;
				this._clearBinding();
				if (this._client) {
					// In reconnect, we want to close the connection immediately,
					// because recoonect is necessary when some problems occur.
					this._client.terminate();
				}
				// Call connect methods
				this.logger.log('_reconnect', 'Reconnecting');
				this._client = this._connect(this.url, this._accessToken, this.headers);
				this._bindSocket(this._client);
			}
		}, this._reconnectInterval);
	}

	/**
	 * @param url Base url of streaming endpoint.
	 * @param stream The specified stream name.
	 * @param accessToken Access token.
	 * @param headers The specified headers.
	 * @return A WebSocket instance.
	 */
	private _connect(url: string, accessToken: string, headers: { [key: string]: string }): WS {
		const parameter: Array<string> = [];

		if (accessToken !== null) {
			parameter.push(`access_token=${accessToken}`);
		}
		const requestURL: string = `${url}/?${parameter.join('&')}`;
		let options: ClientOptions = {
			headers: headers,
		};

		const cli: WS = new WS(requestURL, options);
		return cli;
	}

	/**
	 * Clear binding event for web socket client.
	 */
	private _clearBinding() {
		if (this._client) {
			this._client.removeAllListeners('close');
			this._client.removeAllListeners('pong');
			this._client.removeAllListeners('open');
			this._client.removeAllListeners('message');
			this._client.removeAllListeners('error');
		}
	}

	/**
	 * Bind event for web socket client.
	 * @param client A WebSocket instance.
	 */
	private _bindSocket(client: WS) {
		client.on('close', (code: number, _reason: Buffer) => {
			// Refer the code: https://tools.ietf.org/html/rfc6455#section-7.4
			if (code === 1000) {
				this.emit('close', {});
			} else {
				this.logger.log('_bindSocket', `Closed connection with ${code}`);
				// If already called close method, it does not retry.
				if (!this._connectionClosed) {
					this._reconnect();
				}
			}
		});
		client.on('pong', () => {
			this._pongWaiting = false;
			this.emit('pong', {});
			this._pongReceivedTimestamp = dayjs();
			// It is required to anonymous function since get this scope in checkAlive.
			setTimeout(
				() => this._checkAlive(this._pongReceivedTimestamp),
				this._heartbeatInterval,
			);
		});
		client.on('open', () => {
			this.logger.log('_bindSocket', 'Connected');
			this.emit('connect', {});
			// Call first ping event.
			setTimeout(() => {
				client.ping('');
			}, 10000);
		});
		client.on('message', (data: Data, isBinary: boolean) => {
			this.parser.parse(data, isBinary);
		});
		client.on('error', (err: Error) => {
			this.emit('error', err);
		});
	}

	/**
	 * Set up parser when receive message.
	 */
	private _setupParser() {
		this.parser.on('update', ([stream, content, optional]) => {
			this.emit('update', {
				stream,
				content: PleromaAPI.Converter.status(content),
				...optional,
			});
		});
		this.parser.on('notification', ([stream, content, optional]) => {
			const n = PleromaAPI.Converter.notification(content);
			if (n instanceof UnknownNotificationTypeError) {
				this.logger.warn(
					'_setupParser',
					`Unknown notification event has received: ${content}`,
				);
			} else {
				this.emit('notification', {
					stream,
					content: n,
					...optional,
				});
			}
		});
		this.parser.on('delete', (id: string) => {
			this.emit('delete', id);
		});
		this.parser.on('conversation', ([stream, content, optional]) => {
			this.emit('conversation', {
				stream,
				content: PleromaAPI.Converter.conversation(content),
				...optional,
			});
		});
		this.parser.on('status_update', ([stream, content, optional]) => {
			this.emit('status_update', {
				stream,
				content: PleromaAPI.Converter.status(content),
				...optional,
			});
		});
		this.parser.on('error', (err: Error) => {
			this.emit('parser-error', err);
		});
		this.parser.on('heartbeat', (_) => {
			this.emit('heartbeat', 'heartbeat');
		});
	}

	/**
	 * Call ping and wait to pong.
	 */
	private _checkAlive(timestamp: Dayjs) {
		const now: Dayjs = dayjs();
		// Block multiple calling, if multiple pong event occur.
		// It the duration is less than interval, through ping.
		if (now.diff(timestamp) > this._heartbeatInterval - 1000 && !this._connectionClosed) {
			// Skip ping when client is connecting.
			// https://github.com/websockets/ws/blob/7.2.1/lib/websocket.js#L289
			if (this._client && this._client.readyState !== WS.CONNECTING) {
				this._pongWaiting = true;
				this._client.ping('');
				setTimeout(() => {
					if (this._pongWaiting) {
						this._pongWaiting = false;
						this._reconnect();
					}
				}, 10000);
			}
		}
	}

	send(data: string) {
		if (this._client?.readyState === this._client?.OPEN) {
			this._client?.send(data);
		}
	}
}

/**
 * Parser
 * This class provides parser for websocket message.
 */
export class Parser extends EventEmitter {
	/**
	 * @param message Message body of websocket.
	 */
	public parse(data: Data, isBinary: boolean) {
		const message = isBinary ? data : data.toString();
		if (typeof message !== 'string') {
			this.emit('heartbeat', {});
			return;
		}

		if (message === '') {
			this.emit('heartbeat', {});
			return;
		}

		let event = '';
		let payload = '';
		let stream = [];
		let optional = {};
		let mes = {};
		try {
			const obj = JSON.parse(message);
			event = obj.event;
			payload = obj.payload;
			stream = obj.stream;
			optional = {
				list: obj.list,
				tag: obj.tag,
			};
			mes = JSON.parse(payload);
		} catch (err) {
			// delete event does not have json object
			if (event !== 'delete') {
				this.emit(
					'error',
					new Error(`Error parsing websocket reply: ${message}, error message: ${err}`),
				);
				return;
			}
		}

		switch (event) {
			case 'update':
				this.emit('update', [stream, mes, optional]);
				break;
			case 'notification':
				this.emit('notification', [stream, mes, optional]);
				break;
			case 'conversation':
				this.emit('conversation', [stream, mes, optional]);
				break;
			case 'delete':
				this.emit('delete', payload);
				break;
			case 'status.update':
				this.emit('status_update', [stream, mes, optional]);
				break;
			default:
				this.emit('error', new Error(`Unknown event has received: ${message}`));
		}
	}
}
