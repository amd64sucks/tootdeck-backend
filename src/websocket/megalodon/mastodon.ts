import MastodonAPI from './mastodon/api_client';
import WebSocket from './mastodon/web_socket';
import { MegalodonInterface } from './megalodon';
import { DEFAULT_UA } from './default';
import { Logger } from '../../utils/logger';

export default class Mastodon implements MegalodonInterface {
	public client: MastodonAPI.Interface;
	public baseUrl: string;

	/**
	 * @param baseUrl hostname or base URL
	 * @param accessToken access token from OAuth2 authorization
	 * @param userAgent UserAgent is specified in header on request.
	 */
	constructor(
		baseUrl: string,
		accessToken: string | null = null,
		userAgent: string | null = DEFAULT_UA,
	) {
		let token: string = '';
		if (accessToken) {
			token = accessToken;
		}
		let agent: string = DEFAULT_UA;
		if (userAgent) {
			agent = userAgent;
		}
		this.client = new MastodonAPI.Client(baseUrl, token, agent);
		this.baseUrl = baseUrl;
	}

	// ======================================
	// WebSocket
	// ======================================
	public socket(logger: Logger): WebSocket {
		return this.client.socket(logger, '/api/v1/streaming', '');
	}
}
