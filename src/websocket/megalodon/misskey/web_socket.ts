import { ClientOptions, Data, WebSocket as WS } from 'ws';
import { Dayjs } from 'dayjs';
const dayjs = require('dayjs');
import { EventEmitter } from 'events';
import { WebSocketInterface } from '../megalodon';
import MisskeyAPI from './api_client';
import { UnknownNotificationTypeError } from '../notification';
import { Logger } from '../../../utils/logger';

/**
 * WebSocket
 * Misskey is not support http streaming. It supports websocket instead of streaming.
 * So this class connect to Misskey server with WebSocket.
 */
export default class WebSocket extends EventEmitter implements WebSocketInterface {
	public url: string;
	public parser: Parser;
	public headers: { [key: string]: string };
	public listId: string | null = null;
	private _accessToken: string;
	private _reconnectInterval: number;
	private _reconnectMaxAttempts: number;
	private _reconnectCurrentAttempts: number;
	private _connectionClosed: boolean;
	private _client: WS | null = null;
	private _pongReceivedTimestamp: Dayjs;
	private _heartbeatInterval: number = 60000;
	private _pongWaiting: boolean = false;

	/**
	 * @param url Full url of websocket: e.g. wss://misskey.io/streaming
	 * @param channel Channel name is user, localTimeline, hybridTimeline, globalTimeline, conversation or list.
	 * @param accessToken The access token.
	 * @param listId This parameter is required when you specify list as channel.
	 */
	constructor(
		private readonly logger: Logger,
		url: string,
		accessToken: string,
		userAgent: string,
	) {
		super();
		this.url = url;
		this.parser = new Parser();
		this.headers = {
			'User-Agent': userAgent,
		};
		this._accessToken = accessToken;
		this._reconnectInterval = 10000;
		this._reconnectMaxAttempts = Infinity;
		this._reconnectCurrentAttempts = 0;
		this._connectionClosed = false;
		this._pongReceivedTimestamp = dayjs();
	}

	/**
	 * Start websocket connection.
	 */
	public start() {
		this._connectionClosed = false;
		this._resetRetryParams();
		this._startWebSocketConnection();
	}

	/**
	 * Reset connection and start new websocket connection.
	 */
	private _startWebSocketConnection() {
		this._resetConnection();
		this._setupParser();
		this._client = this._connect();
		this._bindSocket(this._client);
	}

	/**
	 * Stop current connection.
	 */
	public stop() {
		this._connectionClosed = true;
		this._resetConnection();
		this._resetRetryParams();
	}

	/**
	 * Clean up current connection, and listeners.
	 */
	private _resetConnection() {
		if (this._client) {
			this._client.close(1000);
			this._client.removeAllListeners();
			this._client = null;
		}

		if (this.parser) {
			this.parser.removeAllListeners();
		}
	}

	/**
	 * Resets the parameters used in reconnect.
	 */
	private _resetRetryParams() {
		this._reconnectCurrentAttempts = 0;
	}

	/**
	 * Connect to the endpoint.
	 */
	private _connect(): WS {
		let options: ClientOptions = {
			headers: this.headers,
		};
		const cli: WS = new WS(`${this.url}?i=${this._accessToken}`, options);
		return cli;
	}

	/**
	 * Reconnects to the same endpoint.
	 */
	private _reconnect() {
		setTimeout(() => {
			// Skip reconnect when client is closed or when it's connecting.
			// https://github.com/websockets/ws/blob/7.2.1/lib/websocket.js#L365
			if (!this._client || (this._client && this._client.readyState === WS.CONNECTING)) {
				return;
			}

			if (this._reconnectCurrentAttempts < this._reconnectMaxAttempts) {
				this._reconnectCurrentAttempts++;
				this._clearBinding();
				if (this._client) {
					// In reconnect, we want to close the connection immediately,
					// because recoonect is necessary when some problems occur.
					this._client.terminate();
				}
				// Call connect methods
				this.logger.log('_reconnect', 'Reconnecting');
				this._client = this._connect();
				this._bindSocket(this._client);
			}
		}, this._reconnectInterval);
	}

	/**
	 * Clear binding event for websocket client.
	 */
	private _clearBinding() {
		if (this._client) {
			this._client.removeAllListeners('close');
			this._client.removeAllListeners('pong');
			this._client.removeAllListeners('open');
			this._client.removeAllListeners('message');
			this._client.removeAllListeners('error');
		}
	}

	/**
	 * Bind event for web socket client.
	 * @param client A WebSocket instance.
	 */
	private _bindSocket(client: WS) {
		client.on('close', (code: number, _reason: Buffer) => {
			if (code === 1000) {
				this.emit('close', {});
			} else {
				this.logger.log('_bindSocket', `Closed connection with ${code}`);
				if (!this._connectionClosed) {
					this._reconnect();
				}
			}
		});
		client.on('pong', () => {
			this._pongWaiting = false;
			this.emit('pong', {});
			this._pongReceivedTimestamp = dayjs();
			// It is required to anonymous function since get this scope in checkAlive.
			setTimeout(
				() => this._checkAlive(this._pongReceivedTimestamp),
				this._heartbeatInterval,
			);
		});
		client.on('open', () => {
			this.logger.log('_bindSocket', 'Connected');
			this.emit('connect', {});
			// Call first ping event.
			setTimeout(() => {
				client.ping('');
			}, 10000);
		});
		client.on('message', (data: Data, isBinary: boolean) => {
			this.parser.parse(data, isBinary);
		});
		client.on('error', (err: Error) => {
			this.emit('error', err);
		});
	}

	/**
	 * Set up parser when receive message.
	 */
	private _setupParser() {
		this.parser.on('update', ([id, note]) => {
			this.emit('update', { id, content: MisskeyAPI.Converter.note(note) });
		});
		this.parser.on('notification', ([id, notification]) => {
			const n = MisskeyAPI.Converter.notification(notification);
			if (n instanceof UnknownNotificationTypeError) {
				this.logger.warn(
					'_setupParser',
					`Unknown notification event has received: ${notification}`,
				);
			} else {
				this.emit('notification', { id, content: n });
			}
		});
		this.parser.on('conversation', ([id, note]) => {
			this.emit('conversation', {
				id,
				content: MisskeyAPI.Converter.noteToConversation(note),
			});
		});
		this.parser.on('error', (err: Error) => {
			this.emit('parser-error', err);
		});
	}

	/**
	 * Call ping and wait to pong.
	 */
	private _checkAlive(timestamp: Dayjs) {
		const now: Dayjs = dayjs();
		// Block multiple calling, if multiple pong event occur.
		// It the duration is less than interval, through ping.
		if (now.diff(timestamp) > this._heartbeatInterval - 1000 && !this._connectionClosed) {
			// Skip ping when client is connecting.
			// https://github.com/websockets/ws/blob/7.2.1/lib/websocket.js#L289
			if (this._client && this._client.readyState !== WS.CONNECTING) {
				this._pongWaiting = true;
				this._client.ping('');
				setTimeout(() => {
					if (this._pongWaiting) {
						this._pongWaiting = false;
						this._reconnect();
					}
				}, 10000);
			}
		}
	}

	send(data: string) {
		if (this._client?.readyState === this._client?.OPEN) {
			this._client?.send(data);
		}
	}
}

/**
 * Parser
 * This class provides parser for websocket message.
 */
export class Parser extends EventEmitter {
	/**
	 * @param message Message body of websocket.
	 */
	public parse(data: Data, isBinary: boolean) {
		const message = isBinary ? data : data.toString();
		if (typeof message !== 'string') {
			this.emit('heartbeat', {});
			return;
		}

		if (message === '') {
			this.emit('heartbeat', {});
			return;
		}

		let obj: {
			type: string;
			body: {
				id: string;
				type: string;
				body: any;
			};
		};
		let body: {
			id: string;
			type: string;
			body: any;
		};

		try {
			obj = JSON.parse(message);
			if (obj.type !== 'channel') {
				return;
			}
			if (!obj.body) {
				return;
			}
			body = obj.body;
		} catch (err) {
			this.emit(
				'error',
				new Error(`Error parsing websocket reply: ${message}, error message: ${err}`),
			);
			return;
		}

		switch (body.type) {
			case 'note':
				this.emit('update', [body.id, body.body]);
				break;
			case 'notification':
				this.emit('notification', [body.id, body.body]);
				break;
			case 'mention': {
				const note = body.body as MisskeyAPI.Entity.Note;
				if (note.visibility === 'specified') {
					this.emit('conversation', [body.id, note]);
				}
				break;
			}
			// When renote and followed event, the same notification will be received.
			case 'renote':
			case 'followed':
			case 'follow':
			case 'unfollow':
			case 'receiveFollowRequest':
			case 'meUpdated':
			case 'readAllNotifications':
			case 'readAllUnreadSpecifiedNotes':
			case 'readAllAntennas':
			case 'readAllUnreadMentions':
			case 'unreadNotification':
				// Ignore these events
				break;
			default:
				this.emit(
					'error',
					new Error(`Unknown event has received: ${JSON.stringify(body)}`),
				);
				break;
		}
	}
}
