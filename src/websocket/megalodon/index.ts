import generator, { MegalodonInterface, WebSocketInterface } from './megalodon';
import Mastodon from './mastodon';
import Pleroma from './pleroma';
import Misskey from './misskey';

export { MegalodonInterface, WebSocketInterface, Mastodon, Pleroma, Misskey };

export default generator;
