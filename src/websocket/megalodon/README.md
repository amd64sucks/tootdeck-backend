This folder includes files from [Megalodon](https://github.com/h3poteto/megalodon)

These files are licensed under the MIT license.

They have been modified to only implement WebSocket interfaces and parsers.

The WebSocket interfaces have been modified to only return a single interface and allow subscription
externally with data sent through the open socket.

This folder and its files are (possibly) temporary. They should be removed in a future release with
a more robust and trustworthy implementation.
