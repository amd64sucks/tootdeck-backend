import Pleroma from './pleroma';
import Mastodon from './mastodon';
import Misskey from './misskey';

import { Logger } from '../../utils/logger';

export interface WebSocketInterface {
	start(): void;
	stop(): void;
	send(data: string): void;
	// EventEmitter
	on(event: string | symbol, listener: (...args: any[]) => void): this;
	once(event: string | symbol, listener: (...args: any[]) => void): this;
	removeListener(event: string | symbol, listener: (...args: any[]) => void): this;
	removeAllListeners(event?: string | symbol): this;
}

export interface MegalodonInterface {
	// ======================================
	// WebSocket
	// ======================================
	socket(logger: Logger): WebSocketInterface;
}

export class NoImplementedError extends Error {
	constructor(err?: string) {
		super(err);

		this.name = new.target.name;
		Object.setPrototypeOf(this, new.target.prototype);
	}
}

export class ArgumentError extends Error {
	constructor(err?: string) {
		super(err);

		this.name = new.target.name;
		Object.setPrototypeOf(this, new.target.prototype);
	}
}

export class UnexpectedError extends Error {
	constructor(err?: string) {
		super(err);

		this.name = new.target.name;
		Object.setPrototypeOf(this, new.target.prototype);
	}
}

export class NodeinfoError extends Error {
	constructor(err?: string) {
		super(err);

		this.name = new.target.name;
		Object.setPrototypeOf(this, new.target.prototype);
	}
}

/**
 * Get client for each SNS according to megalodon interface.
 *
 * @param sns Name of your SNS, `mastodon` or `pleroma`.
 * @param baseUrl hostname or base URL.
 * @param accessToken access token from OAuth2 authorization
 * @param userAgent UserAgent is specified in header on request.
 * @return Client instance for each SNS you specified.
 */
const generator = (
	sns: 'mastodon' | 'pleroma' | 'misskey',
	baseUrl: string,
	accessToken: string | null = null,
	userAgent: string | null = null,
): MegalodonInterface => {
	switch (sns) {
		case 'pleroma': {
			const pleroma = new Pleroma(baseUrl, accessToken, userAgent);
			return pleroma;
		}
		case 'misskey': {
			const misskey = new Misskey(baseUrl, accessToken, userAgent);
			return misskey;
		}
		case 'mastodon': {
			const mastodon = new Mastodon(baseUrl, accessToken, userAgent);
			return mastodon;
		}
	}
};

export default generator;
