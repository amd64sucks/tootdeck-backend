import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { WebSocket } from 'ws';
import MegalodonGenerator from 'megalodon';

import { OptionalFeaturesService } from '../app/services/optinalFeatures.service';
import { DBApplicationService } from '../database/services/database.application.service';

import { InjectorInterceptor } from '../mirror/interceptors/injector.interceptor';

import CustomMegalodonGenerator, { WebSocketInterface } from './megalodon';

import { Logger } from '../utils/logger';
import { parseHandle } from '../utils/parse.handle';

import { UserCache } from '../user/types/cache.types';
import { InstanceType } from '../database/types';
import {
	AvaliableSubscription,
	CommandType,
	ConnectedClient,
	InstanceSubscription,
	OptionalParams,
	RemoteInstanceClient,
	ResponseType,
	SubscribeEvent,
	WebSocketUser,
	WebsocketAPI,
	WebsocketAvaliabledResponses,
	WebsocketMirror,
} from './types';

@Injectable()
export class WsService {
	private readonly logger = new Logger(WsService.name);
	private readonly clients = new Map<string, ConnectedClient[]>();
	private readonly misskeyTranslate = {
		subscribe: 'connect',
		unsubscribe: 'disconnect',
		user: 'homeTimeline',
		'user:notification': 'main',
		'public:local': 'localTimeline',
		public: 'globalTimeline',
		list: 'userList',
		hashtag: '', // Unsupported
		direct: '', // Unsupported
	};
	constructor(
		private readonly OptionalFeaturesService: OptionalFeaturesService,
		@Inject(forwardRef(() => DBApplicationService))
		private readonly DBApplicationService: DBApplicationService,
		@Inject(forwardRef(() => InjectorInterceptor<any>))
		private readonly InjectCache: InjectorInterceptor<any>,
	) {}

	private async fetchStreamingURL(domain: string, force: boolean = false) {
		const application = await this.DBApplicationService.get(domain);
		if (!application) {
			return null;
		}

		if (!force && application.streaming_url) {
			return application.streaming_url;
		}

		const instance = await MegalodonGenerator(application.type, 'https://' + application.domain)
			.getInstance()
			.catch((e) => {
				this.logger.error(`Unable to get instance info for ${domain}.`, e);
				return null;
			});
		const streaming_url = instance?.data.urls?.streaming_api;
		if (!instance || !streaming_url) {
			return null;
		}

		const update = await this.DBApplicationService.updateStreamingURL(
			application,
			streaming_url,
		);
		if (!update) {
			return null;
		}

		return streaming_url;
	}

	private async getStreamingURL(frontend: WebSocket, domain: string, force: boolean = false) {
		const streaming_url = await this.fetchStreamingURL(domain, force);
		if (!streaming_url) {
			this.logger.error('handleConnect', `Failed to get streaming url for ${domain}.`);
			const response: WebsocketAPI.Response = {
				type: ResponseType.API,
				data: WebsocketAPI.ResponseData.InternalServerError, //TODO
			};
			frontend.send(JSON.stringify(response));
			frontend.close();
			return null;
		}

		return streaming_url;
	}

	private saveClient(user_uuid: string, client: ConnectedClient) {
		client.frontend['user_uuid'] = user_uuid;

		const map = this.clients.get(user_uuid);
		if (!map) {
			this.clients.set(user_uuid, [client]);
		} else {
			map.push(client);
		}

		this.logger.verbose('saveClient', this.clients.size + ' currently connected users.');
	}

	private removeClient(user_uuid: string, client: WebSocketUser) {
		const map = this.clients.get(user_uuid);
		const index = map?.findIndex((x) => Object.is(x.frontend, client));
		if (!map || index === undefined || index === -1) {
			this.logger.warn('removeClient', user_uuid + ' no found, this should not happen.');
			return;
		}

		const removed = map.splice(index, 1)[0]!;
		removed.frontend.removeAllListeners();
		removed.remote.forEach((client) => {
			client.socket.stop();
			client.socket.removeAllListeners();
		});

		if (!map.length) {
			this.clients.delete(user_uuid);
		}

		this.logger.verbose('removeClient', this.clients.size + ' currently connected users.');
	}

	private findSubscription(
		x: InstanceSubscription,
		subscription_name: AvaliableSubscription,
		params?: OptionalParams,
	) {
		if (params?.list_id) {
			return x.name === subscription_name && x.list_id === params?.list_id;
		}

		if (params?.hashtag) {
			return x.name === subscription_name && x.hashtag === params?.hashtag;
		}

		return x.name === subscription_name;
	}

	private formatSubscription(
		command: CommandType,
		type: InstanceType,
		subscription: InstanceSubscription,
	) {
		switch (type) {
			case InstanceType.Mastodon:
			case InstanceType.Pleroma:
				return JSON.stringify({
					type: command,
					stream: subscription.name,
					list: subscription.list_id, // Optional
					tag: subscription.hashtag, // Optional
				});
			case InstanceType.Misskey:
				return JSON.stringify({
					type: this.misskeyTranslate[command],
					body: {
						channel: this.misskeyTranslate[subscription.name],
						id: subscription.uuid,
						params: {
							listId: subscription.list_id, // Optional
						},
					},
				});
		}
	}

	private subscribe(
		remote: RemoteInstanceClient,
		subscription_name: AvaliableSubscription,
		params?: OptionalParams,
	) {
		// Unsupported
		if (
			remote.type === InstanceType.Misskey &&
			(subscription_name === AvaliableSubscription.Direct || params?.hashtag)
		) {
			return;
		}

		const subscription: InstanceSubscription = {
			uuid: randomUUID(),
			name: subscription_name,
			list_id: params?.list_id,
			hashtag: params?.hashtag,
		};

		if (remote.subscriptions.find((x) => this.findSubscription(x, subscription_name, params))) {
			return;
		}

		// If instance is Mastodon API complient, stream: "user" already includes notifications, skipping it
		if (
			remote.type !== InstanceType.Misskey &&
			subscription_name === AvaliableSubscription.Notification &&
			remote.subscriptions.find((x) => x.name === AvaliableSubscription.User)
		) {
			return;
		}

		remote.subscriptions.push(subscription);

		const data = this.formatSubscription(CommandType.Subscribe, remote.type, subscription);
		remote.socket.send(data);
	}

	private unsubscribe(
		remote: RemoteInstanceClient,
		subscription_name: AvaliableSubscription,
		params?: OptionalParams,
	) {
		const subscription = remote.subscriptions.find((x) =>
			this.findSubscription(x, subscription_name, params),
		);
		if (!subscription) {
			return;
		}

		remote.subscriptions = remote.subscriptions.filter(
			(x) => !this.findSubscription(x, subscription_name, params),
		);

		const data = this.formatSubscription(CommandType.Unsubscribe, remote.type, subscription);
		remote.socket.send(data);
	}

	private handleSubscription(entry: ConnectedClient, event: SubscribeEvent) {
		if (!event.handle || !event.type || !event.stream) {
			return;
		}
		if (event.stream === 'list' && !event.list) {
			return;
		}

		const handle = parseHandle(event.handle);
		const instance = entry.remote.find(
			(x) => x.username === handle.username && x.domain === handle.domain,
		);
		if (!instance) {
			return;
		}

		switch (event.type) {
			case CommandType.Subscribe:
				this.subscribe(instance, event.stream, { list_id: event.list, hashtag: event.tag });
				break;
			case CommandType.Unsubscribe:
				this.unsubscribe(instance, event.stream, {
					list_id: event.list,
					hashtag: event.tag,
				});
				break;
		}
	}

	sendByApp(domain: string, data: WebsocketAPI.Response) {
		this.clients.forEach((user) => {
			user.forEach((client) => {
				const found = client.remote.find((x) => x.domain === domain);
				if (found) {
					this.send(client.frontend, data);
				}
			});
		});
	}

	async send(user_uuid: string, data: WebsocketAvaliabledResponses): Promise<void>;
	async send(frontend: WebSocket, data: WebsocketAvaliabledResponses): Promise<void>;
	async send(
		user_or_frontend: WebSocket | string,
		data: WebsocketAvaliabledResponses,
	): Promise<void> {
		if (
			typeof user_or_frontend === 'object' &&
			user_or_frontend?.constructor?.name === 'WebSocket'
		) {
			if (
				data.type !== ResponseType.API &&
				(this.OptionalFeaturesService.CACHE_AVATAR ||
					this.OptionalFeaturesService.CACHE_STATUS_INTERACTIONS)
			) {
				const mirror_response = data as WebsocketMirror.Response;
				if (mirror_response.handle) {
					try {
						const parsed = parseHandle(mirror_response.handle);
						await this.InjectCache.websocketInjectCache(parsed, data);
					} catch (e) {
						this.logger.warn(
							'send',
							"Sent data doesn't provide an handle. " + JSON.stringify(data),
						);
					}
				}
			}
			user_or_frontend.send(JSON.stringify(data));
			return;
		}

		if (typeof user_or_frontend === 'string') {
			const clients = this.clients.get(user_or_frontend);
			if (!clients) {
				return;
			}

			clients.forEach((client) => {
				client.frontend.send(JSON.stringify(data));
			});
			return;
		}

		throw new Error(
			`Unknown type in first parameter: typeof ${typeof user_or_frontend}, name ${user_or_frontend
				?.constructor?.name}, value: ${JSON.stringify(user_or_frontend)}`,
		);
	}

	private handleMisskeyData(entry: RemoteInstanceClient, data: any) {
		if (entry.type !== InstanceType.Misskey) {
			return data;
		}

		const misskey_data = data as {
			id: string;
			content: any;
		};

		const subscription = entry.subscriptions.find((x) => x.uuid === misskey_data.id);
		if (!subscription) {
			this.logger.error(
				'handleMisskeyData',
				`Couldn't translate UUID (${misskey_data.id}) to Mastodon API subscription.`,
			);
			return;
		}

		if (subscription.name === AvaliableSubscription.List) {
			return {
				stream: [subscription.name],
				content: misskey_data.content,
				list: subscription.list_id,
			};
		}

		return {
			stream: [subscription.name],
			content: misskey_data.content,
		};
	}

	private bindListeners(
		frontend: WebSocket,
		remote: WebSocketInterface,
		entry: RemoteInstanceClient,
		error_handle: (error: any) => Promise<void>,
	) {
		const handle = entry.username + '@' + entry.domain;

		// New status
		remote.on('update', (data) => {
			this.send(frontend, {
				type: ResponseType.Update,
				handle,
				data: this.handleMisskeyData(entry, data),
			});
		});
		// New notification
		remote.on('notification', (data) =>
			this.send(frontend, {
				type: ResponseType.Notification,
				handle,
				data: this.handleMisskeyData(entry, data),
			}),
		);
		// New message in conversation
		remote.on('conversation', (data) =>
			this.send(frontend, {
				type: ResponseType.Conversation,
				handle,
				data: this.handleMisskeyData(entry, data),
			}),
		);
		// Deleted status
		remote.on('delete', (data) =>
			this.send(frontend, {
				type: ResponseType.Delete,
				handle,
				data,
			}),
		);
		// Edited status
		remote.on('status_update', (data) =>
			this.send(frontend, {
				type: ResponseType.Status_update,
				handle,
				data: this.handleMisskeyData(entry, data),
			}),
		);

		// Error
		remote.on('error', error_handle);

		// Reconnected
		let initialized: boolean = false;
		remote.on('connect', () => {
			if (initialized) {
				this.send(frontend, {
					type: ResponseType.API,
					handle,
					data: WebsocketAPI.ResponseData.Reconnected,
				} as any);
			}

			entry.subscriptions.forEach((subscription) => {
				remote.send(
					this.formatSubscription(CommandType.Subscribe, entry.type, subscription),
				);
			});

			initialized = true;
		});
	}

	async handleConnect(user: UserCache, frontend: WebSocket) {
		const accounts = [user.main, ...user.secondary];
		const remote: RemoteInstanceClient[] = [];

		for (const account of accounts) {
			const streaming_url = await this.getStreamingURL(frontend, account.application.domain);
			if (!streaming_url) {
				continue;
			}

			let remote_socket = CustomMegalodonGenerator(
				account.application.type,
				streaming_url,
				account.token,
			).socket(
				new Logger(
					`Websocket-${account.application.type} @${account.username}@${account.application.domain}`,
				),
			);

			const remote_entry: RemoteInstanceClient = {
				subscriptions: [],
				username: account.username,
				domain: account.application.domain,
				type: account.application.type,
				socket: remote_socket,
			};

			const error_handler = async (error: any) => {
				const handle = account.username + '@' + account.application.domain;

				const message = error.message as string;
				if (message.includes('301')) {
					remote_socket.stop();
					remote_socket.removeAllListeners();

					const streaming_url = await this.getStreamingURL(
						frontend,
						account.application.domain,
						true,
					);
					if (!streaming_url) {
						this.send(frontend, {
							type: ResponseType.API,
							data: WebsocketAPI.ResponseData.InternalServerError, //TODO
						});
						return;
					}

					const updated_remote_entry = remote.find((x) => Object.is(x, remote_entry));
					if (!updated_remote_entry) {
						// Frontend closed
						return;
					}

					const new_remote_socket = CustomMegalodonGenerator(
						account.application.type,
						streaming_url,
						account.token,
					).socket(new Logger(`Websocket-${account.application.type} ${handle}`));

					updated_remote_entry.socket = new_remote_socket;

					this.bindListeners(
						frontend,
						new_remote_socket,
						updated_remote_entry,
						async (_) => {
							remote_socket.stop();
							remote_socket.removeAllListeners();

							this.logger.error(handle, 'Unable to connect.');

							this.send(frontend, {
								type: ResponseType.API,
								data: WebsocketAPI.ResponseData.InternalServerError, //TODO
							});
						},
					);

					this.logger.verbose(handle, 'Invalid streaming url, recovered.');
					return;
				}

				this.logger.error(handle, error);
			};

			this.bindListeners(frontend, remote_socket, remote_entry, error_handler);

			remote.push(remote_entry);
		}

		const entry: ConnectedClient = {
			frontend: frontend as WebSocketUser,
			remote,
		};

		this.saveClient(user.uuid, entry);

		frontend.addEventListener('message', (data) => {
			try {
				const event = JSON.parse(data.data as string);
				this.handleSubscription(entry, event);
			} catch (e) {}
		});

		this.send(frontend, {
			type: ResponseType.API,
			data: WebsocketAPI.ResponseData.AwaitingSubscription,
		});

		// Logs
		const total = Array.from(this.clients)
			.map(([_, user]) =>
				user.map((connected) => connected.remote.length).reduce((a, b) => a + b, 0),
			)
			.reduce((a, b) => a + b, 0);
		this.logger.debug(
			'handleConnect',
			'Total connected sockets: ' + (this.clients.size + total),
		);
	}

	handleDisconnect(frontend: WebSocketUser) {
		if (!frontend.user_uuid) {
			this.logger.debug('handleDisconnect', 'Disconnected unauthenticated client');
			return;
		}

		this.removeClient(frontend.user_uuid, frontend);

		// Logs
		const total = Array.from(this.clients)
			.map(([_, user]) =>
				user.map((connected) => connected.remote.length).reduce((a, b) => a + b, 0),
			)
			.reduce((a, b) => a + b, 0);
		this.logger.debug(
			'handleDisconnect',
			'Total connected sockets: ' + (this.clients.size + total),
		);
	}
}
