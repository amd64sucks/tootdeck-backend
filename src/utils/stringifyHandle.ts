import { InstanceHandle } from './handle.types';

export function stringifyHandle(handle: InstanceHandle) {
	return handle.username + '@' + handle.domain;
}
