import { parseHandle } from './parse.handle';

describe('Parse handle', () => {
	const TryCatch = (handle: string) => {
		let result: any;
		let has_throw = false;

		try {
			result = parseHandle(handle);
		} catch (e) {
			has_throw = true;
		}

		return {
			result,
			has_throw,
		};
	};

	const valid_handle = [
		['@test@localhost', 'test', 'localhost'],
		['test@localhost', 'test', 'localhost'],
		['@valid.valid@valid.valid', 'valid.valid', 'valid.valid'],
		['valid.valid@valid.valid', 'valid.valid', 'valid.valid'],
	];

	const invalid_handle = [
		'',
		'@@',
		'localhost',
		'@valid@invalid@',
		'@@invalid@valid',
		'@valid@inva..lid',
		'@valid@https://test',
		'@valid@invalid/invalid',
	];

	valid_handle.forEach(([handle, username, domain]) => {
		it('Valid handle ' + handle, async () => {
			const { result, has_throw } = TryCatch(handle!);

			expect(result).toMatchObject({
				username,
				domain,
			});
			expect(has_throw).toBe(false);
		});
	});

	invalid_handle.forEach((handle) => {
		it('invalid handle ' + handle, async () => {
			const { has_throw } = TryCatch(handle);

			expect(has_throw).toBe(true);
		});
	});
});
