import { UserStore } from '../database/entities/user.entity';
import { AccountStore } from '../database/entities/account.entity';

import { InstanceHandle } from './handle.types';
import { AccountCache, UserCache } from '../user/types/cache.types';

/**
 * Search handle in user entity
 *
 * @param user
 * @param handle
 * @returns `T` if found
 * @returns `null` if not found
 */
export function userHasIntance(user: UserStore, handle: InstanceHandle): AccountStore | null;
export function userHasIntance(user: UserCache, handle: InstanceHandle): AccountCache | null;
export function userHasIntance(
	user: UserCache | UserStore,
	handle: InstanceHandle,
): AccountCache | AccountStore | null {
	if (!user.main || !handle) {
		return null;
	}

	if (
		user.main.username.toLowerCase() === handle.username.toLowerCase() &&
		user.main.application.domain === handle.domain
	) {
		return user.main;
	}

	if (!user.secondary) {
		return null;
	}

	for (const account of user.secondary) {
		if (
			account.username.toLowerCase() === handle.username.toLowerCase() &&
			account.application.domain === handle.domain
		) {
			return account;
		}
	}

	return null;
}
