import { isURL } from './isURL';

describe('isURL', () => {
	const valid = [
		'http://www.foufos.gr',
		'https://www.foufos.gr',
		'http://foufos.gr',
		'http://www.foufos.gr/kino',
		'http://werer.gr',
		'www.foufos.gr',
		'www.mp3.com',
		'www.t.co',
		'http://t.co',
		'http://www.t.co',
		'https://www.t.co',
		'www.aa.com',
		'http://aa.com',
		'http://www.aa.com',
		'https://www.aa.com',
		'www.foufos',
		'www.foufos-.gr',
		'www.-foufos.gr',
		'foufos.gr',
		'http://www.foufos',
		'http://foufos',
		'localhost',
		'192.168.1.1',
		'https://192.168.1.1',
	];

	const invalid = [
		'',
		'http:///invalid',
		'http:///invalid.',
		'http://a..a.com',
		'badurlnotvalid://www.google.com',
		'htpp://www.google.com',
		'www.mp3#.com',
		'www.mp3@.com',
	];

	valid.forEach((url) => {
		it('Valid ' + url, () => {
			expect(isURL(url, true)).toBe(true);
		});
	});

	invalid.forEach((url) => {
		it('invalid ' + url, () => {
			expect(isURL(url, true)).toBe(false);
		});
	});
});
