import { BadRequestException } from '@nestjs/common';

import { isURL } from './isURL';

import { ApiResponseError } from '../auth/types/api.types';
import { InstanceHandle } from './handle.types';

/**
 * @throws BadRequestException
 */
export function parseHandle(handle: string | undefined): InstanceHandle {
	if (!handle) {
		throw new BadRequestException(ApiResponseError.InvalidHandle);
	}

	if ((handle.match(/@/g) || []).length > 2) {
		throw new BadRequestException(ApiResponseError.InvalidHandle);
	}

	const parse = handle
		.split('@')
		.map((x) => x.trim())
		.filter((x) => x);

	if (parse.length !== 2) {
		throw new BadRequestException(ApiResponseError.InvalidHandle);
	}

	let raw_domain = parse[1]!;

	if (raw_domain.search('http://') !== -1 || raw_domain.search('https://') !== -1) {
		throw new BadRequestException(ApiResponseError.InvalidDomain);
	}

	if (!isURL(raw_domain, true)) {
		throw new BadRequestException(ApiResponseError.InvalidHandle);
	}

	if (raw_domain.split('/').length !== 1) {
		throw new BadRequestException(ApiResponseError.InvalidDomain);
	}

	return { username: parse[0]!, domain: raw_domain.toLowerCase() };
}
