/**
 * There is no online regex match for both 'localhost' (valid) and 'local..host' (invalid).
 * I think this is frustrating, so I have done it in plain JS, which is easier to debug anyway.
 */
export function isURL(url: string, allow_localhost: boolean) {
	let untested = '';

	let a = url.replace('http://', '');
	let b = url.replace('https://', '');
	if (a.length !== url.length) {
		untested = a;
	} else if (b.length !== url.length) {
		untested = b;
	} else {
		untested = url;
	}

	let c = untested.split('/');
	if (!c[0]) {
		return false;
	}
	untested = c[0];

	if (untested.endsWith('.')) {
		return false;
	}

	if (untested === 'localhost') {
		return allow_localhost;
	}

	const char_array = untested.split('');

	let last = '';
	// Check duplicate dot
	for (const c of char_array) {
		if (c !== '.') {
			last = '';
			continue;
		}
		if (last === c) {
			return false;
		}
		last = c;
	}

	const invalid_char_range: [number, number][] = [
		[0, 44],
		[47, 47],
		[58, 64],
		[91, 94],
		[96, 96],
		[123, 127],
	];

	for (const range of invalid_char_range) {
		for (const c of char_array) {
			let ascii = c.charCodeAt(0);
			if (ascii >= range[0] && ascii <= range[1]) {
				return false;
			}
		}
	}

	return true;
}
