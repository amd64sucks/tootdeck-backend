export enum ApiResponseError {
	InvalidHandle = 'Invalid handle',
	InvalidDomain = 'Invalid domain',
	InvalidInstance = 'Invalid instance',
	InvalidInstanceResponse = 'Got unknow response from instance',
	UnsupportedInstance = 'Unsupported instance type',
	AccountAlreadyExist = 'This account is already linked to your account',
	GenerateRedirectFail = 'Unable to generate redirect uri',
}
