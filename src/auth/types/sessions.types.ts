import { HashedFingerprint } from './fingerprint.types';
import { OAuthAutorizeOptions } from './oauth.types';

export interface OAuthSession {
	token_uuid: string;
	username: string;
	domain: string;
	fingerprint: HashedFingerprint;
	options: OAuthAutorizeOptions;
}

export interface JWESession {
	token_uuid: string;
	user_uuid: string;
	token: string;
	fingerprint: HashedFingerprint;
	linked_to_token_uuid: string;
}

export interface OpenSession {
	token_uuid: string;
	browser: {
		name: string;
		version: number;
	};
	os: string;
	created_at: Date;
	updated_at: Date;
}

export type OpenSessions = OpenSession[];

export interface UserDeleteSession {
	token_uuid: string;
	user_uuid: string;
	code: string;
	token: string;
}
