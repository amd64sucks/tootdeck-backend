import { ApplicationStore } from 'src/database/entities/application.entity';

export interface OAuthSessionToken {
	token: string;
	username: string;
	domain: string;
}

export interface OAuthGeneratedApplication {
	entity: ApplicationStore;
	url: string;
}

export interface OAuthAutorize {
	token: string;
	uri: string;
}

export interface OAuthAutorizeOptions {
	add_account?: boolean;
	to_user_uuid?: string;
	refresh_account?: boolean;
	to_account_uuid?: string;
}

export interface OAuthAutorizeOptionsAddAccount extends OAuthAutorizeOptions {
	add_account: boolean;
	to_user_uuid: string;
}

export interface OAuthAutorizeOptionsRefreshAccount extends OAuthAutorizeOptions {
	refresh_account: boolean;
	to_account_uuid: string;
}

export enum OAuthAppVerify {
	Error = -2,
	NotFound = -1,
	Failed = 0,
	Success = 1,
}

export enum OAuthVerify {
	Unsupported = -1,
	Failed = 0,
	Ok = 1,
}
