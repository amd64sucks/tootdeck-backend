export enum JWEType {
	Access = 0,
	Refresh = 1,
	OAuth = 2,
	AccountDelete = 3,
}
