import { ApiProperty } from '@nestjs/swagger';

export class OAuthAutorizeResponse {
	@ApiProperty()
	uri: string;
}
