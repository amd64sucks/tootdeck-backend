import { ApiProperty } from '@nestjs/swagger';

class OpenSessionBrowser {
	@ApiProperty()
	name: string;

	@ApiProperty()
	version: number;
}

export class OpenSessionResponse {
	@ApiProperty()
	current: boolean;

	@ApiProperty()
	token_uuid: string;

	@ApiProperty({ type: OpenSessionBrowser })
	browser: OpenSessionBrowser;

	@ApiProperty()
	os: string;

	@ApiProperty()
	created_at: Date;

	@ApiProperty()
	updated_at: Date;
}
