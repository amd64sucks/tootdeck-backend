import { BadRequestException, Injectable } from '@nestjs/common';
import { createHash } from 'crypto';
import { UAParser } from 'ua-parser-js';

import { Logger } from '../../../utils/logger';

import { Fingerprint, FingerprintValid, HashedFingerprint } from '../../types/fingerprint.types';

@Injectable()
export class BrowserFingerprintService {
	private readonly logger = new Logger(BrowserFingerprintService.name);

	/**
	 * @throws BadRequestException
	 */
	private checkParams(user_agent: string | undefined, ip: string | undefined) {
		if (!user_agent) {
			this.logger.error('checkParams', 'User agent is undefined');
			throw new BadRequestException();
		}

		if (!ip) {
			this.logger.error('checkParams', 'Ip is undefined');
			throw new BadRequestException();
		}
	}

	/**
	 * @throws BadRequestException
	 */
	get(user_agent: string | undefined, ip: string | undefined): Fingerprint {
		this.checkParams(user_agent, ip);

		const ua = UAParser(user_agent);
		return {
			browser: {
				name: ua.browser.name,
				version: ua.browser.version ? parseInt(ua.browser.version) : 0,
			},
			os: { name: ua.os.name, version: ua.os.version },
			device: { vendor: ua.device.vendor, model: ua.device.model, type: ua.device.type },
			cpu: { architecture: ua.cpu.architecture },
			ip: ip,
		};
	}

	hash(fingerprint: Fingerprint): string {
		return createHash('sha512').update(JSON.stringify(fingerprint)).digest('hex');
	}

	getHashed(fingerprint: Fingerprint): HashedFingerprint {
		return {
			hash: this.hash(fingerprint),
			browser_version: fingerprint.browser.version,
			os: fingerprint.os.name ?? 'unknown',
		};
	}

	validate(hash: HashedFingerprint, fingerprint: Fingerprint): FingerprintValid {
		const fingerprint_hashed = this.hash(fingerprint);
		if (fingerprint_hashed === hash.hash) {
			return FingerprintValid.True;
		}

		if (fingerprint.browser.version !== hash.browser_version) {
			fingerprint.browser.version--;

			const new_hash = this.hash(fingerprint);
			if (new_hash === hash.hash) {
				return FingerprintValid.Regenerate;
			}
		}

		return FingerprintValid.False;
	}
}
