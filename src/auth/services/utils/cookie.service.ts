import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FastifyReply } from 'fastify/types/reply';

import { JWETime } from '../../config';

import { JWEType } from '../../types/jwe.types';

@Injectable()
export class CookieService {
	private readonly DOMAIN: string;

	constructor(private readonly configService: ConfigService) {
		this.DOMAIN = configService.get<string>('DOMAIN') ?? '';
	}

	private getValues(type: JWEType | string) {
		switch (type) {
			case JWEType.Access:
				return { name: 'a', expires: JWETime.Access() };
			case JWEType.Refresh:
				return { name: 'r', expires: JWETime.Refresh() };
			case JWEType.OAuth:
				return { name: 's', expires: JWETime.OAuth() };
			case JWEType.AccountDelete:
				return { name: 'd', expires: JWETime.Delete() };
		}
		return { name: '', expires: new Date() };
	}

	create(res: FastifyReply, type: JWEType, value: string) {
		const { name, expires } = this.getValues(type);
		res.cookie(name, value, {
			domain: this.DOMAIN,
			sameSite: type === JWEType.OAuth ? 'lax' : 'strict',
			httpOnly: true,
			expires,
			secure: type !== JWEType.OAuth,
			path: type !== JWEType.OAuth ? '/' : '',
		});
	}

	delete(res: FastifyReply, type: JWEType) {
		const { name } = this.getValues(type);
		res.cookie(name, '', {
			domain: this.DOMAIN,
			sameSite: 'strict',
			httpOnly: true,
			expires: new Date(),
			secure: true,
			path: '/',
		});
	}
}
