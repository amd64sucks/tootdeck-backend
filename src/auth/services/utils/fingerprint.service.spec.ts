import { Test, TestingModule } from '@nestjs/testing';

import { BrowserFingerprintService } from './fingerprint.service';

import { FingerprintValid } from '../../types/fingerprint.types';

describe('BrowserFingerprintService', () => {
	let service: BrowserFingerprintService;

	const test_user_agent =
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36';
	const test_ip = '192.168.1.1';
	const test_hash =
		'f38cd70a78a7dd09c3242b09894dee8d675a7eb0baf444881d03f63ef66e1f99cb9f055ddf0b56c25acd6f1b36aeaa97e5e2beb3d34125aa0022abab1a9c30eb';

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			providers: [BrowserFingerprintService],
		}).compile();

		service = app.get<BrowserFingerprintService>(BrowserFingerprintService);
	});

	describe('get', () => {
		it('generating valid entry, should return entry', () => {
			expect(service.get(test_user_agent, test_ip)).toMatchObject({
				browser: {
					name: 'Chrome',
					version: 112,
				},
				os: { name: 'Linux', version: 'x86_64' },
				device: { vendor: undefined, model: undefined, type: undefined },
				cpu: { architecture: 'amd64' },
				ip: test_ip,
			});
		});

		it('generating invalid entry, should throw', () => {
			let has_throw = false;
			try {
				service.get('', test_ip);
			} catch (e) {
				has_throw = true;
			}

			expect(has_throw).toBe(true);
		});

		it('generating invalid entry, should throw', () => {
			let has_throw = false;
			try {
				service.get(test_user_agent, '');
			} catch (e) {
				has_throw = true;
			}

			expect(has_throw).toBe(true);
		});
	});

	describe('hash', () => {
		it('should return hash', () => {
			const fingerprint = service.get(test_user_agent, test_ip);

			expect(service.hash(fingerprint)).toBe(test_hash);
		});
	});

	describe('getHashed', () => {
		it('should return hash', () => {
			const fingerprint = service.get(test_user_agent, test_ip);

			expect(service.getHashed(fingerprint)).toMatchObject({
				hash: test_hash,
				browser_version: 112,
			});
		});
	});

	describe('validate', () => {
		it('valid fingerprint, should return true', () => {
			const fingerprint = service.get(test_user_agent, test_ip);
			const hashed = service.getHashed(fingerprint);

			expect(service.validate(hashed, fingerprint)).toBe(FingerprintValid.True);
		});

		it('invalid fingerprint, should return false', () => {
			const fingerprint = service.get(test_user_agent, test_ip);
			const hashed = service.getHashed(fingerprint);
			hashed.hash = 'invalid';

			expect(service.validate(hashed, fingerprint)).toBe(FingerprintValid.False);
		});

		it('browser has been updated, should return regenerate', () => {
			const fingerprint = service.get(test_user_agent, test_ip);
			const hashed = service.getHashed(fingerprint);
			fingerprint.browser.version++;

			expect(service.validate(hashed, fingerprint)).toBe(FingerprintValid.Regenerate);
		});

		it('browser has been downgraded, should return false', () => {
			const fingerprint = service.get(test_user_agent, test_ip);
			const hashed = service.getHashed(fingerprint);
			fingerprint.browser.version--;

			expect(service.validate(hashed, fingerprint)).toBe(FingerprintValid.False);
		});
	});
});
