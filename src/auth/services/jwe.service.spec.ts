import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';
import { randomUUID } from 'crypto';

import { JWEService } from './jwe.service';
import { JWESessionService } from './sessions/jwe.session.service';
import { BrowserFingerprintService } from './utils/fingerprint.service';

import { secretConfig } from '../../app/modules/secret.config';

import { Fingerprint } from '../types/fingerprint.types';
import { JWEType } from '../types/jwe.types';
import { JWESession } from '../types/sessions.types';
import { FullAppModule } from '../../app/app.module';

describe('JWEService', () => {
	let service: JWEService;
	let SessionService: JWESessionService;
	let Jwt: JwtService;
	let BrowserFingerprint: BrowserFingerprintService;
	let fingerprint: Fingerprint;
	let valid_access_token: string;
	let valid_refresh_token: string;
	let access_session: JWESession;
	let refresh_session: JWESession;

	const secret = secretConfig();
	const test_uuid = randomUUID();
	const test_user_agent =
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36';
	const test_ip = '192.168.1.1';
	const test_hash =
		'f38cd70a78a7dd09c3242b09894dee8d675a7eb0baf444881d03f63ef66e1f99cb9f055ddf0b56c25acd6f1b36aeaa97e5e2beb3d34125aa0022abab1a9c30eb';

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		service = app.get(JWEService);
		SessionService = app.get(JWESessionService);
		Jwt = app.get(JwtService);
		BrowserFingerprint = app.get(BrowserFingerprintService);
		fingerprint = BrowserFingerprint.get(test_user_agent, test_ip);
	});

	describe('create', () => {
		const TryCatch = async (user_uuid: string, fingerprint: Fingerprint) => {
			let result: any;
			let has_throw = false;

			try {
				result = await service.create(user_uuid, fingerprint);
			} catch (e) {
				has_throw = true;
			}

			return {
				result,
				has_throw,
			};
		};

		it('creating valid token, should return token', async () => {
			const { result, has_throw } = await TryCatch(test_uuid, fingerprint);

			const r = (result as [string, string])?.filter((x) => x);

			expect(r.length === 2).toBe(true);
			expect(has_throw).toBe(false);

			[valid_access_token, valid_refresh_token] = result;
		});

		it('creating valid token with invalid data, should throw', async () => {
			const { result, has_throw } = await TryCatch('', fingerprint);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});

		it('creating valid token with invalid data, should throw', async () => {
			const { result, has_throw } = await TryCatch(test_uuid, undefined as any);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});
	});

	describe('validate', () => {
		const TryCatch = async (type: JWEType, raw_token: string, fingerprint: Fingerprint) => {
			let result: any;
			let has_throw = false;

			try {
				result = await service.validate(type, raw_token, fingerprint);
			} catch (e) {
				has_throw = true;
			}

			return {
				result,
				has_throw,
			};
		};

		it('valid token, should return true', async () => {
			const { result, has_throw } = await TryCatch(
				JWEType.Access,
				valid_access_token,
				fingerprint,
			);

			expect(result).toMatchObject({
				user_uuid: test_uuid,
				token: valid_access_token,
				fingerprint: {
					browser_version: 112,
					hash: test_hash,
				},
			} as JWESession);
			expect(has_throw).toBe(false);

			access_session = result;
		});

		it('valid token, should return true', async () => {
			const { result, has_throw } = await TryCatch(
				JWEType.Refresh,
				valid_refresh_token,
				fingerprint,
			);

			expect(!!result).toBe(true);
			expect(has_throw).toBe(false);

			refresh_session = result;
		});

		it('valid token with updated browser, should return true', async () => {
			const copy = { ...fingerprint };
			copy.browser.version++;

			const { result, has_throw } = await TryCatch(JWEType.Access, valid_access_token, copy);

			expect(result).toMatchObject({
				user_uuid: test_uuid,
				fingerprint: BrowserFingerprint.getHashed(copy),
				token: valid_access_token,
			} as JWESession);
			expect(has_throw).toBe(false);
		});

		it('valid token with downgraded browser, should return throw', async () => {
			const copy = { ...fingerprint };
			copy.browser.version--;

			const { result, has_throw } = await TryCatch(
				JWEType.Refresh,
				valid_refresh_token,
				copy,
			);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});

		it('invalid token, should return throw', async () => {
			const { result, has_throw } = await TryCatch(
				JWEType.Access,
				valid_refresh_token,
				fingerprint,
			);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});

		it('invalid token, should return throw', async () => {
			const { result, has_throw } = await TryCatch(JWEType.Access, 'invalid', fingerprint);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});

		it('invalid fingerprint, should return throw', async () => {
			const copy = { ...fingerprint };
			copy.browser.name = '';

			const { result, has_throw } = await TryCatch(JWEType.Access, 'invalid', copy);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});
	});

	describe('update', () => {
		let result_access_token: string;
		let result_refresh_token: string;

		const parseToken = async (token: string) => {
			return Jwt.verifyAsync(token, {
				algorithms: ['HS512'],
				secret,
			})
				.then((r) => r.u)
				.catch((e) => {
					return null;
				});
		};

		const TryCatch = async (session: JWESession, fingerprint: Fingerprint) => {
			let result: any;
			let has_throw = false;

			try {
				result = await service.update(session, fingerprint);
			} catch (e) {
				has_throw = true;
			}

			return {
				result,
				has_throw,
			};
		};

		it('update valid token, should return token', async () => {
			const { result, has_throw } = await TryCatch(refresh_session, fingerprint);

			const r = (result as [string, string])?.filter((x) => x);

			expect(r.length === 2).toBe(true);
			expect(has_throw).toBe(false);

			[result_access_token, result_refresh_token] = result;
		});

		it('validate result', async () => {
			const [jwt_access, jwt_refresh] = await Promise.all([
				parseToken(result_access_token),
				parseToken(result_refresh_token),
			]);

			if (!jwt_access || !jwt_refresh) {
				expect(!!jwt_access).toBe(true);
				expect(!!jwt_refresh).toBe(true);
				return;
			}

			const [result_refresh_removed, result_access, result_refresh] = await Promise.all([
				SessionService.get(JWEType.Refresh, refresh_session.token_uuid),
				SessionService.get(JWEType.Access, jwt_access),
				SessionService.get(JWEType.Refresh, jwt_refresh),
			]);

			await Promise.all([
				SessionService.delete(JWEType.Access, jwt_access),
				SessionService.delete(JWEType.Refresh, jwt_refresh),
			]);

			expect(!!result_refresh_removed).toBe(false);

			expect(!!result_access).toBe(true);
			expect(!!result_refresh).toBe(true);
		});
	});

	describe('delete', () => {
		const create = async (user_uuid: string, fingerprint: Fingerprint) => {
			let result: any;
			let has_throw = false;

			try {
				result = await service.create(user_uuid, fingerprint);
			} catch (e) {
				has_throw = true;
			}

			return {
				result,
				has_throw,
			};
		};

		const parseToken = async (token: string) => {
			return Jwt.verifyAsync(token, {
				algorithms: ['HS512'],
				secret,
			})
				.then((r) => r.u)
				.catch((e) => {
					return null;
				});
		};

		let refresh: JWESession | null;

		it('delete valid token, should not throw', async () => {
			const c = await create(test_uuid, fingerprint);
			const [access_token, refresh_token] = c.result;
			if (c.has_throw) {
				expect(c.has_throw).toBe(false);
				return;
			}

			const token = await parseToken(refresh_token);
			if (!token) {
				expect(!!token).toBe(true);
			}

			refresh = await SessionService.get(JWEType.Refresh, token);
			if (!refresh) {
				expect(!!refresh).toBe(true);
				return;
			}

			await service.delete(refresh);

			const delete_refresh = await SessionService.get(JWEType.Refresh, refresh.token_uuid);
			expect(delete_refresh).toBe(null);
		});
	});
});
