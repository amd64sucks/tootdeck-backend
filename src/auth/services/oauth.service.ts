import {
	BadRequestException,
	Inject,
	Injectable,
	InternalServerErrorException,
	ServiceUnavailableException,
	UnauthorizedException,
	forwardRef,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import * as Megalodon from 'megalodon';
import MegalodonGenerator from 'megalodon';
import { NoImplementedError } from 'megalodon/lib/src/megalodon';
import { AxiosError } from 'axios';

import { OAuthSessionService } from './sessions/oauth.session.service';
import { BrowserFingerprintService } from './utils/fingerprint.service';
import { DBApplicationService } from '../../database/services/database.application.service';
import { DBUserService } from '../../database/services/database.user.service';
import { DBAccountService } from '../../database/services/database.account.service';
import { UserCacheService } from '../../user/services/user.cache.service';

import { ApplicationStore } from '../../database/entities/application.entity';
import { AccountStore } from '../../database/entities/account.entity';

import { Logger } from '../../utils/logger';
import { userHasIntance } from '../../utils/userHasInstance';

import { JWETime } from '../config';
import { secretConfig } from '../../app/modules/secret.config';

import { InstanceType } from '../../database/types';
import { ApiResponseError } from '../types/api.types';
import { Fingerprint, FingerprintValid } from '../types/fingerprint.types';
import {
	OAuthAppVerify,
	OAuthAutorize,
	OAuthAutorizeOptionsAddAccount,
	OAuthAutorizeOptionsRefreshAccount,
	OAuthGeneratedApplication,
	OAuthSessionToken,
	OAuthVerify,
} from '../types/oauth.types';
import { OAuthSession } from '../types/sessions.types';

@Injectable()
export class OAuthService {
	private readonly logger = new Logger(OAuthService.name);
	private readonly secret: string;

	private readonly DOMAIN: string;
	private readonly REDIRECT: string;

	constructor(
		private readonly configService: ConfigService,
		private readonly JwtService: JwtService,
		@Inject(forwardRef(() => DBApplicationService))
		private readonly DBApplicationService: DBApplicationService,
		@Inject(forwardRef(() => DBAccountService))
		private readonly DBAccountService: DBAccountService,
		private readonly DBUserService: DBUserService,
		private readonly OAuthSessionService: OAuthSessionService,
		private readonly UserCacheService: UserCacheService,
		private readonly BrowserFingerprint: BrowserFingerprintService,
	) {
		this.secret = secretConfig();
		if (!this.secret) {
			this.logger.error('constructor', 'Unable to read secret');
			process.exit(1);
		}
		this.logger.log('constructor', 'Secret loaded');

		this.DOMAIN = configService.get<string>('DOMAIN') ?? '';
		this.REDIRECT = `https://${this.DOMAIN}/api/oauth/authorize/redirect`;
	}

	/**
	 * Utils
	 */
	//#region

	private async oauthToken(token_uuid: string): Promise<string> {
		return this.JwtService.signAsync(
			{ s: token_uuid },
			{
				algorithm: 'HS512',
				expiresIn: JWETime.OAuth().valueOf(),
				secret: this.secret,
			},
		);
	}

	private async verifyApp(username: string, domain: string): Promise<OAuthAppVerify> {
		const account = await this.DBAccountService.get(username, domain);
		if (!account || !account.token) {
			return OAuthAppVerify.NotFound;
		}

		const megalodon = MegalodonGenerator(
			account.application.type,
			'https://' + account.application.domain,
			account.token,
		);
		const verif_app: OAuthAppVerify = await megalodon
			.verifyAppCredentials()
			.then((r) => (r.status === 200 ? OAuthAppVerify.Success : OAuthAppVerify.Failed))
			.catch((e: AxiosError) => {
				this.logger.error(
					'verifyApp',
					`Unable to verify app for ${account.application.domain}`,
					e,
				);

				if (!e.response) {
					return OAuthAppVerify.Error;
				}

				const error_status = e.response.status;
				const error_text = (e.response.data as any).error;

				if (
					e instanceof NoImplementedError || // Misskey
					(error_status === 401 && error_text === 'The access token is invalid') || // Mastodon
					(error_status === 400 && error_text?.detail === 'Internal server error') // Pleroma
				) {
					return OAuthAppVerify.Failed;
				}

				return OAuthAppVerify.Error;
			});

		return verif_app;
	}

	private redirect(entry: ApplicationStore, domain: string) {
		return `https://${domain}/oauth/authorize?redirect_uri=https://${this.DOMAIN}/api/oauth/authorize/redirect&response_type=code&client_id=${entry.key}&scope=read%20write%20follow&force_login=true`;
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	/**
	 * @throws BadRequestException
	 */
	async generateApplication(domain: string): Promise<OAuthGeneratedApplication>;
	async generateApplication(domain: string, update: boolean): Promise<OAuthGeneratedApplication>;
	async generateApplication(
		domain: string,
		update: boolean = false,
	): Promise<OAuthGeneratedApplication> {
		const base_url = 'https://' + domain;

		const sns = (await Megalodon.detector(base_url).catch((e) => {
			this.logger.error('generateApplication', `Unable to determine sns for ${domain}`, e);
			return null;
		})) as InstanceType;

		if (!sns) {
			throw new BadRequestException(ApiResponseError.InvalidInstance);
		}

		const app_data = await MegalodonGenerator(sns, base_url)
			.registerApp('TootDeck', {
				redirect_uris: this.REDIRECT,
				scopes: ['read', 'write', 'follow'],
			})
			.catch((e) => {
				this.logger.error('generateApplication', `Unable to register app for ${domain}`, e);
				return null;
			});

		const uri = app_data?.url;
		if (!uri) {
			throw new BadRequestException(ApiResponseError.InvalidInstanceResponse);
		}

		this.logger.verbose('generateApplication', 'Created app for ' + domain);

		const url = uri + `&force_login=true`;

		if (update) {
			// Application removed from instance
			const updated = await this.DBApplicationService.updateCredentials(
				domain,
				app_data.client_id,
				app_data.client_secret,
			);
			if (!updated) {
				this.logger.error('generateApplication', `Failed to update ${domain} secrets`);
				throw new InternalServerErrorException();
			}

			return {
				entity: updated,
				url,
			};
		}

		const store = await this.DBApplicationService.create(
			domain,
			sns,
			app_data.client_id,
			app_data.client_secret,
		);
		if (!store) {
			this.logger.error('generateApplication', `Failed to store ${domain} secrets`);
			throw new InternalServerErrorException();
		}

		return {
			entity: store,
			url,
		};
	}

	async validate(raw_token: string, fingerprint: Fingerprint): Promise<OAuthSession> {
		const jwt = await this.JwtService.verifyAsync<{ s: string }>(raw_token, {
			algorithms: ['HS512'],
			secret: this.secret,
		}).catch((e) => {
			this.logger.warn('validate', 'Invalid JWT: signature verification failed');
			throw new UnauthorizedException();
		});

		const token_uuid = jwt.s;

		const session = await this.OAuthSessionService.get(token_uuid);
		if (!session) {
			this.logger.verbose('validate', 'Invalid JWT: no session');
			throw new UnauthorizedException();
		}

		// Validate fingerprint
		const valid = this.BrowserFingerprint.validate(session.fingerprint, fingerprint);
		if (valid === FingerprintValid.False) {
			this.logger.verbose('validate', 'Invalid JWT: fingerprint');
			throw new UnauthorizedException();
		}

		if (process.env['NODE_ENV'] !== 'test') {
			const del = await this.OAuthSessionService.delete(token_uuid);
			if (!del) {
				this.logger.verbose('validate', 'Invalid delete');
				throw new InternalServerErrorException();
			}
		}

		return session;
	}

	/**
	 * @throws BadRequestException
	 * @throws InternalServerErrorException
	 * @throws ServiceUnavailableException
	 */
	async authorize(
		username: string,
		domain: string,
		fingerprint: Fingerprint,
	): Promise<OAuthAutorize>;
	async authorize(
		username: string,
		domain: string,
		fingerprint: Fingerprint,
		options: OAuthAutorizeOptionsAddAccount,
	): Promise<OAuthAutorize>;
	async authorize(
		username: string,
		domain: string,
		fingerprint: Fingerprint,
		options: OAuthAutorizeOptionsRefreshAccount,
	): Promise<OAuthAutorize>;
	async authorize(
		username: string,
		domain: string,
		fingerprint: Fingerprint,
		options?: OAuthAutorizeOptionsAddAccount | OAuthAutorizeOptionsRefreshAccount | undefined,
	): Promise<OAuthAutorize> {
		// Add secondary account
		if (options?.add_account) {
			const user = await this.DBUserService.getByUUID(
				(options as OAuthAutorizeOptionsAddAccount).to_user_uuid,
			);
			if (!user) {
				this.logger.error(
					'authorize',
					`Unable to find user for ${
						(options as OAuthAutorizeOptionsAddAccount).to_user_uuid
					}.`,
				);
				throw new InternalServerErrorException();
			}

			if (userHasIntance(user, { username, domain })) {
				throw new BadRequestException(ApiResponseError.AccountAlreadyExist);
			}
		}

		let redirect_uri: string;
		const application = await this.DBApplicationService.get(domain);
		if (application) {
			if (application.type === InstanceType.Misskey) {
				throw new ServiceUnavailableException(ApiResponseError.UnsupportedInstance);
			}

			if (process.env['NODE_ENV'] === 'test') {
				redirect_uri = this.redirect(application, domain);
			} else {
				// Verify if app is still valid
				const verif = await this.verifyApp(username, domain);
				switch (verif) {
					case OAuthAppVerify.Error: // Errored
						throw new BadRequestException(ApiResponseError.InvalidInstanceResponse);
						break;
					case OAuthAppVerify.Failed: // Failed to verify app, recreate it and updating application
						{
							const app = await this.generateApplication(domain, true);
							if (app.entity.type === InstanceType.Misskey) {
								throw new ServiceUnavailableException(
									ApiResponseError.UnsupportedInstance,
								);
							}

							redirect_uri = app.url;
						}
						break;
					case OAuthAppVerify.NotFound: // User not existing yet
					case OAuthAppVerify.Success:
						redirect_uri = this.redirect(application, domain);
						break;
				}
			}
		} else {
			const app = await this.generateApplication(domain);
			if (app.entity.type === InstanceType.Misskey) {
				throw new ServiceUnavailableException(ApiResponseError.UnsupportedInstance);
			}

			redirect_uri = app.url;
		}

		const session = await this.OAuthSessionService.create(username, domain, fingerprint, {
			add_account: options?.add_account ?? false,
			to_user_uuid: options?.to_user_uuid ?? '',
			refresh_account: options?.refresh_account ?? false,
			to_account_uuid: options?.to_account_uuid ?? '',
		});
		if (!session) {
			this.logger.error('authorize', `Failed to create session.`);
			throw new InternalServerErrorException();
		}

		const token = await this.oauthToken(session.token_uuid);
		if (!token) {
			this.logger.error('authorize', `Failed to create token.`);
			throw new InternalServerErrorException();
		}

		return {
			token,
			uri: redirect_uri,
		};
	}

	/**
	 * @throws InternalServerErrorException
	 * @throws BadRequestException
	 */
	async authorizeRefesh(
		username: string,
		domain: string,
		fingerprint: Fingerprint,
		user_uuid: string,
	) {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error(
				'authorizeRefesh',
				`Unable to get account to refresh token for @${username}@${domain}`,
			);
			throw new InternalServerErrorException();
		}

		let account = userHasIntance(user, { username, domain });
		if (!account) {
			this.logger.error(
				'authorizeRefesh',
				`Account @${username}@${domain} is not linked to this user`,
			);
			throw new BadRequestException();
		}

		return this.authorize(username, domain, fingerprint, {
			refresh_account: true,
			to_account_uuid: account.uuid,
		});
	}

	/**
	 * @throws InternalServerErrorException
	 * @throws ServiceUnavailableException
	 * @throws BadRequestException
	 */
	async getToken(
		session: OAuthSession,
		code: string,
	): Promise<OAuthSessionToken | OAuthVerify.Failed | OAuthVerify.Unsupported> {
		const application = await this.DBApplicationService.get(session.domain);
		if (!application) {
			this.logger.error('getToken', `Unable to find application for ${session.domain}.`);
			throw new InternalServerErrorException();
		}

		const token = await MegalodonGenerator(application.type, 'https://' + session.domain)
			.fetchAccessToken(application.key, application.secret, code, this.REDIRECT)
			.catch((e: Error) => {
				this.logger.error(`getToken`, e);
				throw new BadRequestException(ApiResponseError.InvalidInstanceResponse);
			});

		const updated = await this.DBApplicationService.updateLastUsage(application);
		if (!updated) {
			this.logger.warn('getToken', `Unable to update access time for ${application.domain}`);
		}

		if (process.env['NODE_ENV'] !== 'test') {
			// Check if token match database account
			const entity_user = await MegalodonGenerator(
				application.type,
				'https://' + application.domain,
				token.access_token,
			)
				.verifyAccountCredentials()
				.then((r) => (r ? r.data : null))
				.catch((e) => {
					this.logger.error(`getToken`, e);
					throw new BadRequestException(ApiResponseError.InvalidInstanceResponse);
				});
			if (!entity_user) {
				this.logger.error('getToken', 'Failed to retrieve token.');
				throw new InternalServerErrorException();
			}

			// Wrong account
			if (entity_user.username !== session.username) {
				return OAuthVerify.Failed;
			}

			let verif: boolean = false;
			switch (application.type) {
				case InstanceType.Misskey:
					return OAuthVerify.Unsupported;
				case InstanceType.Mastodon:
					verif = entity_user.url.includes(application.domain + '/@' + session.username);
					break;
				case InstanceType.Pleroma:
					verif = entity_user.url.includes(
						application.domain + '/users/' + session.username,
					);
					break;
				default:
					const { key, secret, ...filtered } = application;
					this.logger.error('getToken', `Account application is invalid`, filtered);
					throw new InternalServerErrorException();
			}
			// Wrong account
			if (!verif) {
				return OAuthVerify.Failed;
			}
		}

		const account = await this.DBAccountService.get(session.username, session.domain);
		// // At this step database should always have an account
		// if (!account) {
		// 	this.logger.error(
		// 		'getToken',
		// 		`At this step database should always have an account. @${session.username}@${session.domain}`,
		// 	);
		// 	throw new InternalServerErrorException();
		// }

		// Update access token for account if empty
		if (account && !account.token) {
			const account_update = await this.DBAccountService.update(
				session.username,
				session.domain,
				token.access_token,
			);
			if (!account_update) {
				this.logger.error(
					'getToken',
					`Failed to set access token for @${account.username}@${account.application.domain}`,
				);
				throw new InternalServerErrorException();
			}

			// Update all connected users cache
			const users = await this.DBUserService.getInMainOrSecondary(account);
			if (!users) {
				this.logger.error(
					'getToken',
					`Failed to get user for @${account.username}@${account.application.domain}`,
				);
				throw new InternalServerErrorException();
			}

			for (const user of users) {
				await this.UserCacheService.update(user);
			}
		}

		return {
			token: token.access_token,
			username: session.username,
			domain: session.domain,
		};
	}

	async revokeToken(account: AccountStore): Promise<boolean> {
		const application = await this.DBApplicationService.get(account.application.domain);
		if (!application) {
			return false;
		}

		const revoked = await MegalodonGenerator(application.type, 'https://' + application.domain)
			.revokeToken(application.key, application.secret, account.token)
			.catch((e: Error) => {
				this.logger.error(
					`revokeToken`,
					`Unable to revoke token for @${account.username}@${account.application.domain}`,
					e,
				);
				return null;
			});
		if (!revoked) {
			return false;
		}

		return revoked.status === 200;
	}
	//#endregion
}
