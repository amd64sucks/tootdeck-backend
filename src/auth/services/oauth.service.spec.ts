import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';

import { OAuthService } from './oauth.service';
import { OAuthSessionService } from './sessions/oauth.session.service';
import { DBUserService } from '../../database/services/database.user.service';
import { DBApplicationService } from '../../database/services/database.application.service';
import { BrowserFingerprintService } from './utils/fingerprint.service';
import { DBAccountService } from '../../database/services/database.account.service';

import { AccountStore } from '../../database/entities/account.entity';
import { UserStore } from '../../database/entities/user.entity';

import { secretConfig } from '../../app/modules/secret.config';

import { InstanceType } from '../../database/types';
import { Fingerprint } from '../types/fingerprint.types';
import { OAuthAutorizeOptionsAddAccount } from '../types/oauth.types';
import { FullAppModule } from '../../app/app.module';

describe('OAuthService', () => {
	let service: OAuthService;
	let ApplicationDB: DBApplicationService;
	let OAuthSession: OAuthSessionService;
	let AccountDB: DBAccountService;
	let UserDB: DBUserService;
	let Jwt: JwtService;
	let fingerprint: Fingerprint;
	let token: string;
	let account_1: AccountStore;
	let account_2: AccountStore;
	let user_1: UserStore;
	let user_2: UserStore;

	const secret = secretConfig();
	const test_username_1 = 'OAuthService1'.toLocaleLowerCase();
	const test_username_2 = 'OAuthService2'.toLocaleLowerCase();
	const test_domain = 'OAuthService'.toLocaleLowerCase();
	const test_redirect_url =
		'https://oauthservice/oauth/authorize?redirect_uri=https://localhost/api/oauth/authorize/redirect&response_type=code&client_id=key&scope=read%20write%20follow&force_login=true';
	const test_user_agent =
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36';
	const test_ip = '192.168.1.1';

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		service = app.get(OAuthService);
		ApplicationDB = app.get(DBApplicationService);
		OAuthSession = app.get(OAuthSessionService);
		AccountDB = app.get(DBAccountService);
		UserDB = app.get(DBUserService);
		Jwt = app.get(JwtService);

		fingerprint = app.get(BrowserFingerprintService).get(test_user_agent, test_ip);
		await ApplicationDB.create(test_domain, InstanceType.Mastodon, 'key', 'secret');

		account_1 = (await AccountDB.create(test_username_1, test_domain, ''))!;
		account_2 = (await AccountDB.create(test_username_2, test_domain, ''))!;
		user_1 = (await UserDB.create(account_1!))!;
		user_1 = (await UserDB.addAccount(user_1, account_2))!;
		user_2 = (await UserDB.create(account_2!))!;

		if (!account_1 || !account_2 || !user_1 || !user_2) {
			expect(!!account_1).toBe(true);
			expect(!!account_2).toBe(true);
			expect(!!user_1).toBe(true);
			expect(!!user_2).toBe(true);
		}
	});

	afterAll(async () => {
		await UserDB.delete(account_1);
		await UserDB.delete(account_2);
		await AccountDB.delete(account_1.username, account_1.application.domain);
		await AccountDB.delete(account_2.username, account_2.application.domain);
		await ApplicationDB.delete(test_domain);
	});

	describe('authorize', () => {
		const TryCatch = async (
			username: string,
			domain: string,
			fingerprint: Fingerprint,
			options?: OAuthAutorizeOptionsAddAccount,
		) => {
			let result: any;
			let has_throw = false;

			try {
				result = await service.authorize(username, domain, fingerprint, options!);
			} catch (e) {
				has_throw = true;
			}

			return {
				result,
				has_throw,
			};
		};

		it('generate basic redirect url', async () => {
			const { result, has_throw } = await TryCatch(test_username_1, test_domain, fingerprint);

			expect(result).toMatchObject({
				uri: test_redirect_url,
			});
			expect(has_throw).toBe(false);

			token = result.token;
		});

		it('generate wrong add redirect url, should throw', async () => {
			const { result, has_throw } = await TryCatch(
				test_username_1,
				test_domain,
				fingerprint,
				{
					add_account: true,
					to_user_uuid: user_1.uuid,
				},
			);

			expect(has_throw).toBe(true);
		});

		it('generate wrong add redirect url, should throw', async () => {
			const { result, has_throw } = await TryCatch(
				test_username_2,
				test_domain,
				fingerprint,
				{
					add_account: true,
					to_user_uuid: user_1.uuid,
				},
			);

			expect(has_throw).toBe(true);
		});

		it('generate wrong add redirect url, should throw', async () => {
			const { result, has_throw } = await TryCatch(
				test_username_1,
				test_domain,
				fingerprint,
				{
					add_account: true,
					to_user_uuid: '',
				},
			);

			expect(has_throw).toBe(true);
		});
	});

	describe('refresh', () => {
		const TryCatch = async (
			username: string,
			domain: string,
			fingerprint: Fingerprint,
			uuid: string,
		) => {
			let result: any;
			let has_throw = false;

			try {
				result = await service.authorizeRefesh(username, domain, fingerprint, uuid);
			} catch (e) {
				has_throw = true;
			}

			return {
				result,
				has_throw,
			};
		};

		it('generate refresh uri for main account token', async () => {
			const { result, has_throw } = await TryCatch(
				test_username_1,
				test_domain,
				fingerprint,
				user_1.uuid,
			);

			expect(result).toMatchObject({
				uri: test_redirect_url,
			});
			expect(has_throw).toBe(false);
		});

		it('generate refresh uri for secondary account token', async () => {
			const { result, has_throw } = await TryCatch(
				test_username_2,
				test_domain,
				fingerprint,
				user_1.uuid,
			);

			expect(result).toMatchObject({
				uri: test_redirect_url,
			});
			expect(has_throw).toBe(false);
		});

		it('generate wrong refresh uri', async () => {
			const { result, has_throw } = await TryCatch(
				test_username_1,
				test_domain,
				fingerprint,
				'',
			);

			expect(has_throw).toBe(true);
		});

		it('generate wrong refresh uri', async () => {
			const { result, has_throw } = await TryCatch(
				'test_username_2',
				test_domain,
				fingerprint,
				user_2.uuid,
			);

			expect(has_throw).toBe(true);
		});
	});

	describe('validate', () => {
		const TryCatch = async (token: string, fingerprint: Fingerprint) => {
			let result: any;
			let has_throw = false;

			try {
				result = await service.validate(token, fingerprint);
			} catch (e) {
				has_throw = true;
			}

			return {
				result,
				has_throw,
			};
		};

		it('valid token, should return entry', async () => {
			const { result, has_throw } = await TryCatch(token, fingerprint);

			expect(!!result).toBe(true);
			expect(has_throw).toBe(false);
		});

		it('valid token with updated browser, should return entry', async () => {
			const copy = { ...fingerprint };
			copy.browser.version++;

			const { result, has_throw } = await TryCatch(token, copy);

			expect(!!result).toBe(true);
			expect(has_throw).toBe(false);
		});

		it('valid token with downgraded browser, should throw', async () => {
			const copy = { ...fingerprint };
			copy.browser.version--;

			const { result, has_throw } = await TryCatch(token, copy);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});

		it('invalid token, should throw', async () => {
			const { result, has_throw } = await TryCatch(undefined as any, fingerprint);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});

		it('invalid token, should throw', async () => {
			const { result, has_throw } = await TryCatch('invalid', fingerprint);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});

		it('invalid fingerprint, should throw', async () => {
			const copy = { ...fingerprint };
			copy.browser.name = '';

			const { result, has_throw } = await TryCatch(token, copy);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});

		it('invalid token, should throw', async () => {
			const jwt = await Jwt.verifyAsync<{ s: string }>(token, {
				algorithms: ['HS512'],
				secret,
			}).catch((e) => {
				return null;
			});

			if (!jwt) {
				expect(!!jwt).toBe(true);
				return;
			}

			const uuid = jwt.s;
			await OAuthSession.delete(uuid);

			const { result, has_throw } = await TryCatch(token, fingerprint);

			expect(!!result).toBe(false);
			expect(has_throw).toBe(true);
		});
	});
});
