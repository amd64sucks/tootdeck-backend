import { Injectable, UnauthorizedException, InternalServerErrorException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { JWESessionService } from './sessions/jwe.session.service';
import { BrowserFingerprintService } from './utils/fingerprint.service';

import { Logger } from '../../utils/logger';

import { secretConfig } from '../../app/modules/secret.config';
import { JWETime } from '../config';

import { JWEType } from '../types/jwe.types';
import { Fingerprint, FingerprintValid } from '../types/fingerprint.types';
import { JWESession } from '../types/sessions.types';

@Injectable()
export class JWEService {
	private readonly logger = new Logger(JWEService.name);
	private readonly secret: string;

	constructor(
		private readonly JwtService: JwtService,
		private readonly JWESessionService: JWESessionService,
		private readonly BrowserFingerprint: BrowserFingerprintService,
	) {
		this.secret = secretConfig();
		if (!this.secret) {
			this.logger.error('constructor', 'Unable to read secret');
			process.exit(1);
		}
		this.logger.log('constructor', 'Secret loaded');
	}

	/**
	 * Utils
	 */
	//#region

	private async accessToken(token_uuid: string): Promise<string> {
		return this.JwtService.signAsync(
			{ u: token_uuid },
			{
				algorithm: 'HS512',
				expiresIn: JWETime.Access().valueOf(),
				secret: this.secret,
			},
		);
	}

	private async refreshToken(token_uuid: string): Promise<string> {
		return this.JwtService.signAsync(
			{ u: token_uuid },
			{
				algorithm: 'HS512',
				expiresIn: JWETime.Refresh().valueOf(),
				secret: this.secret,
			},
		);
	}

	private async createSessions(user_uuid: string, fingerprint: Fingerprint) {
		const access_session_p = this.JWESessionService.create(
			JWEType.Access,
			user_uuid,
			fingerprint,
		);
		const refresh_session_p = this.JWESessionService.create(
			JWEType.Refresh,
			user_uuid,
			fingerprint,
		);

		return Promise.all([access_session_p, refresh_session_p]);
	}

	private async createTokens(access_session_uuid: string, refresh_session_uuid: string) {
		let access_token_promise = this.accessToken(access_session_uuid);
		let refresh_token_promise = this.refreshToken(refresh_session_uuid);

		return Promise.all([access_token_promise, refresh_token_promise]);
	}

	private async updateSessionWithTokens(
		access: { session: string; token: string },
		refresh: { session: string; token: string },
	) {
		const update_access_session_p = this.JWESessionService.updateJWE(
			JWEType.Access,
			access.session,
			access.token,
			refresh.session,
		);
		const update_refresh_session_p = this.JWESessionService.updateJWE(
			JWEType.Refresh,
			refresh.session,
			refresh.token,
		);

		return Promise.all([update_access_session_p, update_refresh_session_p]);
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	/**
	 * @throws UnauthorizedException
	 * @throws InternalServerErrorException
	 */
	async validate(
		type: JWEType,
		raw_token: string,
		fingerprint: Fingerprint,
	): Promise<JWESession> {
		const jwt = await this.JwtService.verifyAsync<{ u: string }>(raw_token, {
			algorithms: ['HS512'],
			secret: this.secret,
		}).catch((e) => {
			this.logger.warn('validate', 'Invalid JWT: signature verification failed');
			throw new UnauthorizedException();
		});

		const token_uuid = jwt.u;

		const session = await this.JWESessionService.get(type, token_uuid);
		if (!session || !session.token || session.token !== raw_token) {
			this.logger.warn('validate', 'Invalid JWT: no session');
			throw new UnauthorizedException();
		}

		if (type === JWEType.Access) {
			const refresh_session = await this.JWESessionService.get(
				JWEType.Refresh,
				session.linked_to_token_uuid,
			);
			if (!refresh_session) {
				this.logger.warn('validate', 'Invalid JWT: session destroyed');
				throw new UnauthorizedException();
			}
		}

		const valid_fingerprint = this.BrowserFingerprint.validate(
			session.fingerprint,
			fingerprint,
		);

		if (valid_fingerprint === FingerprintValid.False) {
			this.logger.warn('validate', 'Invalid JWT: fingerprint');
			throw new UnauthorizedException();
		}

		if (valid_fingerprint === FingerprintValid.Regenerate) {
			const update = await this.JWESessionService.updateFingerprint(
				type,
				token_uuid,
				fingerprint,
			);
			if (!update) {
				this.logger.error('validate', 'Unable to update fingerprint in JWT session');
				throw new InternalServerErrorException();
			}
			return update;
		}

		return session;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async create(user_uuid: string, fingerprint: Fingerprint): Promise<[string, string]> {
		// Create sessions
		const [access_session, refresh_session] = await this.createSessions(user_uuid, fingerprint);
		if (!access_session || !refresh_session) {
			this.logger.error('create', 'Unable to create JWT sessions');
			throw new InternalServerErrorException();
		}

		// Create tokens
		const [access_token, refresh_token] = await this.createTokens(
			access_session.token_uuid,
			refresh_session.token_uuid,
		);
		if (!access_token || !refresh_token) {
			this.logger.error('create', 'Failed to sign JWTs');
			throw new InternalServerErrorException();
		}

		// Update sessions with tokens
		const update_access_session = await this.updateSessionWithTokens(
			{ session: access_session.token_uuid, token: access_token },
			{ session: refresh_session.token_uuid, token: refresh_token },
		);
		if (!update_access_session) {
			this.logger.error('create', 'Unable to set token in JWT sessions');
			await Promise.all([
				this.JWESessionService.delete(JWEType.Access, access_session.token_uuid),
				this.JWESessionService.delete(JWEType.Refresh, refresh_session.token_uuid),
			]);
			throw new InternalServerErrorException();
		}

		return [access_token, refresh_token];
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async update(session: JWESession, fingerprint: Fingerprint): Promise<[string, string]> {
		const tokens = await this.create(session.user_uuid, fingerprint);

		await Promise.all([
			this.JWESessionService.delete(JWEType.Access, session.linked_to_token_uuid),
			this.JWESessionService.delete(JWEType.Refresh, session.token_uuid),
		]);

		return tokens;
	}

	async delete(session: JWESession): Promise<void> {
		await Promise.all([
			this.JWESessionService.delete(JWEType.Access, session.linked_to_token_uuid),
			this.JWESessionService.delete(JWEType.Refresh, session.token_uuid),
		]);
	}
	//#endregion
}
