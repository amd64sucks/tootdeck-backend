import { Test, TestingModule } from '@nestjs/testing';
import { isUUID } from 'class-validator';
import { randomUUID } from 'crypto';

import { UserDeleteSessionService } from './user.delete.session.service';

import { RedisConfig } from '../../../app/modules/redis.config';

import { RedisDB } from '../../../app/redis.config';

import { UserDeleteSession } from '../../types/sessions.types';

describe('UserDeleteSessionService', () => {
	let service: UserDeleteSessionService;
	let uuid: string;

	const test_uuid = randomUUID();
	const test_token = 'ty0osll63wjgwd3bpj2oz22l4y1tc4c883pvvryi359xx9rdbxj';

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [RedisConfig([RedisDB.UserDelete])],
			providers: [UserDeleteSessionService],
		}).compile();

		service = app.get(UserDeleteSessionService);
	});

	describe('create', () => {
		let entry: UserDeleteSession;

		it('creating valid entry, should return true', async () => {
			const result = await service.create(test_uuid);
			if (!result) {
				expect(!!result).toBe(true);
				return;
			}

			entry = result;

			expect(isUUID(result.token_uuid)).toBe(true);
			expect(test_uuid === result.user_uuid).toBe(true);
			expect(!!result.code).toBe(true);
		});

		it('creating existing entry, should return same entry', async () => {
			const result = await service.create(test_uuid);
			if (!result) {
				expect(!!result).toBe(true);
				return;
			}

			uuid = result.token_uuid;

			expect(result).toMatchObject(entry);
		});
	});

	describe('updateJWE', () => {
		it('getting valid uuid, should return entry', async () => {
			expect(await service.updateJWE(uuid, test_token)).toMatchObject({
				token_uuid: uuid,
				user_uuid: test_uuid,
				token: test_token,
			} as UserDeleteSession);
		});

		it('getting invalid uuid, should return null', async () => {
			expect(await service.updateJWE('nothing', test_token)).toBe(null);
		});

		it('getting invalid uuid, should return null', async () => {
			expect(await service.updateJWE(uuid, '')).toBe(null);
		});
	});

	describe('get', () => {
		it('getting valid uuid, should return entry', async () => {
			expect(await service.get(uuid)).toMatchObject({
				token_uuid: uuid,
				user_uuid: test_uuid,
				token: test_token,
			} as UserDeleteSession);
		});

		it('getting invalid uuid, should return null', async () => {
			expect(await service.get('nothing')).toBe(null);
		});
	});

	describe('delete', () => {
		it('deleting valid entry, should return true', async () => {
			expect(await service.delete(uuid)).toBe(true);
		});

		it('deleting invalid entry, should return false', async () => {
			expect(await service.delete('nothing')).toBe(false);
		});
	});
});
