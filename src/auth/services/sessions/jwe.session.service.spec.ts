import { Test, TestingModule } from '@nestjs/testing';
import { randomUUID } from 'crypto';

import { JWESessionService } from './jwe.session.service';
import { BrowserFingerprintService } from '../utils/fingerprint.service';
import { OpenSessionService } from './open.session.service';

import { RedisConfig } from '../../../app/modules/redis.config';

import { RedisDB } from '../../../app/redis.config';

import { Fingerprint, HashedFingerprint } from '../../types/fingerprint.types';
import { JWEType } from '../../types/jwe.types';
import { JWESession } from '../../types/sessions.types';

describe('JWESessionService', () => {
	let service: JWESessionService;
	let BrowserFingerprint: BrowserFingerprintService;
	let fingerprint: Fingerprint;
	let hashed_fingerprint: HashedFingerprint;
	let access_token_uuid: string;
	let refresh_token_uuid: string;

	const test_user_agent =
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36';
	const test_ip = '192.168.1.1';
	const test_uuid = randomUUID();
	const test_token = 'token';

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [
				RedisConfig([RedisDB.AccessToken, RedisDB.RefreshToken, RedisDB.OpenSession]),
			],
			providers: [JWESessionService, BrowserFingerprintService, OpenSessionService],
		}).compile();

		service = app.get<JWESessionService>(JWESessionService);
		BrowserFingerprint = app.get<BrowserFingerprintService>(BrowserFingerprintService);
		fingerprint = BrowserFingerprint.get(test_user_agent, test_ip);
		hashed_fingerprint = BrowserFingerprint.getHashed(fingerprint);
	});

	describe('create', () => {
		it('creating valid entry, should return true', async () => {
			const result = await service.create(JWEType.Access, test_uuid, fingerprint);

			access_token_uuid = result!.token_uuid;

			expect(!!result?.token_uuid).toBe(true);
			expect(result).toMatchObject({
				user_uuid: test_uuid,
				token: '',
				fingerprint: hashed_fingerprint,
			});
		});

		it('creating valid entry, should return true', async () => {
			const result = await service.create(JWEType.Refresh, test_uuid, fingerprint);

			refresh_token_uuid = result!.token_uuid;

			expect(!!result?.token_uuid).toBe(true);
			expect(result).toMatchObject({
				user_uuid: test_uuid,
				token: '',
				fingerprint: hashed_fingerprint,
			});
		});

		it('creating invalid entry, should return null', async () => {
			const result = await service.create('invalid' as any, test_uuid, fingerprint);

			expect(result).toBe(null);
		});

		it('creating invalid entry, should return null', async () => {
			const result = await service.create(JWEType.Access, '', fingerprint);

			expect(result).toBe(null);
		});

		it('creating invalid entry, should return null', async () => {
			const result = await service.create(JWEType.Refresh, test_uuid, undefined as any);

			expect(result).toBe(null);
		});
	});

	describe('get', () => {
		it('getting valid entry, should return entry', async () => {
			expect(await service.get(JWEType.Access, access_token_uuid)).toMatchObject({
				token_uuid: access_token_uuid,
				user_uuid: test_uuid,
				token: '',
				fingerprint: hashed_fingerprint,
			});
		});

		it('getting valid entry, should return entry', async () => {
			expect(await service.get(JWEType.Refresh, refresh_token_uuid)).toMatchObject({
				token_uuid: refresh_token_uuid,
				user_uuid: test_uuid,
				token: '',
				fingerprint: hashed_fingerprint,
			});
		});

		it('getting invalid entry, should return null', async () => {
			expect(await service.get(JWEType.Access, refresh_token_uuid)).toBe(null);
		});

		it('getting invalid entry, should return null', async () => {
			expect(await service.get(JWEType.Refresh, access_token_uuid)).toBe(null);
		});

		it('getting invalid entry, should return null', async () => {
			expect(await service.get('invalid' as any, access_token_uuid)).toBe(null);
		});
	});

	describe('update', () => {
		describe('token', () => {
			it('update valid entry, should return entry', async () => {
				expect(
					await service.updateJWE(
						JWEType.Access,
						access_token_uuid,
						test_token,
						refresh_token_uuid,
					),
				).toMatchObject({
					token_uuid: access_token_uuid,
					user_uuid: test_uuid,
					token: test_token,
					fingerprint: hashed_fingerprint,
					linked_to_token_uuid: refresh_token_uuid,
				} as JWESession);
			});

			it('update valid entry, should return entry', async () => {
				expect(
					await service.updateJWE(
						JWEType.Refresh,
						refresh_token_uuid,
						test_token,
						access_token_uuid,
					),
				).toMatchObject({
					token_uuid: refresh_token_uuid,
					user_uuid: test_uuid,
					token: test_token,
					fingerprint: hashed_fingerprint,
					linked_to_token_uuid: access_token_uuid,
				} as JWESession);
			});

			it('update invalid entry, should return null', async () => {
				expect(
					await service.updateJWE(
						JWEType.Access,
						refresh_token_uuid,
						test_token,
						access_token_uuid,
					),
				).toBe(null);
			});

			it('update invalid entry, should return null', async () => {
				expect(
					await service.updateJWE(
						JWEType.Refresh,
						access_token_uuid,
						test_token,
						refresh_token_uuid,
					),
				).toBe(null);
			});

			it('update valid entry with invalid data, should return null', async () => {
				expect(
					await service.updateJWE(
						'invalid' as any,
						access_token_uuid,
						test_token,
						refresh_token_uuid,
					),
				).toBe(null);
			});

			it('update valid entry with invalid data, should return null', async () => {
				expect(
					await service.updateJWE(JWEType.Access, '', test_token, refresh_token_uuid),
				).toBe(null);
			});

			it('update valid entry with invalid data, should return null', async () => {
				expect(
					await service.updateJWE(
						JWEType.Access,
						access_token_uuid,
						'',
						refresh_token_uuid,
					),
				).toBe(null);
			});
		});

		describe('fingerprint', () => {
			it('update valid entry, should return entry', async () => {
				expect(
					await service.updateFingerprint(JWEType.Access, access_token_uuid, fingerprint),
				).toMatchObject({
					token_uuid: access_token_uuid,
					user_uuid: test_uuid,
					token: test_token,
					fingerprint: hashed_fingerprint,
					linked_to_token_uuid: refresh_token_uuid,
				} as JWESession);
			});

			it('update valid entry, should return entry', async () => {
				expect(
					await service.updateFingerprint(
						JWEType.Refresh,
						refresh_token_uuid,
						fingerprint,
					),
				).toMatchObject({
					token_uuid: refresh_token_uuid,
					user_uuid: test_uuid,
					token: test_token,
					fingerprint: hashed_fingerprint,
					linked_to_token_uuid: access_token_uuid,
				} as JWESession);
			});

			it('update invalid entry, should return null', async () => {
				expect(
					await service.updateFingerprint(
						JWEType.Access,
						refresh_token_uuid,
						fingerprint,
					),
				).toBe(null);
			});

			it('update invalid entry, should return null', async () => {
				expect(
					await service.updateFingerprint(
						JWEType.Refresh,
						access_token_uuid,
						fingerprint,
					),
				).toBe(null);
			});

			it('update valid entry with invalid data, should return null', async () => {
				expect(
					await service.updateFingerprint(
						'invalid' as any,
						access_token_uuid,
						fingerprint,
					),
				).toBe(null);
			});

			it('update valid entry with invalid data, should return null', async () => {
				expect(await service.updateFingerprint(JWEType.Access, '', fingerprint)).toBe(null);
			});

			it('update valid entry with invalid data, should return null', async () => {
				expect(
					await service.updateFingerprint(
						JWEType.Access,
						access_token_uuid,
						undefined as any,
					),
				).toBe(null);
			});
		});
	});

	describe('delete', () => {
		it('deleting valid entry, should return true', async () => {
			expect(await service.delete(JWEType.Access, access_token_uuid)).toBe(true);
		});

		it('deleting valid entry, should return true', async () => {
			expect(await service.delete(JWEType.Refresh, refresh_token_uuid)).toBe(true);
		});

		it('deleting invalid entry, should return false', async () => {
			expect(await service.delete(JWEType.Access, access_token_uuid)).toBe(false);
		});

		it('deleting invalid entry, should return false', async () => {
			expect(await service.delete(JWEType.Refresh, refresh_token_uuid)).toBe(false);
		});
	});
});
