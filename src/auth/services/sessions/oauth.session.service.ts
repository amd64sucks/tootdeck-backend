import { InjectRedis } from '@liaoliaots/nestjs-redis';
import { Injectable } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { Redis } from 'ioredis';
import { deserialize, serialize } from 'v8';

import { BrowserFingerprintService } from '../utils/fingerprint.service';

import { Logger } from '../../../utils/logger';

import { RedisNamespace } from '../../../app/redis.config';
import { JWETime } from '../../config';

import { OAuthSession } from '../../types/sessions.types';
import { Fingerprint, HashedFingerprint } from '../../types/fingerprint.types';
import { OAuthAutorizeOptions } from '../../types/oauth.types';

@Injectable()
export class OAuthSessionService {
	private readonly logger = new Logger(OAuthSessionService.name);

	constructor(
		@InjectRedis(RedisNamespace.OAuthToken) private readonly redis: Redis,
		private readonly BrowserFingerprint: BrowserFingerprintService,
	) {}

	/**
	 * Utils
	 */
	//#region

	private async seralize(data: OAuthSession): Promise<boolean> {
		const { token_uuid: uuid, domain } = data;
		try {
			return !!(await this.redis.set(
				uuid,
				serialize(data),
				'PXAT',
				JWETime.OAuth().valueOf(),
			));
		} catch (e) {
			this.logger.error(`seralize`, e);
			return false;
		}
	}

	private async deseralize(uuid: string): Promise<OAuthSession | null> {
		try {
			const data = await this.redis.getBuffer(uuid);
			if (!data) {
				return null;
			}
			return deserialize(data);
		} catch (e) {
			this.logger.error(`deseralize`, e);
			return null;
		}
	}

	private async find(
		username: string,
		domain: string,
		hashed_fingerprint: HashedFingerprint,
		options: OAuthAutorizeOptions,
	) {
		const all = await this.redis.scan(0);

		for (const key of all[1]) {
			const found = await this.get(key);
			if (!found) {
				continue;
			}
			if (
				found.username === username &&
				found.domain === domain &&
				found.fingerprint.hash === hashed_fingerprint.hash &&
				found.options.add_account === options.add_account &&
				found.options.to_user_uuid === options.to_user_uuid
			) {
				return found;
			}
		}
		return null;
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	async get(uuid: string): Promise<OAuthSession | null> {
		return this.deseralize(uuid);
	}

	async create(
		username: string,
		domain: string,
		fingerprint: Fingerprint,
		options: OAuthAutorizeOptions,
	): Promise<OAuthSession | null> {
		const hashed = this.BrowserFingerprint.getHashed(fingerprint);

		const session: OAuthSession = {
			token_uuid: randomUUID(),
			username,
			domain,
			fingerprint: hashed,
			options,
		};

		const exist = await this.find(username, domain, hashed, options);
		if (exist) {
			return exist;
		}

		while (!!(await this.deseralize(session.token_uuid))) {
			session.token_uuid = randomUUID();
		}

		const result = await this.seralize(session);
		if (!result) {
			return null;
		}

		return session;
	}

	async delete(uuid: string) {
		try {
			return !!(await this.redis.del(uuid));
		} catch (e) {
			this.logger.error(`delete`, e);
			return false;
		}
	}
	//#endregion
}
