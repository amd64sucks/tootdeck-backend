import { BadRequestException, Inject, Injectable, forwardRef } from '@nestjs/common';
import { InjectRedis } from '@liaoliaots/nestjs-redis';
import { Redis } from 'ioredis';
import { deserialize, serialize } from 'v8';

import { JWESessionService } from './jwe.session.service';

import { Logger } from '../../../utils/logger';
import { RedisDB, RedisNamespace } from '../../../app/redis.config';

import { OpenSession, OpenSessions } from '../../types/sessions.types';
import { Fingerprint } from '../../types/fingerprint.types';
import { JWEType } from '../../types/jwe.types';

@Injectable()
export class OpenSessionService {
	private readonly logger = new Logger(OpenSessionService.name);

	constructor(
		@InjectRedis(RedisNamespace.OpenSession) private readonly redis: Redis,
		@InjectRedis(RedisNamespace.OpenSessionSubscribe) private readonly redisListen: Redis,
		@Inject(forwardRef(() => JWESessionService))
		private readonly JWESessionService: JWESessionService,
	) {
		redisListen.subscribe(`__keyevent@${RedisDB.RefreshToken}__:expired`);
		redisListen.on('message', (_: string, key: string) => {
			console.log('garbage collector trigger called');
			this.removeEntry(key);
		});
	}

	/**
	 * Utils
	 */
	//#region

	private async seralize(user_uuid: string, data: OpenSessions): Promise<OpenSessions | null> {
		try {
			const result = await this.redis.set(user_uuid, serialize(data));
			if (!result) {
				return null;
			}
			return data;
		} catch (e) {
			this.logger.error(`seralize`, e);
			return null;
		}
	}

	private async deseralize(user_uuid: string): Promise<OpenSessions | null> {
		try {
			const data = await this.redis.getBuffer(user_uuid);
			if (!data) {
				return null;
			}
			return deserialize(data);
		} catch (e) {
			this.logger.error(`deseralize`, e);
			return null;
		}
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	async findUser(key_to_find: string) {
		const all = await this.redis.scan(0);

		for (const key of all[1]) {
			const entry = await this.get(key);
			if (!entry) {
				continue;
			}
			const found = entry.find((x) => x.token_uuid === key_to_find);
			if (found) {
				return key;
			}
		}
		return null;
	}

	async removeEntry(key: string) {
		const user = await this.findUser(key);
		if (!user) {
			return false;
		}
		return this.delete(user, key);
	}

	async get(user_uuid: string): Promise<OpenSessions | null> {
		const session = await this.deseralize(user_uuid);
		if (!session) {
			return null;
		}

		return session;
	}

	async create(
		user_uuid: string,
		token_uuid: string,
		fingerprint: Fingerprint,
	): Promise<OpenSessions | null> {
		if (!user_uuid || !token_uuid || !fingerprint) {
			return null;
		}

		const new_session: OpenSession = {
			token_uuid,
			browser: {
				name: fingerprint.browser.name ?? 'unknown',
				version: fingerprint.browser.version,
			},
			os: fingerprint.os.name ?? 'unknown',
			created_at: new Date(),
			updated_at: new Date(),
		};

		let sessions = await this.get(user_uuid);
		if (!sessions) {
			sessions = [new_session];
		} else {
			if (sessions.find((x) => x.token_uuid === token_uuid)) {
				return sessions;
			}
			sessions.push(new_session);
		}

		return this.seralize(user_uuid, sessions);
	}

	async delete(user_uuid: string, token_uuid: string): Promise<boolean> {
		let sessions = await this.get(user_uuid);
		if (!sessions) {
			return false;
		}

		const filter = sessions.filter((x) => x.token_uuid !== token_uuid);
		if (filter.length === sessions.length) {
			return false;
		}
		if (!filter.length) {
			return this.deleteAll(user_uuid);
		}

		return !!(await this.seralize(user_uuid, filter));
	}

	async deleteAll(user_uuid: string): Promise<boolean> {
		try {
			return !!(await this.redis.del(user_uuid));
		} catch (e) {
			this.logger.error(`delete`, e);
			return false;
		}
	}

	/**
	 * @throws BadRequestException
	 */
	async revokeToken(user_uuid: string, token_uuid: string) {
		const refresh_session = await this.JWESessionService.get(JWEType.Refresh, token_uuid);
		if (!refresh_session) {
			// This should already be garbage collected
			// delete() already check if the user match
			const result = await this.delete(user_uuid, token_uuid);
			if (!result) {
				this.logger.warn('revokeToken', 'Unable to garbage collect');
				throw new BadRequestException();
			}
			return;
		}

		// check if the user match
		if (refresh_session.user_uuid !== user_uuid) {
			this.logger.warn('revokeToken', "User doesn't match");
			throw new BadRequestException();
		}

		await Promise.all([
			this.JWESessionService.delete(JWEType.Access, refresh_session?.linked_to_token_uuid),
			this.JWESessionService.delete(JWEType.Refresh, token_uuid),
		]);
	}
	//#endregion
}
