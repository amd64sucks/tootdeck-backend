import { Test, TestingModule } from '@nestjs/testing';
import { isUUID } from 'class-validator';

import { OAuthSessionService } from './oauth.session.service';
import { BrowserFingerprintService } from '../utils/fingerprint.service';

import { RedisConfig } from '../../../app/modules/redis.config';

import { RedisDB } from '../../../app/redis.config';

import { Fingerprint } from '../../types/fingerprint.types';
import { OAuthSession } from '../../types/sessions.types';

describe('OAuthSessionService', () => {
	let service: OAuthSessionService;
	let uuid: string;
	let fingerprint: Fingerprint;

	const test_domain = 'https://localhost';
	const test_username = 'username';
	const test_user_agent =
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36';
	const test_ip = '192.168.1.1';
	const test_hash =
		'f38cd70a78a7dd09c3242b09894dee8d675a7eb0baf444881d03f63ef66e1f99cb9f055ddf0b56c25acd6f1b36aeaa97e5e2beb3d34125aa0022abab1a9c30eb';

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [RedisConfig([RedisDB.OAuthToken])],
			providers: [OAuthSessionService, BrowserFingerprintService],
		}).compile();

		service = app.get(OAuthSessionService);
		fingerprint = app.get(BrowserFingerprintService).get(test_user_agent, test_ip);
	});

	describe('create', () => {
		let entry: OAuthSession;

		it('creating valid entry, should return true', async () => {
			const result = await service.create(test_username, test_domain, fingerprint, {
				add_account: false,
				to_user_uuid: '',
			});
			if (!result) {
				expect(!!result).toBe(true);
				return;
			}

			entry = result;
			uuid = result.token_uuid;

			expect(isUUID(result.token_uuid)).toBe(true);
			expect(test_domain === result.domain).toBe(true);
		});

		it('creating existing entry, should return same entry', async () => {
			const result = await service.create(test_username, test_domain, fingerprint, {
				add_account: false,
				to_user_uuid: '',
			});
			if (!result) {
				expect(!!result).toBe(true);
				return;
			}

			uuid = result.token_uuid;

			expect(result).toMatchObject(entry);
		});
	});

	describe('get', () => {
		it('getting valid uuid, should return entry', async () => {
			expect(await service.get(uuid)).toMatchObject({
				token_uuid: uuid,
				username: test_username,
				domain: test_domain,
				fingerprint: {
					hash: test_hash,
					browser_version: 112,
				},
				options: {
					add_account: false,
					to_user_uuid: '',
				},
			} as OAuthSession);
		});

		it('getting invalid uuid, should return null', async () => {
			expect(await service.get('nothing')).toBe(null);
		});
	});

	describe('delete', () => {
		it('deleting valid entry, should return true', async () => {
			expect(await service.delete(uuid)).toBe(true);
		});

		it('deleting invalid entry, should return false', async () => {
			expect(await service.delete('nothing')).toBe(false);
		});
	});
});
