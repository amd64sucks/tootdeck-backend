import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { randomUUID } from 'crypto';

import { JWESessionService } from './jwe.session.service';
import { BrowserFingerprintService } from '../utils/fingerprint.service';
import { JWEService } from '../jwe.service';
import { OpenSessionService } from './open.session.service';

import { RedisConfig } from '../../../app/modules/redis.config';

import { RedisDB } from '../../../app/redis.config';
import { secretConfig } from '../../../app/modules/secret.config';

import { Fingerprint } from '../../types/fingerprint.types';
import { OpenSession } from '../../types/sessions.types';

describe('OpenSessionService', () => {
	let service: OpenSessionService;
	let Jwt: JwtService;
	let JWE: JWEService;
	let BrowserFingerprint: BrowserFingerprintService;
	let fingerprint: Fingerprint;
	let refresh_token_uuid: string;

	const secret = secretConfig();
	const test_user_agent =
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36';
	const test_ip = '192.168.1.1';
	const test_uuid = randomUUID();

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [
				JwtModule,
				RedisConfig([RedisDB.AccessToken, RedisDB.RefreshToken, RedisDB.OpenSession]),
			],
			providers: [
				JWEService,
				JWESessionService,
				BrowserFingerprintService,
				OpenSessionService,
			],
		}).compile();

		Jwt = app.get(JwtService);
		service = app.get(OpenSessionService);
		JWE = app.get(JWEService);
		BrowserFingerprint = app.get(BrowserFingerprintService);
		fingerprint = BrowserFingerprint.get(test_user_agent, test_ip);
	});

	const parseToken = async (token: string) => {
		return Jwt.verifyAsync(token, {
			algorithms: ['HS512'],
			secret,
		})
			.then((r) => r.u)
			.catch((e) => {
				return null;
			});
	};

	describe('create', () => {
		it('creating valid entry, should return true', async () => {
			const [access_token, refresh_token] = await JWE.create(test_uuid, fingerprint);
			if (!refresh_token) {
				expect(!!refresh_token).toBe(true);
				return;
			}

			refresh_token_uuid = await parseToken(refresh_token);

			const session = await service.get(test_uuid);
			if (!session) {
				expect(!!session).toBe(true);
				return;
			}

			const entry = session.find((x) => x.token_uuid === refresh_token_uuid);
			expect(entry).toMatchObject({
				token_uuid: refresh_token_uuid,
				browser: {
					name: fingerprint.browser.name,
					version: fingerprint.browser.version,
				},
				os: fingerprint.os.name,
			} as OpenSession);
		});

		it('creating invalid entry, should return null', async () => {
			expect(!!(await service.create('', refresh_token_uuid, fingerprint))).toBe(false);
		});

		it('creating invalid entry, should return null', async () => {
			expect(!!(await service.create(test_uuid, '', fingerprint))).toBe(false);
		});

		it('creating invalid entry, should return null', async () => {
			expect(!!(await service.create(test_uuid, refresh_token_uuid, undefined as any))).toBe(
				false,
			);
		});
	});

	describe('findUser', () => {
		it('getting valid entry, should return key', async () => {
			const key = await service.findUser(refresh_token_uuid);
			expect(key).toBe(test_uuid);
		});

		it('getting invalid entry, should return null', async () => {
			const key = await service.findUser(test_uuid);
			expect(key).toBe(null);
		});
	});

	describe('get', () => {
		it('getting valid entry, should return entry', async () => {
			const session = await service.get(test_uuid);
			if (!session) {
				expect(!!session).toBe(true);
				return;
			}

			const entry = session.find((x) => x.token_uuid === refresh_token_uuid);
			expect(entry).toMatchObject({
				token_uuid: refresh_token_uuid,
				browser: {
					name: fingerprint.browser.name,
					version: fingerprint.browser.version,
				},
				os: fingerprint.os.name,
			} as OpenSession);
		});

		it('getting invalid entry, should return null', async () => {
			const session = await service.get(randomUUID());

			expect(session).toBe(null);
		});
	});

	describe('removeEntry', () => {
		it('remove valid entry, should return true', async () => {
			expect(await service.removeEntry(refresh_token_uuid)).toBe(true);
		});

		it('remove invalid entry, should return false', async () => {
			expect(await service.removeEntry(randomUUID())).toBe(false);
		});
	});

	describe('delete', () => {
		it('deleting valid entry, should return true', async () => {
			const [access_token, refresh_token] = await JWE.create(test_uuid, fingerprint);
			if (!refresh_token) {
				expect(!!refresh_token).toBe(true);
				return;
			}

			refresh_token_uuid = await parseToken(refresh_token);

			expect(await service.delete(test_uuid, refresh_token_uuid)).toBe(true);
		});

		it('deleting invalid entry, should return false', async () => {
			expect(await service.delete(randomUUID(), refresh_token_uuid)).toBe(false);
		});

		it('deleting invalid entry, should return false', async () => {
			expect(await service.delete(test_uuid, randomUUID())).toBe(false);
		});
	});

	describe('deleteAll', () => {
		it('deleting valid entry, should return true', async () => {
			const [access_token, refresh_token] = await JWE.create(test_uuid, fingerprint);
			if (!refresh_token) {
				expect(!!refresh_token).toBe(true);
				return;
			}

			expect(await service.deleteAll(test_uuid)).toBe(true);
		});

		it('deleting deleted entry, should return false', async () => {
			expect(await service.deleteAll(test_uuid)).toBe(false);
		});

		it('deleting invalid entry, should return false', async () => {
			expect(await service.deleteAll(randomUUID())).toBe(false);
		});
	});

	describe('revokeToken', () => {
		let token: string;

		const TryCatch = async (user_uuid: string, token_uuid: string) => {
			let result: any;
			let has_throw = false;

			try {
				result = await service.revokeToken(user_uuid, token_uuid);
			} catch (e) {
				has_throw = true;
			}

			return {
				result,
				has_throw,
			};
		};

		it('remove valid entry with invalid user, should throw', async () => {
			const [access_token, refresh_token] = await JWE.create(test_uuid, fingerprint);
			if (!refresh_token) {
				expect(!!refresh_token).toBe(true);
				return;
			}

			token = await parseToken(refresh_token);

			const { result, has_throw } = await TryCatch(randomUUID(), token);
			expect(has_throw).toBe(true);
		});

		it('remove invalid entry, should throw throw', async () => {
			const { result, has_throw } = await TryCatch(test_uuid, randomUUID());
			expect(has_throw).toBe(true);
		});

		it('remove valid entry, should not throw', async () => {
			const { result, has_throw } = await TryCatch(test_uuid, token);
			expect(has_throw).toBe(false);
		});
	});
});
