import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { InjectRedis } from '@liaoliaots/nestjs-redis';
import { randomUUID } from 'crypto';
import { Redis } from 'ioredis';
import { deserialize, serialize } from 'v8';

import { OpenSessionService } from './open.session.service';
import { BrowserFingerprintService } from '../utils/fingerprint.service';

import { Logger } from '../../../utils/logger';

import { RedisNamespace } from '../../../app/redis.config';
import { JWETime } from '../../config';

import { JWESession } from '../../types/sessions.types';
import { JWEType } from '../../types/jwe.types';
import { Fingerprint } from '../../types/fingerprint.types';

@Injectable()
export class JWESessionService {
	private readonly logger = new Logger(JWESessionService.name);

	constructor(
		@InjectRedis(RedisNamespace.AccessToken)
		private readonly redis_access: Redis,
		@InjectRedis(RedisNamespace.RefreshToken)
		private readonly redis_refresh: Redis,
		@Inject(forwardRef(() => OpenSessionService))
		private readonly OpenSessionService: OpenSessionService,
		private readonly BrowserFingerprint: BrowserFingerprintService,
	) {
		redis_refresh.on('ready', () => {
			redis_refresh.config('SET', 'notify-keyspace-events', 'Ex');
		});
	}

	/**
	 * Utils
	 */
	//#region

	private async _seralize(redis: Redis, data: JWESession, expires: number): Promise<boolean> {
		const { token_uuid } = data;
		try {
			return !!(await redis.set(token_uuid, serialize(data), 'PXAT', expires));
		} catch (e) {
			this.logger.error(`_seralize`, e);
			return false;
		}
	}

	private async _deseralize(redis: Redis, uuid: string): Promise<JWESession | null> {
		try {
			const data = await redis.getBuffer(uuid);
			if (!data) {
				return null;
			}
			return deserialize(data);
		} catch (e) {
			this.logger.error(`_deseralize`, e);
			return null;
		}
	}

	private async seralize(type: JWEType, data: JWESession): Promise<JWESession | null> {
		let result: boolean = false;

		let expires = 0;
		switch (type) {
			case JWEType.Access:
				expires = JWETime.Access().valueOf();
				result = await this._seralize(this.redis_access, data, expires);
				break;
			case JWEType.Refresh:
				expires = JWETime.Refresh().valueOf();
				result = await this._seralize(this.redis_refresh, data, expires);
				break;
			default:
				return null;
		}
		if (!result) {
			return null;
		}

		return data;
	}

	private async deseralize(type: JWEType, uuid: string) {
		let result: JWESession | null = null;

		switch (type) {
			case JWEType.Access:
				result = await this._deseralize(this.redis_access, uuid);
				break;
			case JWEType.Refresh:
				result = await this._deseralize(this.redis_refresh, uuid);
				break;
			default:
				return null;
		}

		return result;
	}
	//#endregion

	/**
	 * Service
	 */
	//#region

	async get(type: JWEType, token_uuid: string): Promise<JWESession | null> {
		let session: JWESession | null = null;

		session = await this.deseralize(type, token_uuid);
		if (!session) {
			return null;
		}

		return session;
	}

	async create(
		type: JWEType,
		user_uuid: string,
		fingerprint: Fingerprint,
	): Promise<JWESession | null> {
		if (!user_uuid || !fingerprint) {
			return null;
		}

		let token_uuid = randomUUID();
		while (!!(await this.get(type, token_uuid))) {
			token_uuid = randomUUID();
		}

		const hashed_fingerprint = this.BrowserFingerprint.getHashed(fingerprint);
		const session: JWESession = {
			token_uuid,
			user_uuid,
			token: '',
			fingerprint: hashed_fingerprint,
			linked_to_token_uuid: '',
		};

		const add = await this.seralize(type, session);
		if (type === JWEType.Refresh && add) {
			const open_session = await this.OpenSessionService.create(
				user_uuid,
				token_uuid,
				fingerprint,
			);
			if (!open_session) {
				this.logger.warn(
					'create',
					`Failed to add token ${token_uuid} in Redis OpenSession`,
				);
			}
		}

		return add;
	}

	async updateJWE(
		type: JWEType,
		token_uuid: string,
		token: string,
		linked?: string,
	): Promise<JWESession | null> {
		const session = await this.get(type, token_uuid);
		if (!session || !token_uuid || !token) {
			return null;
		}

		session.token = token;
		if (linked) {
			session.linked_to_token_uuid = linked;
		}

		return this.seralize(type, session);
	}

	async updateFingerprint(
		type: JWEType,
		token_uuid: string,
		fingerprint: Fingerprint,
	): Promise<JWESession | null> {
		const session = await this.get(type, token_uuid);
		if (!session || !token_uuid || !fingerprint) {
			return null;
		}

		session.fingerprint = this.BrowserFingerprint.getHashed(fingerprint);

		return this.seralize(type, session);
	}

	async delete(type: JWEType, token_uuid: string): Promise<boolean> {
		try {
			let ret: Buffer | null = null;
			switch (type) {
				case JWEType.Access:
					ret = await this.redis_access.getdelBuffer(token_uuid);
					break;
				case JWEType.Refresh:
					ret = await this.redis_refresh.getdelBuffer(token_uuid);
					break;
			}
			if (type === JWEType.Refresh && ret) {
				const parse = deserialize(ret) as JWESession;
				const open_session = await this.OpenSessionService.delete(
					parse.user_uuid,
					token_uuid,
				);
				if (!open_session) {
					this.logger.warn(
						'delete',
						`Failed to remove token ${token_uuid} in Redis OpenSession`,
					);
				}
			}
			return !!ret;
		} catch (e) {
			this.logger.error(`delete`, e);
			return false;
		}
	}
	//#endregion
}
