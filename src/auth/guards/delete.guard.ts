import {
	CanActivate,
	ExecutionContext,
	Injectable,
	InternalServerErrorException,
	UnauthorizedException,
} from '@nestjs/common';

import { Logger } from '../../utils/logger';

import { UserDeleteService } from '../../user/services/user.delete.service';

import { RequestAccessGuard, RequestAccountDeleteGuard } from '../types/guards.types';

/**
 * @requires AccessGuard
 */
@Injectable()
export class AccountDeleteGuard implements CanActivate {
	private readonly logger = new Logger(AccountDeleteGuard.name);

	constructor(private readonly AccountDeleteService: UserDeleteService) {}

	/**
	 * @throws UnauthorizedException
	 * @throws InternalServerErrorException
	 */
	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest() as RequestAccessGuard;

		if (!request.access_session) {
			this.logger.error(null, 'Guard is called without AccessGuard');
			throw new InternalServerErrorException();
		}

		const token = request.cookies['d'] as string;
		if (!token) {
			this.logger.warn('AccountDeleteGuard', 'No token');
			throw new UnauthorizedException();
		}

		const session = await this.AccountDeleteService.validate(token);
		if (!session) {
			this.logger.verbose(null, 'Invalid JWT [4]');
			throw new UnauthorizedException();
		}

		(request as RequestAccountDeleteGuard)['delete_session'] = session;

		return true;
	}
}
