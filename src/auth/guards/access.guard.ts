import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { FastifyRequest } from 'fastify';

import { JWEService } from '../services/jwe.service';
import { BrowserFingerprintService } from '../services/utils/fingerprint.service';

import { Logger } from '../../utils/logger';

import { RequestAccessGuard } from '../types/guards.types';
import { JWEType } from '../types/jwe.types';

@Injectable()
export class AccessGuard implements CanActivate {
	private readonly logger = new Logger(AccessGuard.name);

	constructor(
		private readonly JWEService: JWEService,
		private readonly BrowserFingerprint: BrowserFingerprintService,
	) {}

	/**
	 * @throws UnauthorizedException
	 * @throws InternalServerErrorException
	 */
	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest() as FastifyRequest;

		const token = request.cookies['a'];

		if (!token) {
			this.logger.warn('AccessGuard', 'No token');
			throw new UnauthorizedException();
		}

		// Get fingerprint
		const user_agent = request.headers['user-agent'];
		const ip = request.headers['x-real-ip'] as string | undefined;
		const fingerprint = this.BrowserFingerprint.get(user_agent, ip);

		const session = await this.JWEService.validate(JWEType.Access, token, fingerprint);
		if (!session) {
			throw new UnauthorizedException();
		}

		// (request as RequestWFingerprint)['fingerprint'] = fingerprint;
		(request as RequestAccessGuard)['access_session'] = session;

		return true;
	}
}
