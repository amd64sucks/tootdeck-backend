export const JWETime = {
	Access: (new_date = new Date()) => new Date(new_date.valueOf() + 60 * 20 * 1000), // 20m
	Refresh: (new_date = new Date()) => new Date(new_date.valueOf() + 60 * 60 * 24 * 2 * 1000), // 2d
	OAuth: (new_date = new Date()) => new Date(new_date.valueOf() + 60 * 10 * 1000), // 10m
	Delete: (new_date = new Date()) => new Date(new_date.valueOf() + 60 * 2 * 1000), // 2m
};
