import {
	Controller,
	Delete,
	Get,
	HttpCode,
	Post,
	Request,
	Response,
	UseGuards,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { FastifyReply } from 'fastify';

import { AccessGuard } from '../guards/access.guard';
import { RefreshGuard } from '../guards/refresh.guard';

import { JWEService } from '../services/jwe.service';
import { CookieService } from '../services/utils/cookie.service';
import { UserService } from '../../user/services/user.service';

import { AuthGetResponse } from '../properties/auth.get.property';

import { RequestAccessGuard, RequestRefreshGuard } from '../types/guards.types';
import { JWEType } from '../types/jwe.types';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
	constructor(
		private readonly UserService: UserService,
		private readonly JWEService: JWEService,
		private readonly CookieService: CookieService,
	) {}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200, type: AuthGetResponse })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500 })
	@HttpCode(200)
	@Get('me')
	async me(@Request() request: RequestAccessGuard) {
		return this.UserService.get(request.access_session.user_uuid);
	}

	@UseGuards(RefreshGuard)
	@ApiResponse({ status: 200 })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500 })
	@HttpCode(200)
	@Post('refresh')
	async refresh(
		@Request() request: RequestRefreshGuard,
		@Response({ passthrough: true }) response: FastifyReply,
	) {
		const session = request.refresh_session;
		const fingerprint = request.fingerprint;

		const [access_token, refresh_token] = await this.JWEService.update(session, fingerprint);
		this.CookieService.create(response, JWEType.Access, access_token);
		this.CookieService.create(response, JWEType.Refresh, refresh_token);
	}

	@UseGuards(RefreshGuard)
	@ApiResponse({ status: 200 })
	@ApiResponse({ status: 401 })
	@HttpCode(200)
	@Delete('disconnect')
	async disconnect(
		@Request() request: RequestRefreshGuard,
		@Response({ passthrough: true }) response: FastifyReply,
	) {
		const session = request.refresh_session;

		await this.JWEService.delete(session);
		this.CookieService.delete(response, JWEType.Access);
		this.CookieService.delete(response, JWEType.Refresh);
	}
}
