import {
	BadRequestException,
	Controller,
	Get,
	HttpCode,
	Query,
	Request,
	Response,
	UseGuards,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { FastifyReply, FastifyRequest } from 'fastify';

import { OAuthService } from '../services/oauth.service';
import { CookieService } from '../services/utils/cookie.service';
import { BrowserFingerprintService } from '../services/utils/fingerprint.service';
import { UserService } from '../../user/services/user.service';
import { JWEService } from '../services/jwe.service';

import { Logger } from '../../utils/logger';
import { parseHandle } from '../../utils/parse.handle';

import { AccessGuard } from '../guards/access.guard';
import { OAuthGuard } from '../guards/oauth.guard';

import { OAuthAutorizeResponse } from '../properties/oauth.autorize.property';

import { ApiResponseError } from '../types/api.types';
import { JWEType } from '../types/jwe.types';
import { RequestAccessGuard, RequestOAuthGuard } from '../types/guards.types';
import {
	OAuthAutorizeOptionsAddAccount,
	OAuthAutorizeOptionsRefreshAccount,
	OAuthVerify,
} from '../types/oauth.types';

@ApiTags('Auth · OAuth')
@Controller('oauth')
export class OAuthController {
	private readonly logger = new Logger(OAuthController.name);
	private readonly DOMAIN: string;

	constructor(
		private readonly configService: ConfigService,
		private readonly OAuthService: OAuthService,
		private readonly UserService: UserService,
		private readonly JWEService: JWEService,
		private readonly CookieService: CookieService,
		private readonly BrowserFingerprint: BrowserFingerprintService,
	) {
		this.DOMAIN = this.configService.get<string>('DOMAIN') ?? '';
	}

	@ApiResponse({ status: 200, description: 'Redirect uri', type: OAuthAutorizeResponse })
	@ApiResponse({ status: 400.1, description: ApiResponseError.InvalidDomain })
	@ApiResponse({ status: 400.2, description: ApiResponseError.InvalidInstance })
	@ApiResponse({ status: 400.3, description: ApiResponseError.InvalidInstanceResponse })
	@ApiResponse({ status: 500, description: ApiResponseError.GenerateRedirectFail })
	@ApiResponse({ status: 503, description: ApiResponseError.UnsupportedInstance })
	@HttpCode(200)
	@Get('authorize')
	async authorize(
		@Request() request: FastifyRequest,
		@Response({ passthrough: true }) response: FastifyReply,
		@Query('handle') handle: string,
	): Promise<OAuthAutorizeResponse> {
		const { username, domain } = parseHandle(handle);

		// fingerprint
		const user_agent = request.headers['user-agent'];
		const ip = request.headers['x-real-ip'] as string | undefined;
		const fingerprint = this.BrowserFingerprint.get(user_agent, ip);

		const authorize = await this.OAuthService.authorize(username, domain, fingerprint);

		this.CookieService.create(response, JWEType.OAuth, authorize.token);

		return { uri: authorize.uri };
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200, description: 'Redirect uri', type: OAuthAutorizeResponse })
	@ApiResponse({ status: 400.1, description: ApiResponseError.InvalidDomain })
	@ApiResponse({ status: 400.2, description: ApiResponseError.InvalidInstance })
	@ApiResponse({ status: 400.3, description: ApiResponseError.InvalidInstanceResponse })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500, description: ApiResponseError.GenerateRedirectFail })
	@ApiResponse({ status: 503, description: ApiResponseError.UnsupportedInstance })
	@HttpCode(200)
	@Get('authorize/add')
	async authorizeAdd(
		@Request() request: RequestAccessGuard,
		@Response({ passthrough: true }) response: FastifyReply,
		@Query('handle') handle: string,
	): Promise<OAuthAutorizeResponse> {
		const { username, domain } = parseHandle(handle);

		// fingerprint
		const user_agent = request.headers['user-agent'];
		const ip = request.headers['x-real-ip'] as string | undefined;
		const fingerprint = this.BrowserFingerprint.get(user_agent, ip);

		const authorize = await this.OAuthService.authorize(username, domain, fingerprint, {
			add_account: true,
			to_user_uuid: request.access_session.user_uuid,
		});

		this.CookieService.create(response, JWEType.OAuth, authorize.token);

		return { uri: authorize.uri };
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200, description: 'Redirect uri', type: OAuthAutorizeResponse })
	@ApiResponse({ status: 400.1, description: ApiResponseError.InvalidDomain })
	@ApiResponse({ status: 400.2, description: ApiResponseError.InvalidInstance })
	@ApiResponse({ status: 400.3, description: ApiResponseError.InvalidInstanceResponse })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500, description: ApiResponseError.GenerateRedirectFail })
	@ApiResponse({ status: 503, description: ApiResponseError.UnsupportedInstance })
	@HttpCode(200)
	@Get('authorize/refresh')
	async authorizeRefresh(
		@Request() request: RequestAccessGuard,
		@Response({ passthrough: true }) response: FastifyReply,
		@Query('handle') handle: string,
	): Promise<OAuthAutorizeResponse> {
		const { username, domain } = parseHandle(handle);

		// fingerprint
		const user_agent = request.headers['user-agent'];
		const ip = request.headers['x-real-ip'] as string | undefined;
		const fingerprint = this.BrowserFingerprint.get(user_agent, ip);

		const authorize = await this.OAuthService.authorizeRefesh(
			username,
			domain,
			fingerprint,
			request.access_session.user_uuid,
		);

		this.CookieService.create(response, JWEType.OAuth, authorize.token);

		return { uri: authorize.uri };
	}

	@UseGuards(OAuthGuard)
	@ApiResponse({ status: 302, description: 'Authenticated' })
	@ApiResponse({ status: 400 })
	@ApiResponse({ status: 500 })
	@HttpCode(302)
	@Get('authorize/redirect')
	async redirect(
		@Request() request: RequestOAuthGuard,
		@Response({ passthrough: true }) response: FastifyReply,
		@Query('code') code: string,
		@Query('error') error: string,
	) {
		if (error) {
			return response
				.status(302)
				.redirect(
					`https://${this.DOMAIN}/?message_type=0&message="Authorization hasn't been granted."`,
				);
		}

		const session = request.oauth_session;
		const fingerprint = request.fingerprint;

		if (!code) {
			this.logger.warn('redirect', 'No code');
			throw new BadRequestException();
		}

		// Get token
		const session_token = await this.OAuthService.getToken(session, code);
		switch (session_token) {
			// Misskey or other unsupported instance
			case OAuthVerify.Unsupported:
				return response
					.status(302)
					.redirect(
						`https://${this.DOMAIN}/?message_type=0&message="Unsupported instance type."`,
					);
			// Wrong account
			case OAuthVerify.Failed:
				return response
					.status(302)
					.redirect(
						`https://${this.DOMAIN}/?message_type=0&message="Handle provided doesn't match currently logged account on ${session.domain}."`,
					);
		}

		// Refresh token for account
		if (session.options.refresh_account) {
			await this.UserService.refresh(
				session_token,
				(session.options as OAuthAutorizeOptionsRefreshAccount).to_account_uuid,
			);
			return response.status(302).redirect(`https://${this.DOMAIN}/`);
		}

		// Add account to user
		if (session.options.add_account) {
			await this.UserService.add(
				session_token,
				(session.options as OAuthAutorizeOptionsAddAccount).to_user_uuid,
			);
			return response.status(302).redirect(`https://${this.DOMAIN}/`);
		}

		// Create user if not existing and then log user
		const user_uuid = await this.UserService.login(session_token);

		// Get tokens
		const [access, refresh] = await this.JWEService.create(user_uuid, fingerprint);

		// Set tokens
		this.CookieService.create(response, JWEType.Access, access);
		this.CookieService.create(response, JWEType.Refresh, refresh);

		// Remove oauth token
		this.CookieService.delete(response, JWEType.OAuth);

		return response.status(302).redirect(`https://${this.DOMAIN}/`);
	}
}
