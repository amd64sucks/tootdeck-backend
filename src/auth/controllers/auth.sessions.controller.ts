import {
	BadRequestException,
	Controller,
	Delete,
	Get,
	HttpCode,
	Param,
	Request,
	UseGuards,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { isUUID } from 'class-validator';

import { OpenSessionService } from '../services/sessions/open.session.service';

import { AccessGuard } from '../guards/access.guard';

import { OpenSessionResponse } from '../properties/auth.sessions.get.property';

import { RequestAccessGuard } from '../types/guards.types';

@ApiTags('Auth · Sessions')
@Controller('auth/sessions')
export class AuthSessionsController {
	constructor(private readonly OpenSessionService: OpenSessionService) {}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200, type: [OpenSessionResponse] })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500 })
	@HttpCode(200)
	@Get()
	async get(@Request() request: RequestAccessGuard) {
		const user_uuid = request.access_session.user_uuid;
		const token_uuid = request.access_session.linked_to_token_uuid;

		const open_session = (await this.OpenSessionService.get(user_uuid)) || [];

		return open_session
			.map((x) => {
				if (x.token_uuid === token_uuid) {
					return { current: true, ...x };
				}
				return { current: false, ...x };
			})
			.sort((a, b) => {
				// Sort by state
				if (a.current && !b.current) {
					return -1;
				}
				if (!a.current && b.current) {
					return 1;
				}

				// Sort by date
				if (a.created_at > b.created_at) {
					return -1;
				}
				if (a.created_at < b.created_at) {
					return 1;
				}

				return 0;
			});
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200 })
	@ApiResponse({ status: 400 })
	@ApiResponse({ status: 401 })
	@HttpCode(200)
	@Delete(':token_uuid')
	async delete(@Request() request: RequestAccessGuard, @Param('token_uuid') token_uuid: string) {
		if (!token_uuid || !isUUID(token_uuid)) {
			throw new BadRequestException();
		}

		const user_uuid = request.access_session.user_uuid;

		return this.OpenSessionService.revokeToken(user_uuid, token_uuid);
	}
}
