import {
	ExceptionFilter,
	ArgumentsHost,
	InternalServerErrorException,
	Catch,
} from '@nestjs/common';
import { FastifyReply } from 'fastify';

@Catch()
export class AxiosErrorFilter implements ExceptionFilter {
	catch(exception: any, host: ArgumentsHost) {
		const ctx = host.switchToHttp();
		const response = ctx.getResponse() as FastifyReply;

		/**
		 * exception.constructor.name = 'AxiosError'
		 * exception instanceof AxiosError = false
		 * View comment below to have an example
		 */
		if (exception.constructor.name !== 'AxiosError') {
			if (exception.status && exception.response) {
				response.status(exception.status).send(exception.response);
			} else {
				throw exception;
			}
			return;
		}

		if (!exception.response || !exception.response.status) {
			throw new InternalServerErrorException();
		}

		const { status, statusText, data } = exception.response;

		response
			.status(status)
			.header('X-RateLimit-reset', exception.response.headers['x-ratelimit-reset'])
			.send({ mirror: true, statusCode: status, message: statusText, detail: data });
	}
}

/**
message: 'Request failed with status code 404',
name: 'AxiosError',
stack: 'AxiosError: Request failed with status code 404\n    at settle (/app/node_modules/megalodon/node_modules/axios/lib/core/settle.js:19:12)\n    at BrotliDecompress.handleStreamEnd (/app/node_modules/megalodon/node_modules/axios/lib/adapters/http.js:556:11)\n    at BrotliDecompress.emit (node:events:525:35)\n    at endReadableNT (node:internal/streams/readable:1359:12)\n    at processTicksAndRejections (node:internal/process/task_queues:82:21)',
config: {
	transitional: {
		silentJSONParsing: true,
		forcedJSONParsing: true,
		clarifyTimeoutError: false,
	},
	adapter: ['xhr', 'http'],
	transformRequest: [null],
	transformResponse: [null],
	timeout: 0,
	xsrfCookieName: 'XSRF-TOKEN',
	xsrfHeaderName: 'X-XSRF-TOKEN',
	maxContentLength: null,
	maxBodyLength: null,
	env: {},
	headers: {
		Accept: 'application/json, text/plain',
		Authorization: 'Bearer M6I2lEa0g6JpYBj6ETXby23e8w4rWB4KVD2lX9UzSjo',
		'User-Agent': 'axios/1.3.4',
		'Accept-Encoding': 'gzip, compress, deflate, br',
	},
	signal: {},
	params: {},
	method: 'get',
	url: 'https://mstdn.shalyu.run/api/v1/accounts/1',
},
code: 'ERR_BAD_REQUEST',
status: 404,
}
*/
