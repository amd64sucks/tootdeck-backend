import { MegalodonInterface } from 'megalodon';

import { RequestAccessGuard } from '../../auth/types/guards.types';

export interface RequestMirror extends RequestAccessGuard {
	instance: MegalodonInterface;
}
