import {
	CanActivate,
	ExecutionContext,
	ForbiddenException,
	Injectable,
	InternalServerErrorException,
} from '@nestjs/common';
import MegalodonGenerator from 'megalodon';

import { UserCacheService } from '../../user/services/user.cache.service';

import { Logger } from '../../utils/logger';
import { userHasIntance } from '../../utils/userHasInstance';
import { parseHandle } from '../../utils/parse.handle';

import { RequestAccessGuard } from '../../auth/types/guards.types';
import { RequestMirror } from './guards.types';
import { InstanceHandle } from '../../utils/handle.types';

@Injectable()
export class MirrorGuard implements CanActivate {
	private readonly logger = new Logger(MirrorGuard.name);

	constructor(private readonly UserCacheService: UserCacheService) {}

	/**
	 * @throws UnauthorizedException
	 * @throws InternalServerErrorException
	 */
	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest() as RequestAccessGuard;

		if (!request.access_session) {
			this.logger.error(null, 'Guard is called without AccessGuard');
			throw new InternalServerErrorException();
		}

		const raw_handle = request.headers.authorization?.substring(7);
		let handle: InstanceHandle;
		try {
			handle = parseHandle(raw_handle);
		} catch (e) {
			this.logger.verbose(null, 'Failed to parse handle ' + raw_handle);
			throw new ForbiddenException();
		}

		const user_uuid = request.access_session.user_uuid;
		const user = await this.UserCacheService.get(user_uuid);
		if (!user) {
			this.logger.error(null, 'Failed to get user from cache');
			throw new InternalServerErrorException();
		}

		const account = userHasIntance(user, handle);
		if (!account) {
			this.logger.verbose(
				null,
				`User @${user.main.username}@${user.main.application.domain} isn't linked to ${raw_handle}`,
			);
			throw new ForbiddenException();
		}

		const instance = MegalodonGenerator(
			account.application.type,
			'https://' + account.application.domain,
			account.token,
		);

		(request as RequestMirror)['instance'] = instance;

		return true;
	}
}
