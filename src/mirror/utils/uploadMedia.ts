import { Mastodon, MegalodonInterface, Misskey, Pleroma } from 'megalodon';
import MastodonAPI from 'megalodon/lib/src/mastodon/api_client';
import MisskeyAPI from 'megalodon/lib/src/misskey/api_client';
import PleromaAPI from 'megalodon/lib/src/pleroma/api_client';

import { InstanceType } from '../../database/types';

interface UploadOptional {
	description?: string;
	focus?: string;
}

export function detectInterfaceInstance(megalodon: MegalodonInterface): InstanceType | null {
	if (megalodon instanceof Mastodon) return InstanceType.Mastodon;
	if (megalodon instanceof Pleroma) return InstanceType.Pleroma;
	if (megalodon instanceof Misskey) return InstanceType.Misskey;
	return null;
}

/**
 * This reimplements the uploadMedia method from https://github.com/h3poteto/megalodon/blob/v6.1.0/megalodon/src/mastodon.ts
 * To support creation of a formData from a buffer
 */
export async function mastodonUpload(
	client: MastodonAPI.Interface,
	file: Express.Multer.File,
	options: UploadOptional,
) {
	const form = new FormData();
	form.set('file', new Blob([file.buffer]), file.filename);

	if (options) {
		if (options.description) {
			form.set('description', options.description);
		}
		if (options.focus) {
			form.set('focus', options.focus);
		}
	}

	return client
		.postForm<MastodonAPI.Entity.AsyncAttachment>('/api/v2/media', form)
		.then((res) => {
			return Object.assign(res, {
				data: MastodonAPI.Converter.async_attachment(res.data),
			});
		});
}

/**
 * This reimplements the uploadMedia method from https://github.com/h3poteto/megalodon/blob/v6.1.0/megalodon/src/pleroma.ts
 * To support creation of a formData from a buffer
 */
export async function pleromaUpload(
	client: PleromaAPI.Interface,
	file: Express.Multer.File,
	options: UploadOptional,
) {
	const form = new FormData();
	form.set('file', new Blob([file.buffer]), file.filename);

	if (options) {
		if (options.description) {
			form.set('description', options.description);
		}
		if (options.focus) {
			form.set('focus', options.focus);
		}
	}

	return client
		.postForm<MastodonAPI.Entity.AsyncAttachment>('/api/v2/media', form)
		.then((res) => {
			return Object.assign(res, {
				data: MastodonAPI.Converter.async_attachment(res.data),
			});
		});
}

/**
 * This reimplements the uploadMedia method from https://github.com/h3poteto/megalodon/blob/v6.1.0/megalodon/src/misskey.ts
 * To support creation of a formData from a buffer
 */
export async function misskeyUpload(
	client: MisskeyAPI.Interface,
	file: Express.Multer.File,
	_: UploadOptional,
) {
	const form = new FormData();
	form.set('file', new Blob([file.buffer]), file.filename);

	return client
		.post<MisskeyAPI.Entity.File>('/api/drive/files/create', form)
		.then((res) => ({ ...res, data: MisskeyAPI.Converter.file(res.data) }));
}
