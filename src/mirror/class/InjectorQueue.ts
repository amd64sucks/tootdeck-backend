import { Redis } from 'ioredis';
import { InjectRedis } from '@liaoliaots/nestjs-redis';

import { RedisNamespace } from '../../app/redis.config';

type Domain = string;
type QueueID = string;
type PromiseCallback<T> = (data: T) => void;
type QueuedPromise<T> = () => Promise<T>;
type QueueValue<T> = { fetcher: QueuedPromise<T>; setters: Array<PromiseCallback<T>> };
type PromiseMap<T> = Map<QueueID, QueueValue<T>>;

/**
 * This class and `ApplicationTokenService` dependency needs to be manually instanced
 * for `ApplicationTokenService` works optimally
 */
export class InjectorQueue {
	private readonly _queue = new Map<Domain, PromiseMap<any>>();

	constructor(
		@InjectRedis(RedisNamespace.ExcludedDomain) private readonly redis_excluded_domain: Redis,
	) {}

	/**
	 * Add to queue
	 */
	async add<T>(domain: string, key: string, promise: QueuedPromise<void>): Promise<void>;
	async add<T>(
		domain: string,
		key: string,
		fetcher: QueuedPromise<T>,
		setters: PromiseCallback<T>,
	): Promise<void>;
	async add<T>(
		domain: string,
		key: string,
		fetcher: QueuedPromise<T>,
		setters?: PromiseCallback<T>,
	): Promise<void> {
		const excluded = await this.redis_excluded_domain.get(domain);
		if (excluded) {
			return;
		}

		const inner = this._queue.get(domain);
		let value = inner?.get(key);
		if (value) {
			if (setters) {
				value.setters.push(setters);
			}

			return;
		}

		value = { fetcher: fetcher, setters: setters ? [setters] : [] };
		if (!inner) {
			const inner_map = new Map([[key, value]]);
			this._queue.set(domain, inner_map);
		} else {
			inner.set(key, value);
		}
	}

	/**
	 * Iterates on promises map and execute them
	 */
	private async processPromiseMap(promise_map: PromiseMap<any>) {
		const values = promise_map.entries();

		let iter: [string, QueueValue<any>];
		while ((iter = values.next().value)) {
			const [_, value] = iter;

			const data = await value.fetcher();
			value.setters.forEach((callback) => callback(data));
		}
	}

	/**
	 * Process queue, getting promises map and execute them
	 */
	async process() {
		const values = this._queue.entries();

		// console.log(Array.from(this._queue.entries()).map((x) => [x[0], x[1].size]));

		let iter: [string, PromiseMap<any>];
		while ((iter = values.next().value)) {
			const [domain, promise_map] = iter;

			await this.processPromiseMap(promise_map);

			this._queue.delete(domain);
		}
	}
}
