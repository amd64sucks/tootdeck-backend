import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { FastifyReply, FastifyRequest } from 'fastify';
import { from, mergeMap } from 'rxjs';
import { Redis } from 'ioredis';
import { InjectRedis } from '@liaoliaots/nestjs-redis';

import { AvatarCacheService } from '../services/avatar.service';
import { InteractionsService } from '../services/Interactions.service';
import { OptionalFeaturesService } from '../../app/services/optinalFeatures.service';
import { ApplicationTokenService } from '../services/applicationToken.service';

import { InjectorQueue } from '../class/InjectorQueue';
import { RedisNamespace } from '../../app/redis.config';

import { getHandleFromURL } from '../../utils/getHandleFromURL';
import { Logger } from '../../utils/logger';
import { parseHandle } from '../../utils/parse.handle';
import { InstanceHandle } from '../../utils/handle.types';

@Injectable()
export class InjectorInterceptor<T> implements NestInterceptor<T, FastifyReply> {
	private readonly logger = new Logger(InjectorInterceptor.name);

	constructor(
		private readonly HttpService: HttpService,
		private readonly OptionalFeaturesService: OptionalFeaturesService,
		private readonly ApplicationTokenService: ApplicationTokenService,
		@InjectRedis(RedisNamespace.ExcludedDomain) private readonly redis_excluded_domain: Redis,
		@InjectRedis(RedisNamespace.AvatarCache) private readonly redis_avatar_cache: Redis,
		@InjectRedis(RedisNamespace.StatusesInteractions)
		private readonly redis_statuses_interactions: Redis,
	) {}

	/**
	 * Avatars
	 */
	// #region

	private readonly AvatarService = new AvatarCacheService(
		this.OptionalFeaturesService,
		this.HttpService,
		this.ApplicationTokenService,
		this.redis_excluded_domain,
		this.redis_avatar_cache,
	);
	private readonly avatar_queue = new InjectorQueue(this.redis_excluded_domain);

	async getAvatar(entity: Entity.Account) {
		if (!this.OptionalFeaturesService.CACHE_AVATAR) {
			return;
		}

		const handle = getHandleFromURL(entity.url);
		const cache_key = handle.username + '@' + handle.domain;
		await this.avatar_queue.add(
			handle.domain,
			cache_key,
			() => this.AvatarService.injectAvatar(handle, entity.avatar),
			(cached) => {
				if (cached) {
					entity.avatar = cached.avatar;
					entity.avatar_static = cached.avatar_static;
				}
			},
		);

		if (entity.moved) {
			this.forAccount(entity.moved);
		}
	}

	// #endregion

	/**
	 * Statuses interactions
	 */
	// #region

	private readonly InteractionsService = new InteractionsService(
		this.OptionalFeaturesService,
		this.ApplicationTokenService,
		this.redis_excluded_domain,
		this.redis_statuses_interactions,
	);
	private readonly interaction_queue = new InjectorQueue(this.redis_excluded_domain);

	async getInteractions(handle: InstanceHandle, entity: Entity.Status) {
		if (!this.OptionalFeaturesService.CACHE_STATUS_INTERACTIONS) {
			return;
		}

		// Status just created, skipping it
		if (new Date(entity.created_at).valueOf() + 10000 - new Date().valueOf() > 0) {
			// this.logger.verbose('getInteractions', 'Skipped (just created)');
			return;
		}

		// Status not public, skipping it
		if (entity.visibility === 'direct' || entity.visibility === 'private') {
			// this.logger.verbose('getInteractions', 'Skipped (not public)');
			return;
		}

		// Same instance, skipping it
		const domain = new URL(entity.account.url).hostname;
		if (domain === handle.domain) {
			await this.interaction_queue.add(domain, entity.uri, () =>
				this.InteractionsService.store(entity),
			);
			// this.logger.verbose('getInteractions', 'Stored (same instance)');
			return;
		}

		await this.interaction_queue.add(
			domain,
			entity.uri,
			async () => {
				const application = await this.ApplicationTokenService.get(domain);
				return this.InteractionsService.fetch(entity.uri, application);
			},
			(cached) => {
				if (cached) {
					entity.replies_count = cached.replies_count;
					entity.reblogs_count = cached.reblogs_count;
					entity.favourites_count = cached.favourites_count;
				}
			},
		);
	}

	// #endregion

	/**
	 * Parser
	 */
	// #region

	async forAccount(entity: Entity.Account) {
		await this.getAvatar(entity);
	}

	async forAccounts(entities: Entity.Account[]) {
		await Promise.all(entities.map((entity) => this.forAccount(entity)));
	}

	async forReaction(entity: Entity.Reaction) {
		if (!entity.accounts?.length) {
			return;
		}

		await this.forAccounts(entity.accounts);
	}

	async forReactions(entities: Entity.Reaction[]) {
		if (!entities?.length) {
			return;
		}

		await Promise.all(entities.map((entity) => this.forReaction(entity)));
	}

	async forStatus(handle: InstanceHandle, entity: Entity.Status | undefined) {
		if (!entity) {
			return;
		}

		await Promise.all([
			this.forAccount(entity.account),
			this.forReactions(entity.emoji_reactions),
			this.getInteractions(handle, entity),
		]);
	}

	async forStatuses(handle: InstanceHandle, entities: Entity.Status[]) {
		await Promise.all(entities.map((entity) => this.forStatus(handle, entity)));
	}

	async forConversation(entity: Entity.Conversation) {
		await this.forAccounts(entity.accounts);
	}

	async forConversations(entities: Entity.Conversation[]) {
		await Promise.all(entities.map((entity) => this.forConversation(entity)));
	}

	async forNotification(handle: InstanceHandle, entity: Entity.Notification) {
		await Promise.all([this.forAccount(entity.account), this.forStatus(handle, entity.status)]);
	}

	async forNotifications(handle: InstanceHandle, entities: Entity.Notification[]) {
		await Promise.all(entities.map((entity) => this.forNotification(handle, entity)));
	}

	async forInstance(entity: Entity.Instance) {
		if (!entity.contact_account) {
			return;
		}

		await this.forAccount(entity.contact_account);
	}

	async forReport(entity: Entity.Report) {
		if (!entity.target_account) {
			return;
		}

		await this.forAccount(entity.target_account);
	}

	async forResults(handle: InstanceHandle, entity: Entity.Results) {
		await Promise.all([
			this.forAccounts(entity.accounts),
			this.forStatuses(handle, entity.statuses),
		]);
	}

	// #endregion

	private async interceptorInjectCache(handle: InstanceHandle, data: any) {
		if (!data?.data || typeof data.data === 'string') {
			return data;
		}

		const entities = data.data as
			| Entity.Account[]
			| Entity.Status[]
			| Entity.Conversation[]
			| Entity.Notification[]
			| Entity.Account
			| Entity.Status
			| Entity.Conversation
			| Entity.Notification
			| Entity.Instance
			| Entity.Report
			| Entity.Results;

		// (context.switchToHttp().getResponse() as Response).header
		if (Array.isArray(entities)) {
			if (entities.length) {
				if ((entities[0] as Entity.Account).acct) {
					await this.forAccounts(data.data);
				} else if ((entities[0] as Entity.Status).account) {
					if ((entities[0] as Entity.Notification).type) {
						await this.forNotifications(handle, data.data);
					} else {
						await this.forStatuses(handle, data.data);
					}
				} else if ((entities[0] as Entity.Conversation).accounts) {
					await this.forConversations(data.data);
				}
			}
		} else {
			if ((entities as Entity.Account).acct) {
				await this.forAccount(data.data);
			} else if ((entities as Entity.Status).account) {
				if ((entities as Entity.Notification).type) {
					await this.forNotification(handle, data.data);
				} else {
					await this.forStatus(handle, data.data);
				}
			} else if ((entities as Entity.Conversation).accounts) {
				await this.forConversation(data.data);
			} else if ((entities as Entity.Instance).contact_account) {
				await this.forInstance(data.data);
			} else if ((entities as Entity.Report).target_account) {
				await this.forReport(data.data);
			} else if (
				(entities as Entity.Results).accounts &&
				(entities as Entity.Results).hashtags &&
				(entities as Entity.Results).statuses
			) {
				await this.forResults(handle, data.data);
			}
		}

		try {
			// console.log('--------- API');
			// console.log('# Avatar');
			await this.avatar_queue.process();
			// console.log('# Interactions');
			await this.interaction_queue.process();
		} catch (e) {}

		return data;
	}

	async websocketInjectCache(handle: InstanceHandle, data: any) {
		if (!data?.data || !data?.data.content || typeof data.data === 'string') {
			return data;
		}

		const content = data.data.content as
			| Entity.Account[]
			| Entity.Status[]
			| Entity.Conversation[]
			| Entity.Notification[]
			| Entity.Account
			| Entity.Status
			| Entity.Conversation
			| Entity.Notification
			| Entity.Instance
			| Entity.Report
			| Entity.Results;

		// (context.switchToHttp().getResponse() as Response).header
		if (Array.isArray(content)) {
			if (content.length) {
				if ((content[0] as Entity.Account).acct) {
					await this.forAccounts(data.data.content);
				} else if ((content[0] as Entity.Status).account) {
					if ((content[0] as Entity.Notification).type) {
						await this.forNotifications(handle, data.data.content);
					} else {
						await this.forStatuses(handle, data.data.content);
					}
				} else if ((content[0] as Entity.Conversation).accounts) {
					await this.forConversations(data.data.content);
				}
			}
		} else {
			if ((content as Entity.Account).acct) {
				await this.forAccount(data.data.content);
			} else if ((content as Entity.Status).account) {
				if ((content as Entity.Notification).type) {
					await this.forNotification(handle, data.data.content);
				} else {
					await this.forStatus(handle, data.data.content);
				}
			} else if ((content as Entity.Conversation).accounts) {
				await this.forConversation(data.data.content);
			} else if ((content as Entity.Instance).contact_account) {
				await this.forInstance(data.data.content);
			} else if ((content as Entity.Report).target_account) {
				await this.forReport(data.data.content);
			} else if (
				(content as Entity.Results).accounts &&
				(content as Entity.Results).hashtags &&
				(content as Entity.Results).statuses
			) {
				await this.forResults(handle, data.data.content);
			}
		}

		try {
			// console.log('--------- Websocket');
			// console.log('# Avatar');
			await this.avatar_queue.process();
			// console.log('# Interactions');
			await this.interaction_queue.process();
		} catch (e) {}

		return data;
	}

	intercept(ctx: ExecutionContext, next: CallHandler) {
		const request = ctx.switchToHttp().getRequest() as FastifyRequest;
		const raw_handle = request.headers.authorization?.substring(7);
		if (
			!raw_handle ||
			(!this.OptionalFeaturesService.CACHE_AVATAR &&
				!this.OptionalFeaturesService.CACHE_STATUS_INTERACTIONS)
		) {
			return next.handle();
		}

		const handle = parseHandle(raw_handle);
		return next
			.handle()
			.pipe(mergeMap((data) => from(this.interceptorInjectCache(handle, data))));
	}
}
