import {
	CallHandler,
	ExecutionContext,
	Inject,
	mixin,
	NestInterceptor,
	Optional,
	Type,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import FastifyMulter from 'fastify-multer';
import { Multer } from 'multer';

export function FastifyFileInterceptor(fieldName: string): Type<NestInterceptor> {
	class MixinInterceptor implements NestInterceptor {
		protected multer: Multer;

		constructor(
			@Optional()
			@Inject('MULTER_MODULE_OPTIONS')
			options: Multer,
		) {
			this.multer = (FastifyMulter as any)({ ...options });
		}

		async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
			const ctx = context.switchToHttp();

			await new Promise<void>((resolve, reject) =>
				this.multer.single(fieldName)(ctx.getRequest(), ctx.getResponse(), (error: any) => {
					if (error) {
						// const error = transformException(err);
						return reject(error);
					}
					resolve();
				}),
			);

			return next.handle();
		}
	}

	const Interceptor = mixin(MixinInterceptor);

	return Interceptor as Type<NestInterceptor>;
}
