import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export namespace Body {
	export class Follow {
		@ApiPropertyOptional()
		reblog?: boolean;
	}

	export class Mute {
		@ApiProperty()
		notifications: boolean;
	}

	export class MarkerLastReadID {
		@ApiProperty()
		last_read_id: string;
	}

	export class Marker {
		@ApiPropertyOptional({ type: MarkerLastReadID })
		home?: MarkerLastReadID;

		@ApiPropertyOptional({ type: MarkerLastReadID })
		notifications?: MarkerLastReadID;
	}

	class Poll {
		@ApiProperty({ type: String, isArray: true })
		options: Array<string>;

		@ApiProperty()
		expires_in: number;

		@ApiPropertyOptional()
		multiple?: boolean;

		@ApiPropertyOptional()
		hide_totals?: boolean;
	}

	export class Status {
		@ApiProperty()
		status: string;

		@ApiPropertyOptional({ type: String, isArray: true })
		media_ids?: Array<string>;

		@ApiPropertyOptional({ type: Poll })
		poll?: Poll;

		@ApiPropertyOptional()
		in_reply_to_id?: string;

		@ApiPropertyOptional()
		sensitive?: boolean;

		@ApiPropertyOptional()
		spoiler_text?: string;

		@ApiPropertyOptional()
		visibility?: 'public' | 'unlisted' | 'private' | 'direct';

		@ApiPropertyOptional()
		scheduled_at?: string;

		@ApiPropertyOptional()
		language?: string;

		@ApiPropertyOptional()
		quote_id?: string;
	}

	export class UploadMedia {
		@ApiPropertyOptional()
		description?: string;

		@ApiPropertyOptional()
		focus?: string;
	}

	export class UpdateMedia {
		@ApiPropertyOptional()
		description?: string;

		@ApiPropertyOptional()
		focus?: string;

		@ApiPropertyOptional()
		is_sensitive?: boolean;
	}

	export class Report {
		@ApiProperty()
		account_id: string;

		@ApiPropertyOptional({ type: String, isArray: true })
		status_ids?: string[];

		@ApiProperty()
		comment: string;

		@ApiPropertyOptional()
		forward?: boolean;

		@ApiPropertyOptional()
		category?: 'spam' | 'violation' | 'other';

		@ApiPropertyOptional({ type: Number, isArray: true })
		rule_ids?: number[];
	}

	export class DomainBlock {
		@ApiProperty()
		domain: string;
	}

	export class Filter {
		@ApiProperty()
		phrase: string;

		@ApiProperty({ type: String, isArray: true })
		context: string[];

		@ApiPropertyOptional()
		irreversible?: boolean;

		@ApiPropertyOptional()
		whole_word?: boolean;

		@ApiPropertyOptional()
		expires_in?: string;
	}
}

export const FileBody = {
	schema: {
		type: 'object',
		properties: {
			file: {
				type: 'string',
				format: 'binary',
				nullable: true,
			},
			description: {
				type: 'string',
				nullable: false,
			},
			focus: {
				type: 'string',
				nullable: false,
			},
		},
	},
};
