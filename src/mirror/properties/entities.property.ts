import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export namespace Entity {
	export class Emoji {
		@ApiProperty()
		shortcode: string;

		@ApiProperty()
		static_url: string;

		@ApiProperty()
		url: string;

		@ApiProperty()
		visible_in_picker: boolean;
	}

	export class Field {
		@ApiProperty()
		name: string;

		@ApiProperty()
		value: string;

		@ApiProperty()
		verified_at: string | null;
	}

	export class Source {
		@ApiProperty()
		privacy: string | null;

		@ApiProperty()
		sensitive: boolean | null;

		@ApiProperty()
		language: string | null;

		@ApiProperty()
		note: string;

		@ApiProperty({ type: Field, isArray: true })
		fields: Array<Field>;
	}

	export class Account {
		@ApiProperty()
		id: string;

		@ApiProperty()
		username: string;

		@ApiProperty()
		acct: string;

		@ApiProperty()
		display_name: string;

		@ApiProperty()
		locked: boolean;

		@ApiProperty()
		created_at: string;

		@ApiProperty()
		followers_count: number;

		@ApiProperty()
		following_count: number;

		@ApiProperty()
		statuses_count: number;

		@ApiProperty()
		note: string;

		@ApiProperty()
		url: string;

		@ApiProperty()
		avatar: string;

		@ApiProperty()
		avatar_static: string;

		@ApiProperty()
		header: string;

		@ApiProperty()
		header_static: string;

		@ApiProperty({ type: Emoji, isArray: true })
		emojis: Array<Emoji>;

		@ApiProperty()
		moved: Account | null;

		@ApiProperty({ type: Field, isArray: true })
		fields: Array<Field>;

		@ApiProperty()
		bot: boolean | null;

		@ApiProperty()
		source?: Source;
	}

	export class Sub {
		@ApiPropertyOptional()
		width?: number;

		@ApiPropertyOptional()
		height?: number;

		@ApiPropertyOptional()
		size?: string;

		@ApiPropertyOptional()
		aspect?: number;

		@ApiPropertyOptional()
		frame_rate?: string;

		@ApiPropertyOptional()
		duration?: number;

		@ApiPropertyOptional()
		bitrate?: number;
	}

	export class Focus {
		@ApiProperty()
		x: number;

		@ApiProperty()
		y: number;
	}

	export class Meta {
		@ApiPropertyOptional()
		original?: Sub;

		@ApiPropertyOptional()
		small?: Sub;

		@ApiPropertyOptional()
		focus?: Focus;

		@ApiPropertyOptional()
		length?: string;

		@ApiPropertyOptional()
		duration?: number;

		@ApiPropertyOptional()
		fps?: number;

		@ApiPropertyOptional()
		size?: string;

		@ApiPropertyOptional()
		width?: number;

		@ApiPropertyOptional()
		height?: number;

		@ApiPropertyOptional()
		aspect?: number;

		@ApiPropertyOptional()
		audio_encode?: string;

		@ApiPropertyOptional()
		audio_bitrate?: string;

		@ApiPropertyOptional()
		audio_channel?: string;
	}

	export class Attachment {
		@ApiProperty()
		id: string;

		@ApiProperty()
		type: 'unknown' | 'image' | 'gifv' | 'video' | 'audio';

		@ApiProperty()
		url: string;

		@ApiProperty()
		remote_url: string | null;

		@ApiProperty()
		preview_url: string | null;

		@ApiProperty()
		text_url: string | null;

		@ApiProperty()
		meta: Meta | null;

		@ApiProperty()
		description: string | null;

		@ApiProperty()
		blurhash: string | null;
	}

	export class Mention {
		@ApiProperty()
		id: string;

		@ApiProperty()
		username: string;

		@ApiProperty()
		url: string;

		@ApiProperty()
		acct: string;
	}

	export class History {
		@ApiProperty()
		day: string;

		@ApiProperty()
		uses: number;

		@ApiProperty()
		accounts: number;
	}

	export class Tag {
		@ApiProperty()
		name: string;

		@ApiProperty()
		url: string;

		@ApiProperty({ type: History, isArray: true })
		history: Array<History> | null;

		@ApiPropertyOptional()
		following?: boolean;
	}

	export class Card {
		@ApiProperty()
		url: string;

		@ApiProperty()
		title: string;

		@ApiProperty()
		description: string;

		@ApiProperty()
		type: 'link' | 'photo' | 'video' | 'rich';

		@ApiPropertyOptional()
		image?: string;

		@ApiPropertyOptional()
		author_name?: string;

		@ApiPropertyOptional()
		author_url?: string;

		@ApiPropertyOptional()
		provider_name?: string;

		@ApiPropertyOptional()
		provider_url?: string;

		@ApiPropertyOptional()
		html?: string;

		@ApiPropertyOptional()
		width?: number;

		@ApiPropertyOptional()
		height?: number;
	}

	export class PollOption {
		@ApiProperty()
		title: string;

		@ApiProperty()
		votes_count: number | null;
	}

	export class Poll {
		@ApiProperty()
		id: string;

		@ApiProperty()
		expires_at: string | null;

		@ApiProperty()
		expired: boolean;

		@ApiProperty()
		multiple: boolean;

		@ApiProperty()
		votes_count: number;

		@ApiProperty({ type: PollOption, isArray: true })
		options: Array<PollOption>;

		@ApiProperty()
		voted: boolean;
	}

	export class Application {
		@ApiProperty()
		name: string;

		@ApiPropertyOptional()
		website?: string | null;

		@ApiProperty()
		vapid_key?: string | null;
	}

	export class Reaction {
		@ApiProperty()
		count: number;

		@ApiProperty()
		me: boolean;

		@ApiProperty()
		name: string;

		@ApiPropertyOptional({ type: Account, isArray: true })
		accounts?: Array<Account>;
	}

	export class StatusTag {
		@ApiProperty()
		name: string;

		@ApiProperty()
		url: string;
	}

	export class Status {
		@ApiProperty()
		id: string;

		@ApiProperty()
		uri: string;

		@ApiProperty()
		account: Account;

		@ApiProperty()
		in_reply_to_id: string | null;

		@ApiProperty()
		in_reply_to_account_id: string | null;

		@ApiProperty()
		reblog: Status | null;

		@ApiProperty()
		content: string;

		@ApiProperty()
		plain_content: string | null;

		@ApiProperty()
		created_at: string;

		@ApiProperty({ type: Emoji, isArray: true })
		emojis: Emoji[];

		@ApiProperty()
		replies_count: number;

		@ApiProperty()
		reblogs_count: number;

		@ApiProperty()
		favourites_count: number;

		@ApiProperty()
		reblogged: boolean | null;

		@ApiProperty()
		favourited: boolean | null;

		@ApiProperty()
		muted: boolean | null;

		@ApiProperty()
		sensitive: boolean;

		@ApiProperty()
		spoiler_text: string;

		@ApiProperty()
		visibility: 'public' | 'unlisted' | 'private' | 'direct';

		@ApiProperty({ type: Attachment, isArray: true })
		media_attachments: Array<Attachment>;

		@ApiProperty({ type: Mention, isArray: true })
		mentions: Array<Mention>;

		@ApiProperty({ type: StatusTag, isArray: true })
		tags: Array<StatusTag>;

		@ApiProperty()
		card: Card | null;

		@ApiProperty()
		poll: Poll | null;

		@ApiProperty()
		application: Application | null;

		@ApiProperty()
		language: string | null;

		@ApiProperty()
		pinned: boolean | null;

		@ApiProperty({ type: Reaction, isArray: true })
		emoji_reactions: Array<Reaction>;

		@ApiProperty()
		quote: boolean;

		@ApiProperty()
		bookmarked: boolean;
	}

	export class List {
		@ApiProperty()
		id: string;

		@ApiProperty()
		title: string;
	}

	export class Relationship {
		@ApiProperty()
		id: string;

		@ApiProperty()
		following: boolean;

		@ApiProperty()
		followed_by: boolean;

		@ApiPropertyOptional()
		delivery_following?: boolean;

		@ApiProperty()
		blocking: boolean;

		@ApiProperty()
		blocked_by: boolean;

		@ApiProperty()
		muting: boolean;

		@ApiProperty()
		muting_notifications: boolean;

		@ApiProperty()
		requested: boolean;

		@ApiProperty()
		domain_blocking: boolean;

		@ApiProperty()
		showing_reblogs: boolean;

		@ApiProperty()
		endorsed: boolean;

		@ApiProperty()
		notifying: boolean;
	}

	export class Conversation {
		@ApiProperty()
		id: string;

		@ApiProperty({ type: Entity.Account, isArray: true })
		accounts: Array<Entity.Account>;

		@ApiProperty({ type: Entity.Status })
		last_status: Entity.Status | null;

		@ApiProperty()
		unread: boolean;
	}

	export class MarkerHome {
		@ApiProperty()
		last_read_id: string;

		@ApiProperty()
		version: number;

		@ApiProperty()
		updated_at: string;
	}

	export class MarkerNotification extends MarkerHome {
		@ApiProperty()
		unread_count: number;
	}

	export class Marker {
		@ApiPropertyOptional({ type: Entity.MarkerHome })
		home?: Entity.MarkerHome;

		@ApiPropertyOptional({ type: Entity.MarkerNotification })
		notifications?: Entity.MarkerNotification;
	}

	export class Notification {
		@ApiProperty({ type: Entity.Account })
		account: Entity.Account;

		@ApiProperty()
		created_at: string;

		@ApiProperty()
		id: string;

		@ApiPropertyOptional({ type: Entity.Status })
		status?: Entity.Status;

		@ApiPropertyOptional()
		emoji?: string;

		@ApiProperty()
		type: string;
	}

	export class URLs {
		@ApiProperty()
		streaming_api: string;
	}

	export class Stats {
		@ApiProperty()
		user_count: number;

		@ApiProperty()
		status_count: number;

		@ApiProperty()
		domain_count: number;
	}

	export class InstanceRule {
		@ApiProperty()
		id: string;

		@ApiProperty()
		text: string;
	}

	export class ConfigurationStatuses {
		@ApiProperty()
		max_characters: number;

		@ApiPropertyOptional()
		max_media_attachments?: number;
	}

	export class ConfigurationPolls {
		@ApiProperty()
		max_options: number;

		@ApiProperty()
		max_characters_per_option: number;

		@ApiProperty()
		min_expiration: number;

		@ApiProperty()
		max_expiration: number;
	}

	export class Configuration {
		@ApiProperty({ type: ConfigurationStatuses })
		statuses: ConfigurationStatuses;

		@ApiPropertyOptional({ type: ConfigurationPolls })
		polls?: ConfigurationPolls;
	}
	export class Instance {
		@ApiProperty()
		uri: string;

		@ApiProperty()
		title: string;

		@ApiProperty()
		description: string;

		@ApiProperty()
		email: string;

		@ApiProperty()
		version: string;

		@ApiProperty()
		thumbnail: string | null;

		@ApiProperty({ type: Entity.URLs })
		urls: Entity.URLs;

		@ApiProperty({ type: Entity.Stats })
		stats: Entity.Stats;

		@ApiProperty({ type: String, isArray: true })
		languages: Array<string>;

		@ApiProperty()
		registrations: boolean;

		@ApiProperty()
		approval_required: boolean;

		@ApiPropertyOptional()
		invites_enabled?: boolean;

		configuration: {};

		@ApiPropertyOptional({ type: Account })
		contact_account?: Account;

		@ApiPropertyOptional({ type: Entity.InstanceRule, isArray: true })
		rules?: Array<Entity.InstanceRule>;
	}

	export class Activity {
		@ApiProperty()
		week: string;

		@ApiProperty()
		statuses: string;

		@ApiProperty()
		logins: string;

		@ApiProperty()
		registrations: string;
	}

	export class Announcement {
		@ApiProperty()
		id: string;

		@ApiProperty()
		content: string;

		@ApiProperty()
		starts_at: string | null;

		@ApiProperty()
		ends_at: string | null;

		@ApiProperty()
		published: boolean;

		@ApiProperty()
		all_day: boolean;

		@ApiProperty()
		published_at: string;

		@ApiProperty()
		updated_at: string | null;

		read: boolean | null;
		@ApiProperty()
		@ApiProperty()
		mentions: Array<AnnouncementAccount>;

		@ApiProperty()
		statuses: Array<AnnouncementStatus>;

		@ApiProperty()
		tags: Array<StatusTag>;

		@ApiProperty()
		emojis: Array<Emoji>;

		@ApiProperty()
		reactions: Array<AnnouncementReaction>;
	}
	export class AnnouncementAccount {
		@ApiProperty()
		id: string;

		@ApiProperty()
		username: string;

		@ApiProperty()
		url: string;

		@ApiProperty()
		acct: string;
	}
	export class AnnouncementStatus {
		@ApiProperty()
		id: string;

		@ApiProperty()
		url: string;
	}
	export class AnnouncementReaction {
		@ApiProperty()
		name: string;

		@ApiProperty()
		count: number;

		@ApiProperty()
		me: boolean | null;

		@ApiProperty()
		url: string | null;

		@ApiProperty()
		static_url: string | null;
	}

	export class Context {
		@ApiProperty({ type: Status, isArray: true })
		ancestors: Array<Status>;

		@ApiProperty({ type: Status, isArray: true })
		descendants: Array<Status>;
	}

	export class StatusSource {
		@ApiProperty()
		id: string;

		@ApiProperty()
		text: string;

		@ApiProperty()
		spoiler_text: string;
	}

	export class StatusParams {
		@ApiProperty()
		text: string;

		@ApiProperty()
		in_reply_to_id: string | null;

		@ApiProperty({ type: String, isArray: true })
		media_ids: Array<string> | null;

		@ApiProperty()
		sensitive: boolean | null;

		@ApiProperty()
		spoiler_text: string | null;

		@ApiProperty()
		visibility: 'public' | 'unlisted' | 'private' | 'direct' | null;

		@ApiProperty()
		scheduled_at: string | null;

		@ApiProperty()
		application_id: number | null;
	}

	export class ScheduledStatus {
		@ApiProperty()
		id: string;

		@ApiProperty()
		scheduled_at: string;

		@ApiProperty({ type: StatusParams })
		params: StatusParams;

		@ApiProperty({ type: Attachment, isArray: true })
		media_attachments: Array<Attachment> | null;
	}

	export class AsyncAttachment {
		@ApiProperty()
		id: string;

		@ApiProperty()
		type: 'unknown' | 'image' | 'gifv' | 'video' | 'audio';

		@ApiProperty()
		url: string | null;

		@ApiProperty()
		remote_url: string | null;

		@ApiProperty()
		preview_url: string;

		@ApiProperty()
		text_url: string | null;

		@ApiProperty({ type: Meta })
		meta: Meta | null;

		@ApiProperty()
		description: string | null;

		@ApiProperty()
		blurhash: string | null;
	}

	export class Report {
		@ApiProperty()
		id: string;

		@ApiProperty()
		action_taken: boolean;

		@ApiProperty()
		category: 'spam' | 'violation' | 'other';

		@ApiProperty()
		comment: string;

		@ApiProperty()
		forwarded: boolean;

		@ApiProperty({ type: String, isArray: true })
		status_ids: Array<string> | null;

		@ApiProperty({ type: String, isArray: true })
		rule_ids: Array<string> | null;

		@ApiPropertyOptional({ type: Account })
		target_account?: Account;
	}

	export class Preferences {
		@ApiProperty()
		'posting:default:visibility': 'public' | 'unlisted' | 'private' | 'direct';

		@ApiProperty()
		'posting:default:sensitive': boolean;

		@ApiProperty()
		'posting:default:language': string | null;

		@ApiProperty()
		'reading:expand:media': 'default' | 'show_all' | 'hide_all';

		@ApiProperty()
		'reading:expand:spoilers': boolean;
	}

	export class Filter {
		@ApiProperty()
		id: string;

		@ApiProperty()
		phrase: string;

		@ApiProperty({ type: String, isArray: true })
		context: Array<String>;

		@ApiProperty()
		expires_at: string | null;

		@ApiProperty()
		irreversible: boolean;

		@ApiProperty()
		whole_word: boolean;
	}

	export class Results {
		@ApiProperty({ type: Account, isArray: true })
		accounts: Array<Account>;

		@ApiProperty({ type: Status, isArray: true })
		statuses: Array<Status>;

		@ApiProperty({ type: Tag, isArray: true })
		hashtags: Array<Tag>;
	}
}
