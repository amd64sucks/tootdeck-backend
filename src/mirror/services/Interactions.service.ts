import {
	BadRequestException,
	Injectable,
	ServiceUnavailableException,
	UnprocessableEntityException,
} from '@nestjs/common';
import { InjectRedis } from '@liaoliaots/nestjs-redis';
import { deserialize, serialize } from 'v8';
import { Redis } from 'ioredis';
import MegalodonGenerator from 'megalodon';

import { OptionalFeaturesService } from '../../app/services/optinalFeatures.service';
import { ApplicationTokenService } from './applicationToken.service';

import { Logger } from '../../utils/logger';
import { ErrorActions, parseError } from '../utils/parseError';

import { RedisNamespace } from '../../app/redis.config';

import { ApiResponseError } from '../../auth/types/api.types';
import { InstanceType } from '../../database/types';
import { CachedStatusInteractionsResponse } from '../types/interactions.types';
import { ApplicationStore } from 'src/database/entities/application.entity';

@Injectable()
export class InteractionsService {
	private readonly logger = new Logger(InteractionsService.name);

	constructor(
		private readonly OptionalFeaturesService: OptionalFeaturesService,
		private readonly ApplicationTokenService: ApplicationTokenService,
		@InjectRedis(RedisNamespace.ExcludedDomain) private readonly excluded_domain_redis: Redis,
		@InjectRedis(RedisNamespace.StatusesInteractions) private readonly redis: Redis,
	) {
		if (!this.OptionalFeaturesService.CACHE_STATUS_INTERACTIONS) {
			this.logger.log('constructor', 'Service disabled');
		}
	}

	/**
	 * Utils
	 */
	// #region

	private getStatusID(type: InstanceType, pathname: string): string | undefined {
		switch (type) {
			case InstanceType.Mastodon:
				// https://wetdry.world/users/ipg/statuses/110839371146336104/activity
				// users/ipg/statuses/110839371146336104/activity
				// 110839371146336104
				return pathname.replace('/', '').split('/')[3];
				break;
			case InstanceType.Pleroma:
				// https://akko.wtf/objects/bdbd1a9f-1e6e-40c4-bbb9-8513bbeb0c3b
				// objects/bdbd1a9f-1e6e-40c4-bbb9-8513bbeb0c3b
				// bdbd1a9f-1e6e-40c4-bbb9-8513bbeb0c3b
				return pathname.replace('/', '').split('/')[1];
				break;
			// case InstanceType.Misskey:
			// 	// https://misskey.io/notes/9i08mrj42s
			// 	// notes/9i08mrj42s
			// 	// 9i08mrj42s
			// 	return pathname.replace('/', '').split('/')[1];
			// 	break;
		}

		return undefined;
	}

	// #endregion

	/**
	 * Service
	 */
	// #region

	/**
	 * @throws BadRequestException
	 * @throws InternalServerErrorException
	 */
	private async fetchInteractions(url: string, application: ApplicationStore) {
		const { origin, pathname } = new URL(url);

		const id = this.getStatusID(application.type, pathname);
		if (!id) {
			throw new BadRequestException('Unsupported instance type');
		}

		const status = await MegalodonGenerator(
			application.type,
			origin,
			application.token,
		).getStatus(id);

		const { replies_count, reblogs_count, favourites_count } = status.data;
		const ret = {
			replies_count,
			reblogs_count,
			favourites_count,
		};

		this.redis.set(
			url,
			serialize({
				replies_count,
				reblogs_count,
				favourites_count,
			}),
			'EX',
			30 * 60,
		);

		return ret;
	}

	/**
	 * @throws ServiceUnavailableException
	 * @throws UnprocessableEntityException
	 * @throws BadRequestException
	 * @throws InternalServerErrorException
	 */
	async fetch(
		url: string,
		application?: ApplicationStore,
	): Promise<CachedStatusInteractionsResponse | null> {
		if (!this.OptionalFeaturesService.CACHE_STATUS_INTERACTIONS) {
			throw new ServiceUnavailableException();
		}

		const { hostname } = new URL(url);
		if (!application) {
			const is_excluded = await this.excluded_domain_redis.get(hostname);
			if (is_excluded) {
				throw new UnprocessableEntityException(`This domain is excluded ${url}`);
			}
		}

		const buffer = await this.redis.getBuffer(url);
		const interactions = buffer ? deserialize(buffer) : null;

		if (interactions === 'DEAD') {
			throw new UnprocessableEntityException();
		}

		// Cache is in waiting state before requerying unresponsive instance
		if (interactions === 'RETRY') {
			throw new BadRequestException(
				ApiResponseError.InvalidInstanceResponse + ', retry later',
			);
		}

		if (!interactions) {
			try {
				if (!application) {
					application = await this.ApplicationTokenService.get(hostname);
				}
				return (await this.fetchInteractions(url, application)) ?? null;
			} catch (e) {
				const parsed = parseError(e);
				const { hostname } = new URL(url);

				switch (parsed) {
					case ErrorActions.Invalid:
						this.excluded_domain_redis.set(hostname, serialize(''));
						this.logger.verbose('getInteractions', `Excluded ${hostname}`);
						break;
					case ErrorActions.Dead:
						this.redis.set(url, serialize('DEAD'));
						break;
					case ErrorActions.Retry:
						this.redis.set(url, serialize('RETRY'), 'EX', 30 * 60);
						break;
				}

				return null;
			}
		}

		return interactions;
	}

	async store(entity: Entity.Status) {
		const { replies_count, reblogs_count, favourites_count, uri } = entity;

		await this.redis.set(
			uri,
			serialize({
				replies_count,
				reblogs_count,
				favourites_count,
			}),
			'EX',
			30 * 60,
		);
	}

	// #endregion
}
