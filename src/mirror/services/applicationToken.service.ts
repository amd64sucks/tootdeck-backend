import {
	BadRequestException,
	Inject,
	Injectable,
	InternalServerErrorException,
	ServiceUnavailableException,
	forwardRef,
} from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import MegalodonGenerator from 'megalodon';

import { DBApplicationService } from '../../database/services/database.application.service';
import { OAuthService } from '../../auth/services/oauth.service';

import { Logger } from '../../utils/logger';

import { ApplicationStore } from '../../database/entities/application.entity';

import { ApiResponseError } from '../../auth/types/api.types';
import { InstanceType } from '../../database/types';
import { ApplicationTokenResponse } from '../types/applicationToken.types';

@Injectable()
export class ApplicationTokenService {
	private readonly logger = new Logger(ApplicationTokenService.name);

	constructor(
		private readonly httpService: HttpService,
		@Inject(forwardRef(() => DBApplicationService))
		private readonly DBApplicationService: DBApplicationService,
		@Inject(forwardRef(() => OAuthService))
		private readonly OAuthService: OAuthService,
	) {}

	/**
	 * Uses OAuth to obtain an application token and stores it in the database
	 *
	 * Verifies application validity if it already have a token
	 *
	 * @throws AxiosError
	 * @throws NotFoundException
	 */
	private async getApplicationToken(application: ApplicationStore): Promise<ApplicationStore> {
		if (application.token) {
			const ckeck = await MegalodonGenerator(
				application.type,
				'https://' + application.domain,
				application.token,
			).verifyAppCredentials();

			if (ckeck.status === 200) {
				// this.logger.debug(
				// 	'getApplicationToken',
				// 	'Successfully checked application for ' + application.domain,
				// );
				return application;
			}
		}

		const form = new FormData();
		form.append('grant_type', 'client_credentials');
		form.append('client_id', application.key);
		form.append('client_secret', application.secret);
		form.append('redirect_uri', 'urn:ietf:wg:oauth:2.0:oob');

		const oauth = await this.httpService.axiosRef
			.post(`https://${application.domain}/oauth/token`, form, {
				headers: {
					'Content-Type': 'application/json',
				},
			})
			.then((r) => r.data as ApplicationTokenResponse)
			.catch((e) => {
				this.logger.error(
					'getApplicationToken',
					`Unable to fetch application token for ${application.domain}`,
				);
				throw e;
			});

		const token = oauth?.access_token;
		if (!token) {
			throw new BadRequestException(ApiResponseError.InvalidInstanceResponse);
		}

		const update = await this.DBApplicationService.updateToken(application, token);
		if (!update) {
			this.logger.error(
				'getApplicationToken',
				`Unable to update application token for ${application.domain}`,
			);
			throw new InternalServerErrorException();
		}

		// this.logger.debug('getApplicationToken', 'Got token for ' + application.domain);

		return update;
	}

	/**
	 * Finds application in database, create one if not existing and return application with it's token
	 * @throws AxiosError
	 */
	async get(domain: string): Promise<ApplicationStore> {
		let application = await this.DBApplicationService.get(domain);
		if (!application) {
			const promise = this.OAuthService.generateApplication(domain, false).catch((e) => {
				if (
					e.message === ApiResponseError.InvalidInstance ||
					e.message === ApiResponseError.InvalidInstanceResponse
				) {
					throw new ServiceUnavailableException('Misskey instance or unknown sns');
				}

				throw e;
			});
			application = (await promise).entity;
		}

		if (application.type === InstanceType.Misskey) {
			throw new ServiceUnavailableException(
				"Misskey doesn't support app based authentication",
			);
		}

		return this.getApplicationToken(application);
	}
}
