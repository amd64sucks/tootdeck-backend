import {
	BadRequestException,
	Injectable,
	InternalServerErrorException,
	NotFoundException,
	ServiceUnavailableException,
	UnprocessableEntityException,
} from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { randomUUID } from 'crypto';
import { existsSync, rmSync, writeFileSync } from 'fs';
import { InjectRedis } from '@liaoliaots/nestjs-redis';
import { Redis } from 'ioredis';
import MegalodonGenerator from 'megalodon';
const ffmpeg = require('fluent-ffmpeg');
// @ts-ignore for types
import FFMPEG from 'fluent-ffmpeg';

import { OptionalFeaturesService } from '../../app/services/optinalFeatures.service';
import { ApplicationTokenService } from './applicationToken.service';

import { Logger } from '../../utils/logger';
import { stringifyHandle } from '../../utils/stringifyHandle';

import { RedisNamespace } from '../../app/redis.config';
import { ErrorActions, parseError } from '../utils/parseError';

import { ApplicationStore } from '../../database/entities/application.entity';

import { ApiResponseError } from '../../auth/types/api.types';
import { InstanceHandle } from '../../utils/handle.types';
import { CachedAvatarResponse } from '../types/avatar.types';

@Injectable()
export class AvatarCacheService {
	private readonly logger = new Logger(AvatarCacheService.name);

	constructor(
		private readonly OptionalFeaturesService: OptionalFeaturesService,
		private readonly httpService: HttpService,
		private readonly ApplicationTokenService: ApplicationTokenService,
		@InjectRedis(RedisNamespace.ExcludedDomain) private readonly excluded_domain_redis: Redis,
		@InjectRedis(RedisNamespace.AvatarCache) private readonly redis: Redis,
	) {
		if (!this.OptionalFeaturesService.CACHE_AVATAR) {
			this.logger.log('constructor', 'Service disabled');
		}
	}
	/**
	 * Instance
	 */
	// #region

	/**
	 * Finds account entity on instance
	 *
	 * @throws NotFoundException
	 * @throws AxiosError
	 */
	private async searchAccount(
		application: ApplicationStore,
		handle: InstanceHandle,
	): Promise<Entity.Account> {
		const stringify_handle = stringifyHandle(handle);

		const search = await MegalodonGenerator(
			application.type,
			'https://' + application.domain,
			application.token,
		)
			.search(stringify_handle, 'accounts', {
				limit: 1,
			})
			.catch((e) => {
				this.logger.error('searchAccount', `Unable to find ${stringify_handle}`);
				throw e;
			});

		const accounts = search.data.accounts;

		if (accounts.length === 0) {
			throw new NotFoundException('Handle not found on instance');
		}

		return accounts[0]!;
	}

	// #endregion

	/**
	 * Avatar processing
	 */
	// #region

	/**
	 * @throws InternalServerErrorException
	 */
	private getAvatarFilenameExt(avatar: string): string {
		// https://mstdn.shalyu.run/system/accounts/avatars/108/238/931/200/572/991/original/a662305c12c938dd.jpg => /system/accounts/avatars/108/238/931/200/572/991/original/a662305c12c938dd.jpg
		// /system/accounts/avatars/108/238/931/200/572/991/original/a662305c12c938dd.jpg => [system, accounts, avatars, 108, 238, 931, 200, 572, 991, original, a662305c12c938dd.jpg]
		const url_split = new URL(avatar).pathname.split('/');
		// [system, accounts, avatars, 108, 238, 931, 200, 572, 991, original, a662305c12c938dd.jpg] => a662305c12c938dd.jpg
		const filename = url_split.pop();
		// a662305c12c938dd.jpg => a662305c12c938dd
		const ext = filename?.split('.').pop();
		if (!ext) {
			this.logger.error('getAvatarFilename', `Unable to determine filename`, {
				url: avatar,
			});
			throw new InternalServerErrorException('Error when processing avatar filename');
		}

		return ext;
	}

	/**
	 * Transcode downloaded avatar to a static webp image
	 */
	private async createStaticAvatar(
		avatar_path: string,
		stringify_handle: string,
		account: Entity.Account,
	): Promise<string | null> {
		const output_path = `cached_avatar/static/${stringify_handle}.webp`;

		// Remove previous avatar
		try {
			rmSync(output_path, { force: true });
		} catch (e) {
			this.logger.error(
				'createStaticAvatar',
				`Unable remove avatar for account ${stringify_handle}`,
				{ url: account.avatar, output_path },
				e,
			);
		}

		const promise = new Promise((resolve, reject) => {
			ffmpeg()
				.input(avatar_path)
				.frames(1)
				.videoCodec('libwebp')
				.save(output_path)
				.on('end', () => {
					resolve(true);
				})
				.on('error', (e: any) => {
					reject(e);
				});
		});
		const result = await promise.catch((e) => {
			this.logger.error(
				'createStaticAvatar',
				`Unable to convert avatar to static avatar for account ${stringify_handle}`,
				{ url: account.avatar, avatar_path },
				e,
			);
			return false;
		});

		if (!result) {
			return null;
		}

		return output_path;
	}

	/**
	 * Transcode downloaded avatar to a webp image
	 */
	private async createAvatar(
		avatar_path: string,
		stringify_handle: string,
		account: Entity.Account,
	): Promise<string | null> {
		const output_path = `cached_avatar/avatar/${stringify_handle}.webp`;

		const promise = new Promise((resolve, reject) => {
			ffmpeg()
				.input(avatar_path)
				.videoCodec('libwebp')
				.outputOption('-loop 0')
				.save(output_path)
				.on('end', () => {
					resolve(true);
				})
				.on('error', (e: any) => {
					reject(e);
				});
		});
		const result = await promise.catch((e) => {
			this.logger.error(
				'createAvatar',
				`Unable to convert avatar for account ${stringify_handle}`,
				{ url: account.avatar, avatar_path },
				e,
			);
			return false;
		});

		if (!result) {
			return null;
		}

		return output_path;
	}

	/**
	 * Downloads avatar and create locals webp versions
	 *
	 * @throws InternalServerErrorException
	 */
	private async createAvatarCache(
		account: Entity.Account,
		handle: InstanceHandle,
	): Promise<CachedAvatarResponse> {
		const stringify_handle = stringifyHandle(handle);

		const raw = await this.httpService
			.axiosRef({
				method: 'get',
				url: account.avatar,
				responseType: 'arraybuffer',
			})
			.then((r) => {
				if (!(r.headers['content-type'] as string).includes('image')) {
					this.logger.error(
						'createAvatarCache',
						`Unable to fetch remote avatar for ${stringify_handle}`,
						'Invalid content-type: ' + r.headers['content-type'],
					);
					return null;
				}
				return Buffer.from(r.data, 'binary');
			})
			.catch((e) => {
				this.logger.error(
					'createAvatarCache',
					`Unable to fetch remote avatar for ${stringify_handle}`,
				);
				throw e;
			});

		if (!raw) {
			throw new InternalServerErrorException('Invalid avatar content type');
		}

		// Create tmp file
		const ext = this.getAvatarFilenameExt(account.avatar);
		const tmp_path = `/tmp/${randomUUID()}.${ext}`;

		try {
			writeFileSync(tmp_path, raw);
		} catch (e) {
			this.logger.error(
				'createAvatar',
				`Unable write tmp avatar for account ${stringify_handle}`,
				{ url: account.avatar, tmp_path },
				e,
			);
			throw new InternalServerErrorException('Unable to buffer avatar');
		}

		// Create avatar cache
		const avatar = await this.createAvatar(tmp_path, stringify_handle, account);
		const avatar_static = await this.createStaticAvatar(tmp_path, stringify_handle, account);

		// Remove tmp file
		try {
			rmSync(tmp_path, { force: true });
		} catch (e) {
			this.logger.error(
				'createAvatarCache',
				`Unable remove tmp avatar for account ${stringify_handle}`,
				{ url: account.avatar, tmp_path },
				e,
			);
			throw new InternalServerErrorException('Unable to remove avatar buffer');
		}

		if (!avatar || !avatar_static) {
			this.logger.error(
				'createAvatar',
				`Unable to convert avatars for account ${stringify_handle}`,
				{ url: account.avatar },
			);
			throw new InternalServerErrorException(
				'Something when wrong when creating avatar cache',
			);
		}

		return {
			avatar,
			avatar_static,
		};
	}

	// #endregion

	/**
	 * Service
	 */
	// #region

	/**
	 * @throws BadRequestException
	 * @throws InternalServerErrorException
	 */
	private async fetchAvatar(handle: InstanceHandle): Promise<void> {
		const stringify_handle = stringifyHandle(handle);
		const cache = await this.redis.get(stringify_handle);
		if (cache === 'DEAD') {
			return;
		}
		if (cache === 'RETRY') {
			throw new BadRequestException(
				ApiResponseError.InvalidInstanceResponse + ', retrying in one hour',
			);
		}

		const cached_avatar = `cached_avatar/avatar/${stringify_handle}.webp`;
		let exist: boolean = true;
		try {
			exist = existsSync(cached_avatar);
		} catch (e) {
			this.logger.error('avatar', `Unable to stat ${cached_avatar}`, e);
		}

		const application = await this.ApplicationTokenService.get(handle.domain);
		if (!application) {
			throw new BadRequestException(
				ApiResponseError.InvalidInstanceResponse + ', retrying in one hour',
			);
		}
		const remote_account = await this.searchAccount(application, handle);

		if (
			// Not in cache and need to be cached
			!cache ||
			// In cache and need to be updated
			cache !== remote_account.avatar ||
			// In cache and file is missing
			(cache && !exist)
		) {
			await this.createAvatarCache(remote_account, handle);
			await this.redis.set(stringify_handle, remote_account.avatar);
			return;
		}

		if (!cache) {
			this.logger.error('avatar', `Unable to get avatar from cache for ${stringify_handle}`);
			throw new InternalServerErrorException();
		}
	}

	/**
	 * @throws ServiceUnavailableException
	 * @throws InternalServerErrorException
	 */
	async injectAvatar(
		handle: InstanceHandle,
		avatar_url: string,
	): Promise<CachedAvatarResponse | null> {
		if (!this.OptionalFeaturesService.CACHE_AVATAR) {
			throw new ServiceUnavailableException();
		}

		const stringify_handle = stringifyHandle(handle);
		const cached_avatar = await this.redis.get(stringify_handle);

		if (!cached_avatar || cached_avatar === 'DEAD' || cached_avatar === 'RETRY') {
			return null;
		}

		// Remote avatar changed
		if (cached_avatar !== avatar_url) {
			return null;
		}

		return {
			avatar: `cached_avatar/avatar/${stringify_handle}.webp`,
			avatar_static: `cached_avatar/static/${stringify_handle}.webp`,
		};
	}

	/**
	 * @throws ServiceUnavailableException
	 * @throws InternalServerErrorException
	 * @throws BadRequestException
	 * @throws UnprocessableEntityException
	 * @throws NotFoundException
	 */
	async getAvatar(handle: InstanceHandle): Promise<CachedAvatarResponse | null> {
		if (!this.OptionalFeaturesService.CACHE_AVATAR) {
			throw new ServiceUnavailableException();
		}

		const stringify_handle = stringifyHandle(handle);
		const cached_avatar = await this.redis.get(stringify_handle);

		if (cached_avatar === 'DEAD') {
			throw new NotFoundException();
		}

		// Cache is in waiting state before requerying unresponsive instance
		if (cached_avatar === 'RETRY') {
			throw new BadRequestException(
				ApiResponseError.InvalidInstanceResponse + ', retry later',
			);
		}

		if (!cached_avatar) {
			await this.fetchAvatar(handle).catch(async (e) => {
				const parsed = parseError(e);
				switch (parsed) {
					case ErrorActions.Invalid:
						await this.excluded_domain_redis.set(handle.domain, '.');
						this.logger.verbose('getAvatar', `Excluded ${handle.domain}`);
						throw new UnprocessableEntityException();
					case ErrorActions.Dead:
						await this.redis.set(stringify_handle, 'DEAD');
						throw new NotFoundException();
					case ErrorActions.Retry:
						await this.redis.set(stringify_handle, 'RETRY', 'EX', 60 * 60);
						throw new BadRequestException(
							ApiResponseError.InvalidInstanceResponse + ', retry later',
						);
				}
			});
		}

		return {
			avatar: `cached_avatar/avatar/${stringify_handle}.webp`,
			avatar_static: `cached_avatar/static/${stringify_handle}.webp`,
		};
	}

	// #endregion
}
