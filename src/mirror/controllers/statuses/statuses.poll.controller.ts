import {
	BadRequestException,
	Body,
	Controller,
	Get,
	Param,
	Post,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiParam, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../utils/preprocess.guard';

import { AxiosErrorFilter } from '../../utils/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../utils/guards.types';

@ApiTags('Mirror · Statuses · Polls')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/polls')
export class MirrorStatusesPollsController {
	/**
	 * GET /api/v1/polls/:id
	 * https://docs.joinmastodon.org/methods/polls/#get
	 *
	 * @param `id`
	 * @returns `Entity.Poll`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'View a poll.',
		type: Entity.Poll,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Get(':id')
	async get(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getPoll(id);
	}

	/**
	 * POST /api/v1/polls/:id/votes
	 * https://docs.joinmastodon.org/methods/polls/#vote
	 *
	 * @param `id`
	 * @returns `Entity.Poll`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Vote on a poll.',
		type: Entity.Poll,
	})
	@ApiResponse({
		status: 400.1,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 400.2,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@ApiResponse({
		status: 422.1,
		description: 'Validation failed: The poll has already ended.',
	})
	@ApiResponse({
		status: 422.2,
		description: 'Validation failed: You have already voted on this poll.',
	})
	@Post(':id/votes')
	async vote(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Body('choices') choices: number[],
	) {
		if (!id || !choices || !choices.length) {
			throw new BadRequestException();
		}

		return request.instance.votePoll(id, choices);
	}
}
