import {
	BadRequestException,
	Controller,
	Delete,
	Get,
	Param,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiParam, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../utils/preprocess.guard';

import { AxiosErrorFilter } from '../../utils/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../utils/guards.types';

@ApiTags('Mirror · Statuses · Scheduled')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/scheduled_statuses')
export class MirrorStatusesScheduledController {
	/**
	 * GET /api/v1/scheduled_statuses
	 * https://docs.joinmastodon.org/methods/scheduled_statuses/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Emoji>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'View scheduled statuses.',
		type: Entity.ScheduledStatus,
		isArray: true,
	})
	@Get()
	async getAll(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
		};

		return request.instance.getScheduledStatuses(options);
	}

	/**
	 * GET /api/v1/scheduled_statuses/:id
	 * https://docs.joinmastodon.org/methods/scheduled_statuses/#get-one
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Entity.ScheduledStatus`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'View a single scheduled status.',
		type: Entity.ScheduledStatus,
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Get('id')
	async getOne(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getScheduledStatus(id);
	}

	/**
	 * PUT /api/v1/scheduled_statuses/:id
	 * https://docs.joinmastodon.org/methods/scheduled_statuses/#update
	 *
	 * @param `id`
	 * @returns `Entity.ScheduledStatus`
	 */
	// @ApiParam({
	// 	name: 'id',
	// 	type: String,
	// 	required: true,
	// })
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Update a scheduled status’s publishing date.',
	// 	type: Entity.ScheduledStatus,
	// })
	// @ApiResponse({
	// 	status: 404,
	// 	description: 'Record not found.',
	// })
	// @Put('id')
	// async edit(@Request() request: RequestMirror, @Param('id') id: string) {
	// 	if (!id) {
	// 		throw new BadRequestException();
	// 	}
	// }

	/**
	 * DELETE /api/v1/scheduled_statuses/:id
	 * https://docs.joinmastodon.org/methods/scheduled_statuses/#cancel
	 *
	 * @param `id`
	 * @returns `{}`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Cancel a scheduled status.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Delete('id')
	async delete(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.cancelScheduledStatus(id);
	}
}
