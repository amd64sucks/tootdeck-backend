import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Post,
	Put,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiParam, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../utils/preprocess.guard';

import { AxiosErrorFilter } from '../../utils/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { validateBody } from '../../utils/validateBody';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../utils/guards.types';

@ApiTags('Mirror · Timelines · Lists')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/lists')
export class MirrorTimelinesListsController {
	/**
	 * GET /api/v1/lists
	 * https://docs.joinmastodon.org/methods/lists/#get
	 *
	 * @returns `Array<Entity.List>`
	 */
	@ApiResponse({
		status: 200,
		description: 'Fetch all lists that the user owns.',
		type: Entity.List,
		isArray: true,
	})
	@Get()
	async getAll(@Request() request: RequestMirror) {
		return request.instance.getLists();
	}

	/**
	 * GET /api/v1/lists/:id
	 * https://docs.joinmastodon.org/methods/lists/#get-one
	 *
	 * @params `id`
	 * @returns `Entity.List`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description:
			'Fetch the list with the given ID. Used for verifying the title of a list, and which replies to show within that list.',
		type: Entity.List,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'The conversation does not exist, or is not owned by you.',
	})
	@Get(':id')
	async getOne(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getList(id);
	}

	/**
	 * GET /api/v1/lists/:id/accounts
	 * https://docs.joinmastodon.org/methods/lists/#accounts
	 *
	 * @params `id`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Entity.List`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'View accounts in a list.',
		type: Entity.List,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'The conversation does not exist, or is not owned by you.',
	})
	@Get(':id/accounts')
	async getAccounts(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Query() query: any,
	) {
		if (!id) {
			throw new BadRequestException();
		}

		const options = {
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
		};

		return request.instance.getAccountsInList(id, options);
	}

	/**
	 * POST /api/v1/lists
	 * https://docs.joinmastodon.org/methods/lists/#create
	 *
	 * @params `id`
	 * @body `title`
	 * @returns `Entity.List`
	 */
	@ApiResponse({
		status: 200,
		description: 'Create a new list.',
		type: Entity.List,
	})
	@ApiResponse({
		status: 400,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 422.1,
		description: "Validation failed: Title can't be blank.",
	})
	@ApiResponse({
		status: 422.2,
		description: "'some' is not a valid replies_policy",
	})
	@Post()
	async create(@Request() request: RequestMirror, @Body('title') title: string) {
		if (!title) {
			throw new BadRequestException();
		}

		return request.instance.createList(title);
	}

	/**
	 * POST /api/v1/lists/:id/accounts
	 * https://docs.joinmastodon.org/methods/lists/#accounts-add
	 *
	 * @params `id`
	 * @body `account_ids`
	 * @returns `{}`
	 */
	@ApiResponse({
		status: 200,
		description:
			'Add accounts to the given list. Note that the user must be following these accounts.',
	})
	@ApiResponse({
		status: 400,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 422,
		description: 'Validation failed: Account has already been taken.',
	})
	@Post(':id/accounts')
	async addAccounts(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Body('account_ids') account_ids: string[],
	) {
		if (!id || !account_ids || !account_ids.length) {
			throw new BadRequestException();
		}

		return request.instance.addAccountsToList(id, account_ids);
	}

	/**
	 * PUT /api/v1/lists/:id
	 * https://docs.joinmastodon.org/methods/lists/#update
	 *
	 * @params `id`
	 * @body `title`
	 * @returns `Entity.List`
	 */
	@ApiResponse({
		status: 200,
		description: 'Change the title of a list, or which replies to show.',
		type: Entity.List,
	})
	@ApiResponse({
		status: 400.1,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 400.2,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Put(':id')
	async update(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Body('title') title: string,
	) {
		if (!id || !title) {
			throw new BadRequestException();
		}

		return request.instance.updateList(id, title);
	}

	/**
	 * DELETE /api/v1/lists/:id/accounts
	 * https://docs.joinmastodon.org/methods/lists/#delete
	 *
	 * @params `id`
	 * @body `account_ids`
	 * @returns `{}`
	 */
	@ApiResponse({
		status: 200,
		description: 'Delete a list.',
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Delete(':id')
	async delete(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.deleteList(id);
	}

	/**
	 * DELETE /api/v1/lists/:id/accounts
	 * https://docs.joinmastodon.org/methods/lists/#accounts-remove
	 *
	 * @params `id`
	 * @body `account_ids`
	 * @returns `{}`
	 */
	@ApiResponse({
		status: 200,
		description: 'Remove accounts from the given list.',
	})
	@ApiResponse({
		status: 400,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Delete(':id/accounts')
	async deleteAccounts(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Body() body: any,
	) {
		if (!id || !validateBody(body)) {
			throw new BadRequestException();
		}

		return request.instance.deleteAccountsFromList(id, body.account_ids);
	}
}
