import { Controller, Get, Request, UseFilters, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../utils/preprocess.guard';

import { AxiosErrorFilter } from '../../utils/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../utils/guards.types';

@ApiTags('Mirror · Instance · Emojis')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/custom_emojis')
export class MirrorInstanceEmojisController {
	/**
	 * GET /api/v1/custom_emojis
	 * https://docs.joinmastodon.org/methods/custom_emojis/#get
	 *
	 * @returns `Array<Entity.Emoji>`
	 */
	@ApiResponse({
		status: 200,
		description: 'Returns custom emojis that are available on the server.',
		type: Entity.Emoji,
		isArray: true,
	})
	@Get()
	async get(@Request() request: RequestMirror) {
		return request.instance.getInstanceCustomEmojis();
	}
}
