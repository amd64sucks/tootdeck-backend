import {
	Controller,
	Get,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../utils/preprocess.guard';

import { AxiosErrorFilter } from '../../utils/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../utils/guards.types';

@ApiTags('Mirror · Instance')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/instance')
export class MirrorInstanceController {
	/**
	 * GET /api/v1/instance
	 * https://docs.joinmastodon.org/methods/instance/#v1
	 *
	 * @returns `Entity.Instance`
	 */
	@ApiResponse({
		status: 200,
		description: 'Obtain general information about the server.',
		type: Entity.Instance,
	})
	@Get()
	async get(@Request() request: RequestMirror) {
		return request.instance.getInstance();
	}

	/**
	 * GET /api/v1/instance/peers
	 * https://docs.joinmastodon.org/methods/instance/#peers
	 *
	 * @returns `Array<String>`
	 */
	@ApiResponse({
		status: 200,
		description: 'Domains that this instance is aware of.',
		type: String,
		isArray: true,
	})
	@Get('peers')
	async peers(@Request() request: RequestMirror) {
		return request.instance.getInstancePeers();
	}

	/**
	 * GET /api/v1/instance/activity
	 * https://docs.joinmastodon.org/methods/instance/#activity
	 *
	 * @returns `Entity.Activity`
	 */
	@ApiResponse({
		status: 200,
		description: 'Instance activity over the last 3 months, binned weekly.',
		type: Entity.Activity,
	})
	@Get('activity')
	async activity(@Request() request: RequestMirror) {
		return request.instance.getInstanceActivity();
	}

	/**
	 * GET /api/v1/instance/rules
	 * https://docs.joinmastodon.org/methods/instance/#rules
	 */
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Rules that the users of this service should follow.',
	// })
	// @Get('rules')
	// async rules(@Request() request: RequestMirror) {}

	/**
	 * GET /api/v1/instance/domain_blocks
	 * https://docs.joinmastodon.org/methods/instance/#domain_blocks
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `min_id`
	 * @returns `Array<String>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'Obtain a list of domains that have been blocked.',
		type: String,
		isArray: true,
	})
	@Get('domain_blocks')
	async domain_blocks(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			min_id: query.min_id,
		};

		return request.instance.getDomainBlocks(options);
	}

	/**
	 * GET /api/v1/instance/extended_description
	 * https://docs.joinmastodon.org/methods/instance/#extended_description
	 */
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Obtain an extended description of this server.',
	// })
	// @Get('extended_description')
	// async extended_description(@Request() request: RequestMirror) {}
}
