import {
	Controller,
	Get,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../utils/preprocess.guard';

import { AxiosErrorFilter } from '../../utils/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../utils/guards.types';

@ApiTags('Mirror · Instance · Directory')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/directory')
export class MirrorInstanceDirectoryController {
	/**
	 * GET /api/v1/directory
	 * https://docs.joinmastodon.org/methods/directory/#get
	 *
	 * @query `limit`
	 * @query `offset`
	 * @query `order`
	 * @query `local`
	 * @returns `Array<Entity.Account>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description: 'How many accounts to load. Defaults to 40 accounts. Max 80 accounts.',
		type: Number,
	})
	@ApiQuery({
		name: 'offset',
		required: false,
		description: 'Skip the first n results.',
		type: Number,
	})
	@ApiQuery({
		name: 'order',
		required: false,
		description:
			'Use active to sort by most recently posted statuses (default) or new to sort by most recently created profiles.',
		type: Number,
	})
	@ApiQuery({
		name: 'local',
		required: false,
		description: 'If true, returns only local accounts.',
		type: Boolean,
	})
	@ApiResponse({
		status: 200,
		description: 'List accounts visible in the directory.',
		type: Entity.Account,
		isArray: true,
	})
	@Get()
	async trends(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			limit: query.limit,
			offset: query.offset,
			order: query.order,
			local: query.local,
		};

		return request.instance.getInstanceDirectory(options);
	}
}
