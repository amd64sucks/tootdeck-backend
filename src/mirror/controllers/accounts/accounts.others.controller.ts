import {
	BadRequestException,
	Body,
	Controller,
	Get,
	Post,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../utils/preprocess.guard';

import { AxiosErrorFilter } from '../../utils/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { validateBody } from '../../utils/validateBody';

import { Entity } from '../../properties/entities.property';
import { Body as FormBody } from '../../properties/body.property';

import { RequestMirror } from '../../utils/guards.types';

@ApiTags('Mirror · Accounts · Others')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/')
export class MirrorAccountsOthersController {
	/**
	 * GET /api/v1/bookmarks
	 * https://docs.joinmastodon.org/methods/bookmarks/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'Statuses the user has bookmarked.',
		type: Entity.Status,
		isArray: true,
	})
	@Get('bookmarks')
	async bookmarks(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
		};

		return request.instance.getBookmarks(options);
	}

	/**
	 * GET /api/v1/favourites
	 * https://docs.joinmastodon.org/methods/favourites/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'Statuses the user has favourited.',
		type: Entity.Status,
		isArray: true,
	})
	@Get('favourites')
	async favourites(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
		};

		return request.instance.getFavourites(options);
	}

	/**
	 * GET /api/v1/mutes
	 * https://docs.joinmastodon.org/methods/mutes/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Account>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'Statuses the user has favourited.',
		type: Entity.Account,
		isArray: true,
	})
	@Get('mutes')
	async mutes(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			min_id: query.min_id,
		};

		return request.instance.getMutes(options);
	}

	/**
	 * GET /api/v1/blocks
	 * https://docs.joinmastodon.org/methods/blocks/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Account>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'View domains the user has blocked.',
		type: Entity.Account,
		isArray: true,
	})
	@Get('blocks')
	async blocks(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			min_id: query.min_id,
		};

		return request.instance.getBlocks(options);
	}

	/**
	 * POST /api/v1/reports
	 * https://docs.joinmastodon.org/methods/reports/#post
	 *
	 * @body `FormBody.Report`
	 * @returns `Array<String>`
	 */
	@ApiBody({
		type: FormBody.Report,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'File a report.',
		type: Entity.Report,
	})
	@ApiResponse({
		status: 400,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@ApiResponse({
		status: 422.1,
		description: 'This method requires an authenticated user.',
	})
	@ApiResponse({
		status: 422.2,
		description: 'Validation failed: Rule ids does not reference valid rules.',
	})
	@ApiResponse({
		status: 422.3,
		description: 'Validation failed: Rule ids must be blank.',
	})
	@Post('reports')
	async reports(@Request() request: RequestMirror, @Body() body: FormBody.Report) {
		if (!validateBody(body) || !body.account_id || !body.comment) {
			throw new BadRequestException();
		}

		const options = {
			status_ids: body.status_ids,
			comment: body.comment,
			forward: body.forward,
			category: body.category,
			rule_ids: body.rule_ids,
		} as any;

		return request.instance.report(body.account_id, options);
	}

	/**
	 * GET /api/v1/endorsements
	 * https://docs.joinmastodon.org/methods/endorsements/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @returns `Array<Entity.Account>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'Accounts that the user is currently featuring on their profile.',
		type: Entity.Account,
		isArray: true,
	})
	@Get('endorsements')
	async endorsements(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			min_id: query.min_id,
		};

		return request.instance.getEndorsements(options);
	}

	/**
	 * GET /api/v1/preferences
	 * https://docs.joinmastodon.org/methods/preferences/#get
	 *
	 * @returns `Entity.Preferences`
	 */
	@ApiResponse({
		status: 200,
		description: 'Accounts that the user is currently featuring on their profile.',
		type: Entity.Preferences,
	})
	@Get('preferences')
	async preferences(@Request() request: RequestMirror) {
		return request.instance.getPreferences();
	}

	/**
	 * GET /api/v1/followed_tags
	 * https://docs.joinmastodon.org/methods/followed_tags/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Account>`
	 */
	// @ApiQuery({
	// 	name: 'limit',
	// 	required: false,
	// 	description:
	// 		'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
	// 	type: Number,
	// })
	// @ApiQuery({
	// 	name: 'max_id',
	// 	required: false,
	// 	description: 'Return results older than ID.',
	// 	type: String,
	// })
	// @ApiQuery({
	// 	name: 'since_id',
	// 	required: false,
	// 	description: 'Return results newer than ID.',
	// 	type: String,
	// })
	// @ApiQuery({
	// 	name: 'min_id',
	// 	required: false,
	// 	description: 'Return results immediately newer than ID.',
	// 	type: String,
	// })
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'View all followed tags.',
	// 	type: Entity.Tag,
	// 	isArray: true,
	// })
	// @Get('followed_tags')
	// async followed_tags(@Request() request: RequestMirror, @Query() query: any) {}

	/**
	 * GET /api/v1/suggestions
	 * https://docs.joinmastodon.org/methods/suggestions/#v1
	 *
	 * @deprecated Deprecated endpoint but Megalodon use it
	 * @returns `Array<Entity.Account>`
	 */
	@ApiResponse({
		status: 200,
		description:
			'Accounts the user has had past positive interactions with, but is not yet following.',
		type: Entity.Account,
		isArray: true,
	})
	@Get('suggestions')
	async suggestions(@Request() request: RequestMirror) {
		return request.instance.getSuggestions();
	}
}
