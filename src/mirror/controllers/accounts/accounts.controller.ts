import {
	BadRequestException,
	Body,
	Controller,
	Get,
	Next,
	Param,
	Post,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiParam, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../utils/preprocess.guard';

import { AxiosErrorFilter } from '../../utils/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { validateBody } from '../../utils/validateBody';

import { Entity } from '../../properties/entities.property';
import { Body as FormBody } from '../../properties/body.property';

import { RequestMirror } from '../../utils/guards.types';

@ApiTags('Mirror · Accounts')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/accounts')
export class MirrorAccountsController {
	/**
	 * GET /api/v1/accounts/verify_credentials
	 * https://docs.joinmastodon.org/methods/accounts/#verify_credentials
	 *
	 * @returns `Entity.Account`
	 */
	@ApiResponse({
		status: 200,
		description: 'Test to make sure that the user token works.',
		type: Entity.Account,
	})
	@Get('verify_credentials')
	async verifyAccountCredentials(@Request() request: RequestMirror) {
		return request.instance.verifyAccountCredentials();
	}

	/**
	 * GET /api/v1/accounts/search
	 * https://docs.joinmastodon.org/methods/accounts/#search
	 *
	 * @query `resolve`
	 * @query `following`
	 * @query `offset`
	 * @query `limit`
	 * @query `q`
	 * @returns `Array<Entity.Account>`
	 */
	@ApiQuery({
		name: 'q',
		required: true,
		description: 'Search query for accounts.',
	})
	@ApiQuery({
		name: 'resolve',
		required: false,
		description:
			'Attempt WebFinger lookup. Defaults to false. Use this when q is an exact address.',
	})
	@ApiQuery({
		name: 'following',
		required: false,
		description: 'Limit the search to users you are following. Defaults to false.',
	})
	@ApiQuery({
		name: 'offset',
		required: false,
		description: 'Skip the first n results.',
	})
	@ApiQuery({
		name: 'limit',
		required: false,
		description: 'Maximum number of results. Defaults to 40 accounts. Max 80 accounts.',
	})
	@ApiResponse({
		status: 200,
		description: 'Search for matching accounts by username or display name.',
		type: Entity.Account,
		isArray: true,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing search query.',
	})
	@Get('search')
	async searchAccount(@Request() request: RequestMirror, @Query() query: any) {
		if (!query.q) {
			throw new BadRequestException();
		}

		const { q, ...optional } = query;
		const options = {
			following: optional.following,
			resolve: optional.resolve,
			limit: optional.limit,
			max_id: optional.max_id,
			since_id: optional.since_id,
		};

		return request.instance.searchAccount(q, options);
	}

	/**
	 * GET /api/v1/accounts/relationships
	 * https://docs.joinmastodon.org/methods/accounts/#relationships
	 *
	 * @query `Array<id>`
	 * @returns `Array<Entity.Relationship>`
	 */
	@ApiQuery({
		name: 'id',
		type: String,
		isArray: true,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Find out whether a given account is followed, blocked, muted, etc.',
		type: Entity.Relationship,
		isArray: true,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing IDs in query.',
	})
	@Get('relationships')
	async relationships(@Request() request: RequestMirror, @Query('id[]') _id: string | string[]) {
		let id: string[];

		if (_id && typeof _id === 'string') {
			id = [_id];
		} else if (_id instanceof Array && _id.length) {
			id = _id;
		} else {
			throw new BadRequestException();
		}

		return request.instance.getRelationships(id);
	}

	/**
	 * GET /api/v1/accounts/:id
	 * https://docs.joinmastodon.org/methods/accounts/#get
	 *
	 * @param `id`
	 * @returns `Entity.Account`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'View information about a profile.',
		type: Entity.Account,
	})
	@Get(':id')
	async getAccount(@Request() request: RequestMirror, @Param('id') id: string) {
		if (request.originalUrl.includes('/api/v1/accounts/search')) Next();

		return request.instance.getAccount(id);
	}

	/**
	 * GET /api/v1/accounts/:id/statuses
	 * https://docs.joinmastodon.org/methods/accounts/#statuses
	 *
	 * @param `id`
	 * @query limit`
	 * @query max_id`
	 * @query since_id`
	 * @query min_id`
	 * @query exclude_replies`
	 * @query exclude_reblogs`
	 * @query only_media`
	 * @returns `Array<Entity.Status>`
	 *
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiQuery({
		name: 'limit',
		description:
			'Maximum number of results to return. Defaults to 40 accounts. Max 80 accounts',
		type: Number,
		required: false,
	})
	@ApiQuery({
		name: 'max_id',
		type: String,
		required: false,
	})
	@ApiQuery({
		name: 'since_id',
		type: String,
		required: false,
	})
	@ApiQuery({
		name: 'min_id',
		type: Boolean,
		required: false,
	})
	@ApiQuery({
		name: 'exclude_replies',
		type: Boolean,
		required: false,
	})
	@ApiQuery({
		name: 'exclude_reblogs',
		type: Boolean,
		required: false,
	})
	@ApiQuery({
		name: 'only_media',
		type: Boolean,
		required: false,
	})
	@ApiResponse({
		status: 200,
		description: 'Statuses posted to the given account.',
		type: Entity.Status,
		isArray: true,
	})
	@Get(':id/statuses')
	async getAccountStatuses(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Query() query: any,
	) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
			pinned: query.pinned,
			exclude_replies: query.exclude_replies,
			exclude_reblogs: query.exclude_reblogs,
			only_media: query.only_media,
		};

		return request.instance.getAccountStatuses(id, options);
	}

	/**
	 * GET /api/v1/accounts/:id/followers
	 * https://docs.joinmastodon.org/methods/accounts/#followers
	 *
	 * @param `id`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `get_all`
	 * @query `sleep_ms`
	 * @returns `Array<Entity.Account>`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiQuery({
		name: 'limit',
		description:
			'Maximum number of results to return. Defaults to 40 accounts. Max 80 accounts',
		type: Number,
		required: false,
	})
	@ApiQuery({
		name: 'max_id',
		type: String,
		required: false,
	})
	@ApiQuery({
		name: 'since_id',
		type: String,
		required: false,
	})
	@ApiQuery({
		name: 'get_all',
		type: Boolean,
		required: false,
	})
	@ApiResponse({
		status: 200,
		description:
			'Accounts which follow the given account, if network is not hidden by the account owner.',
		type: Entity.Account,
		isArray: true,
	})
	@Get(':id/followers')
	async getAccountFollowers(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Query() query: any,
	) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			get_all: query.get_all,
		};

		return request.instance.getAccountFollowers(id, options);
	}

	/**
	 * GET /api/v1/accounts/:id/following
	 * https://docs.joinmastodon.org/methods/accounts/#following
	 *
	 * @param `id`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `get_all`
	 * @query `sleep_ms`
	 * @returns `Array<Entity.Account>`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiQuery({
		name: 'limit',
		description:
			'Maximum number of results to return. Defaults to 40 accounts. Max 80 accounts',
		type: Number,
		required: false,
	})
	@ApiQuery({
		name: 'max_id',
		type: String,
		required: false,
	})
	@ApiQuery({
		name: 'since_id',
		type: String,
		required: false,
	})
	@ApiQuery({
		name: 'get_all',
		type: Boolean,
		required: false,
	})
	@ApiResponse({
		status: 200,
		description:
			'Accounts which the given account is following, if network is not hidden by the account owner.',
		type: Entity.Account,
		isArray: true,
	})
	@Get(':id/following')
	async getAccountFollowing(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Query() query: any,
	) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			get_all: query.get_all,
		};

		return request.instance.getAccountFollowing(id, options);
	}

	/**
	 * GET /api/v1/accounts/:id/featured_tags
	 * https://docs.joinmastodon.org/methods/accounts/#featured_tags
	 *
	 * @param `id`
	 */
	// @ApiParam({
	// 	name: 'id',
	// 	type: String,
	// 	required: true,
	// })
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Tags featured by this account.',
	// })
	// @Get(':id/featured_tags')
	// async featured_tags(@Request() request: RequestMirror, @Param('id') id: string) {}

	/**
	 * GET /api/v1/accounts/:id/lists
	 * https://docs.joinmastodon.org/methods/accounts/#lists
	 *
	 * @param `id`
	 * @returns `Array<Entity.List>`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'User lists that you have added this account to.',
		type: Entity.List,
		isArray: true,
	})
	@Get(':id/lists')
	async getAccountLists(@Request() request: RequestMirror, @Param('id') id: string) {
		return request.instance.getAccountLists(id);
	}

	/**
	 * GET /api/v1/accounts/familiar_followers
	 * https://docs.joinmastodon.org/methods/accounts/#familiar_followers
	 */
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Obtain a list of all accounts that follow a given account, filtered for accounts you follow.',
	// })
	// @Get('familiar_followers')
	// async familiar_followers(@Request() request: RequestMirror) {}

	/**
	 * GET /api/v1/accounts/lookup
	 * https://docs.joinmastodon.org/methods/accounts/#lookup
	 */
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Quickly lookup a username to see if it is available, skipping WebFinger resolution.',
	// })
	// @Get('lookup')
	// async lookup(@Request() request: RequestMirror) {}

	/**
	 * POST /api/v1/accounts/:id/follow
	 * https://docs.joinmastodon.org/methods/accounts/#follow
	 *
	 * @param `id`
	 * @body `Body.Follow`
	 * @returns `Entity.Relationship`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiBody({
		type: FormBody.Follow,
	})
	@ApiResponse({
		status: 200,
		description:
			'Follow the given account. Can also be used to update whether to show reblogs or enable notifications.',
		type: Entity.Relationship,
	})
	@Post(':id/follow')
	async followAccount(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Body() body: FormBody.Follow,
	) {
		if (!id) {
			throw new BadRequestException();
		}

		const options = {
			reblog: body.reblog ?? true,
		};

		return request.instance.followAccount(id, options);
	}

	/**
	 * POST /api/v1/accounts/:id/unfollow
	 * https://docs.joinmastodon.org/methods/accounts/#unfollow
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Unfollow the given account.',
		type: Entity.Relationship,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@Post(':id/unfollow')
	async unfollowAccount(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.unfollowAccount(id);
	}

	/**
	 * POST /api/v1/accounts/:id/remove_from_followers
	 * https://docs.joinmastodon.org/methods/accounts/#remove_from_followers
	 *
	 * @param `id`
	 */
	// @ApiParam({
	// 	name: 'id',
	// 	type: String,
	// 	required: true,
	// })
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Remove the given account from your followers.',
	// })
	// @Post(':id/remove_from_followers')
	// async remove_from_followers(@Request() request: RequestMirror, @Param('id') id: string) {}

	/**
	 * POST /api/v1/accounts/:id/block
	 * https://docs.joinmastodon.org/methods/accounts/#block
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description:
			'Block the given account. Clients should filter statuses from this account if received (e.g. due to a boost in the Home timeline)',
		type: Entity.Relationship,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@Post(':id/block')
	async blockAccount(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.blockAccount(id);
	}

	/**
	 * POST /api/v1/accounts/:id/unblock
	 * https://docs.joinmastodon.org/methods/accounts/#unblock
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Unblock the given account.',
		type: Entity.Relationship,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@Post(':id/unblock')
	async unblockAccount(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.unblockAccount(id);
	}

	/**
	 * POST /api/v1/accounts/:id/mute
	 * https://docs.joinmastodon.org/methods/accounts/#mute
	 *
	 * @param `id`
	 * @body `Body.Mute`
	 * @returns `Entity.Relationship`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiBody({
		type: FormBody.Mute,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description:
			'Mute the given account. Clients should filter statuses and notifications from this account, if received (e.g. due to a boost in the Home timeline).',
		type: Entity.Relationship,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@Post(':id/mute')
	async muteAccount(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Body() body: FormBody.Mute,
	) {
		if (!id || !validateBody(body)) {
			throw new BadRequestException();
		}

		return request.instance.muteAccount(id, body.notifications ?? true);
	}

	/**
	 * POST /api/v1/accounts/:id/unmute
	 * https://docs.joinmastodon.org/methods/accounts/#unmute
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Unmute the given account.',
		type: Entity.Relationship,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@Post(':id/unmute')
	async unmuteAccount(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.unmuteAccount(id);
	}

	/**
	 * POST /api/v1/accounts/:id/pin
	 * https://docs.joinmastodon.org/methods/accounts/#pin
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description:
			'Add the given account to the user’s featured profiles. (Featured profiles are currently shown on the user’s own public profile.)',
		type: Entity.Relationship,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@Post(':id/pin')
	async pinAccount(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.pinAccount(id);
	}

	/**
	 * POST /api/v1/accounts/:id/unpin
	 * https://docs.joinmastodon.org/methods/accounts/#unpin
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Remove the given account from the user’s featured profiles.',
		type: Entity.Relationship,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@Post(':id/unpin')
	async unpinAccount(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.unpinAccount(id);
	}

	/**
	 * POST /api/v1/accounts/:id/note
	 * https://docs.joinmastodon.org/methods/accounts/#note
	 *
	 * @param `id`
	 */
	// @ApiParam({
	// 	name: 'id',
	// 	type: String,
	// 	required: true,
	// })
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Sets a private note on a user.',
	// })
	// @Post(':id/note')
	// async note(@Request() request: RequestMirror, @Param('id') id: string) {}
}
