import { BadRequestException, Body, Controller, Get, Param, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../auth/guards/access.guard';

import { AvatarCacheService } from '../services/avatar.service';
import { InteractionsService } from '../services/Interactions.service';

import { parseHandle } from '../../utils/parse.handle';

@ApiTags('Mirror · Injector')
@UseGuards(AccessGuard)
@Controller('mirror')
export class MirrorInjectorController {
	constructor(
		private readonly AvatarService: AvatarCacheService,
		private readonly InteractionsService: InteractionsService,
	) {}

	@Get('avatar/:handle')
	async avatar(@Param('handle') handle: string) {
		const parsed = parseHandle(handle);
		return this.AvatarService.getAvatar(parsed);
	}

	@Get('interactions')
	async interactions(@Body('url') url: string) {
		if (!url) {
			throw new BadRequestException();
		}

		return this.InteractionsService.fetch(url);
	}
}
