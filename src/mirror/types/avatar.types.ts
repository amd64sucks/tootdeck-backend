export interface CachedAvatarResponse {
	avatar: string;
	avatar_static: string;
}
