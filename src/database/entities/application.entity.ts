import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { InstanceType } from '../types';

@Entity()
export class ApplicationStore {
	@PrimaryGeneratedColumn('uuid')
	uuid: string;

	@Column({ nullable: false, unique: true })
	domain: string;

	@Column({ nullable: false })
	type: InstanceType;

	@Column({ nullable: false })
	key: string;

	@Column({ nullable: false })
	secret: string;

	@Column({ nullable: true })
	token: string;

	@Column({ nullable: true })
	streaming_url: string;

	@Column({ nullable: false })
	created_at: Date;

	@Column({ nullable: false })
	updated_at: Date;
}
