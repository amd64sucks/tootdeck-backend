import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ConfigStore {
	@PrimaryGeneratedColumn('uuid')
	uuid: string;

	@Column({ nullable: false })
	value: string;

	@Column({ nullable: false })
	updated_at: Date;
}
