import { Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { AccountStore } from './account.entity';
import { ConfigStore } from './config.entity';

@Entity()
export class UserStore {
	@PrimaryGeneratedColumn('uuid')
	uuid: string;

	@ManyToOne((type) => AccountStore, (user) => user.uuid, {
		nullable: false,
	})
	main: AccountStore;

	@ManyToMany((type) => AccountStore, (user) => user.uuid, {
		nullable: false,
	})
	@JoinTable({
		name: 'user_account',
	})
	secondary: AccountStore[];

	@ManyToOne((type) => ConfigStore, (config) => config.uuid, {
		nullable: true,
	})
	config: ConfigStore | null;

	addSecondary(account: AccountStore) {
		if (!this.secondary) {
			this.secondary = [];
		}
		this.secondary.push(account);
	}

	removeSecondary(account: AccountStore) {
		if (!this.secondary) {
			return;
		}
		this.secondary = this.secondary.filter((a) => a.uuid !== account.uuid);
	}
}
