export enum InstanceType {
	Mastodon = 'mastodon',
	Pleroma = 'pleroma',
	Misskey = 'misskey',
	// Friendica = 'friendica',
}

export enum DeleteUser {
	False = 0,
	True = 1,
	RemoveAccount = 2,
}
