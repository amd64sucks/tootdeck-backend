import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UserStore } from '../entities/user.entity';
import { AccountStore } from '../entities/account.entity';

import { Logger } from '../../utils/logger';

import { DeleteUser } from '../types';
import { ConfigStore } from '../entities/config.entity';

@Injectable()
export class DBUserService {
	private readonly logger = new Logger(DBUserService.name);

	constructor(
		@InjectRepository(UserStore) private readonly userRepo: Repository<UserStore>,
		@InjectRepository(ConfigStore) private readonly configRepo: Repository<ConfigStore>,
	) {}

	/**
	 * Utils
	 */
	//#region

	private async usedIn(account: AccountStore) {
		const used = await this.userRepo
			.find({
				where: [{ main: { uuid: account.uuid } }, { secondary: { uuid: account.uuid } }],
				relations: ['secondary', 'secondary.application'],
			})
			.catch((e) => {
				this.logger.error(
					'usedIn',
					`Unable to find user for @${account.username}@${account.application.domain}`,
					e,
				);
				return null;
			});

		if (!used) {
			return null;
		}

		if (!used.length) {
			return false;
		}

		return true;
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	async get(account: AccountStore) {
		return this.userRepo
			.findOne({
				where: { main: { uuid: account.uuid } },
				relations: ['main', 'main.application', 'secondary', 'secondary.application'],
			})
			.catch((e) => {
				this.logger.error(
					'get',
					`Unable to find user for @${account.username}@${account.application.domain}`,
					e,
				);
				return null;
			});
	}

	async getInMainOrSecondary(account: AccountStore) {
		return this.userRepo
			.find({
				where: [{ main: { uuid: account.uuid } }, { secondary: { uuid: account.uuid } }],
				relations: ['main', 'main.application', 'secondary', 'secondary.application'],
			})
			.catch((e) => {
				this.logger.error(
					'get',
					`Unable to find user for @${account.username}@${account.application.domain}`,
					e,
				);
				return null;
			});
	}

	async getByUUID(uuid: string) {
		return this.userRepo
			.findOne({
				where: { uuid },
				relations: [
					'main',
					'main.application',
					'secondary',
					'secondary.application',
					'config',
				],
			})
			.catch((e) => {
				this.logger.error('getByUUID', `Unable to find user with uuid ${uuid}`, e);
				return null;
			});
	}

	async getMain(account: AccountStore) {
		return this.userRepo
			.findOne({
				where: { main: { uuid: account.uuid } },
				relations: ['main'],
			})
			.catch((e) => {
				this.logger.error(
					'getLite',
					`Unable to find user for @${account.username}@${account.application.domain}`,
					e,
				);
				return null;
			});
	}

	async create(account: AccountStore) {
		const exist = await this.getMain(account);
		if (exist) {
			this.logger.error(
				'store',
				`Account already exist for @${account.username}@${account.application.domain}`,
			);
			return null;
		}

		const entity = this.userRepo.create({
			main: account,
		});

		return this.userRepo.save(entity).catch((e) => {
			this.logger.error(
				'store',
				`Unable to store account for @${account.username}@${account.application.domain}`,
				e,
			);
			return null;
		});
	}

	async addAccount(user: UserStore, new_account: AccountStore): Promise<UserStore | null>;
	async addAccount(account: AccountStore, new_account: AccountStore): Promise<UserStore | null>;
	async addAccount(
		user_or_account: UserStore | AccountStore,
		new_account: AccountStore,
	): Promise<UserStore | null> {
		let entry: UserStore | null = null;

		if (user_or_account instanceof UserStore) {
			entry = user_or_account;
		} else if (user_or_account instanceof AccountStore) {
			entry = await this.get(user_or_account);
			if (!entry) {
				this.logger.error(
					'addAccount',
					`No user with main account @${user_or_account.username}@${user_or_account.application.domain}`,
				);
				return null;
			}
		}
		if (!entry) {
			return null;
		}

		if (entry.secondary?.find((a) => a.uuid === new_account.uuid)) {
			this.logger.error(
				'addAccount',
				`Account @${new_account.username}@${new_account.application.domain} already exist for @${entry.main.username}@${entry.main.application.domain}`,
			);
			return null;
		}

		entry.addSecondary(new_account);

		return this.userRepo.save(entry).catch((e) => {
			this.logger.error(
				'addAccount',
				`Unable to store account for @${entry!.main.username}@${
					entry!.main.application.domain
				}`,
				e,
			);
			return null;
		});
	}

	async deleteAccount(account: AccountStore, remove_account: AccountStore): Promise<DeleteUser> {
		const entry = await this.get(account);
		if (!entry) {
			this.logger.warn(
				'deleteAccount',
				`Attempting to remove account @${remove_account.username}@${remove_account.application.domain} but user for @${account.username}@${account.application.domain} doesn't exist`,
			);
			return DeleteUser.False;
		}

		if (!entry.secondary.find((x) => x.uuid === remove_account.uuid)) {
			this.logger.warn(
				'deleteAccount',
				`Attempting to remove account @${remove_account.username}@${remove_account.application.domain} for @${account.username}@${account.application.domain} but it doesn't exist`,
			);
			return DeleteUser.False;
		}

		entry.removeSecondary(remove_account);

		const del = await this.userRepo.save(entry).catch((e) => {
			this.logger.error(
				'deleteAccount',
				`Unable to remove account @${account.username}@${account.application.domain}`,
				e,
			);
			return null;
		});
		if (!del) {
			return DeleteUser.False;
		}

		const used = await this.usedIn(remove_account);
		if (used === null) {
			return DeleteUser.False;
		}
		if (used === false) {
			return DeleteUser.RemoveAccount;
		}

		return DeleteUser.True;
	}

	async delete(account: AccountStore): Promise<DeleteUser> {
		const entry = await this.get(account);
		if (!entry) {
			this.logger.warn(
				'delete',
				`Attempting to delete user @${account.username}@${account.application.domain} but it doesn't exist`,
			);
			return DeleteUser.False;
		}

		const del_config = await this.deleteConfig(entry);
		if (del_config === null) {
			return DeleteUser.False;
		}

		const del = await this.userRepo.delete(entry.uuid).catch((e) => {
			this.logger.error(
				'delete',
				`Unable to delete user @${account.username}@${account.application.domain}`,
				e,
			);
			return null;
		});
		if (!del) {
			return DeleteUser.False;
		}

		const used = await this.usedIn(account);
		if (used === null) {
			return DeleteUser.False;
		}
		if (used === false) {
			return DeleteUser.RemoveAccount;
		}

		return DeleteUser.True;
	}

	//#endregion

	/**
	 * Config
	 */
	//#region

	async getConfig(uuid: string) {
		return this.userRepo
			.findOne({
				where: { uuid },
				relations: ['config'],
			})
			.catch((e) => {
				this.logger.error('getConfig', `Unable to find user with uuid ${uuid}`, e);
				return null;
			});
	}

	async addConfig(user: UserStore, config_value: string) {
		let config = user.config;
		if (config) {
			config.value = config_value;
			config.updated_at = new Date();
		} else {
			config = this.configRepo.create({
				value: config_value,
				updated_at: new Date(),
			});
		}

		const store = await this.configRepo.save(config).catch((e) => {
			this.logger.error(
				'addConfig',
				`Unable to store user config for @${user.main.username}@${user.main.application.domain}`,
				e,
			);
			return null;
		});

		if (!store) {
			return null;
		}

		user.config = config;

		return this.userRepo.save(user).catch((e) => {
			this.logger.error(
				'addConfig',
				`Unable to update user for @${user.main.username}@${user.main.application.domain}`,
				e,
			);
			return null;
		});
	}

	async deleteConfig(user: UserStore) {
		const config = user.config;
		if (!config) {
			return false;
		}

		user.config = null;

		const ret = await this.userRepo.save(user).catch((e) => {
			this.logger.error(
				'deleteConfig',
				`Unable to delete user config for @${user.main.username}@${user.main.application.domain}`,
				e,
			);
			return null;
		});
		if (!ret) {
			return null;
		}

		return this.configRepo.delete(config.uuid).catch((e) => {
			this.logger.error(
				'deleteConfig',
				`Unable to delete user config for @${user.main.username}@${user.main.application.domain}`,
				e,
			);
			return null;
		});
	}

	//#endregion
}
