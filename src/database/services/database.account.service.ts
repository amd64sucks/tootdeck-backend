import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';

import { DBApplicationService } from './database.application.service';

import { AccountStore } from '../entities/account.entity';

import { Logger } from '../../utils/logger';

@Injectable()
export class DBAccountService {
	private readonly logger = new Logger(DBAccountService.name);

	constructor(
		@InjectRepository(AccountStore) private readonly repo: Repository<AccountStore>,
		@Inject(forwardRef(() => DBApplicationService))
		private readonly DBApplicationService: DBApplicationService,
	) {}

	async get(username: string, domain: string) {
		return this.repo
			.findOne({
				where: { username: ILike(username), application: { domain } },
				relations: ['application'],
			})
			.catch((e) => {
				this.logger.error('get', `Unable to find account for @${username}@${domain}`, e);
				return null;
			});
	}

	async getByUUID(uuid: string) {
		return this.repo.findOne({ where: { uuid }, relations: ['application'] }).catch((e) => {
			this.logger.error('getByUUID', `Unable to find account with uuid ${uuid}`, e);
			return null;
		});
	}

	async create(username: string, domain: string, token: string) {
		const exist = await this.get(username, domain);
		if (exist) {
			this.logger.error('update', `Account already exist for @${username}@${domain}`);
			return null;
		}

		const application = await this.DBApplicationService.get(domain);
		if (!application) {
			this.logger.error('update', `${domain} doesn't exist`);
			return null;
		}

		const entity = this.repo.create({
			username,
			token,
			application,
		});

		return this.repo.save(entity).catch((e) => {
			this.logger.error('store', `Unable to store account for @${username}@${domain}`, e);
			return null;
		});
	}

	async update(username: string, domain: string, token: string) {
		if (!token) {
			this.logger.error('update', `Invalid token for @${username}@${domain}`);
			return null;
		}

		const entity = await this.get(username, domain);
		if (!entity) {
			this.logger.error('update', `Account doesn't exist for @${username}@${domain}`);
			return null;
		}

		entity.token = token;

		return this.repo.save(entity).catch((e) => {
			this.logger.error('update', `Unable to update account for @${username}@${domain}`, e);
			return null;
		});
	}

	async delete(username: string, domain: string) {
		const entry = await this.get(username, domain);
		if (!entry) {
			this.logger.warn(
				'store',
				`Attempting to delete account @${username}@${domain} but it doesn't exist`,
			);
			return null;
		}

		return this.repo.delete(entry.uuid).catch((e) => {
			this.logger.error('delete', `Unable to delete account @${username}@${domain}`, e);
			return null;
		});
	}
}
