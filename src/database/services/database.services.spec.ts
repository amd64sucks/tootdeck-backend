import { Test, TestingModule } from '@nestjs/testing';
import { randomUUID } from 'crypto';

import { DBApplicationService } from './database.application.service';
import { DBAccountService } from './database.account.service';
import { DBUserService } from './database.user.service';

import { ApplicationStore } from '../entities/application.entity';
import { AccountStore } from '../entities/account.entity';

import { DeleteUser, InstanceType } from '../types';
import { FullAppModule } from '../../app/app.module';

describe('DBServices', () => {
	let ApplicationDB: DBApplicationService;
	let AccountDB: DBAccountService;
	let UserDB: DBUserService;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		ApplicationDB = app.get<DBApplicationService>(DBApplicationService);
		AccountDB = app.get<DBAccountService>(DBAccountService);
		UserDB = app.get<DBUserService>(DBUserService);
	});

	/**
	 * DBApplicationService
	 */

	const test_domain = 'domain';

	describe('DBApplicationService', () => {
		describe('store', () => {
			it('storing valid entry, should return true', async () => {
				expect(
					!!(await ApplicationDB.create(
						test_domain,
						InstanceType.Mastodon,
						'key',
						'secret',
					)),
				).toBe(true);
			});

			it('already exist, should return false', async () => {
				expect(
					!!(await ApplicationDB.create(
						test_domain,
						InstanceType.Mastodon,
						'key',
						'secret',
					)),
				).toBe(false);
			});
		});

		describe('get', () => {
			it('getting valid entry, should return true', async () => {
				const { uuid, created_at, updated_at, ...filtered } = (await ApplicationDB.get(
					test_domain,
				)) as ApplicationStore;

				expect(filtered).toStrictEqual({
					domain: test_domain,
					type: InstanceType.Mastodon,
					key: 'key',
					streaming_url: null,
					secret: 'secret',
					token: null,
				});
			});

			it('getting invalid entry, should return null', async () => {
				expect(await ApplicationDB.get('nothing')).toBe(null);
			});
		});

		describe('update', () => {
			it('updating updated_at with valid entry, should return true', async () => {
				const old = ((await ApplicationDB.get(test_domain)) as ApplicationStore).updated_at;
				const updated = await ApplicationDB.updateLastUsage(test_domain);

				if (!updated) {
					expect(!!updated).toBe(true);
					return;
				}

				expect(old !== updated.updated_at).toBe(true);
			});

			it('updating secrets with valid entry, should return entry', async () => {
				expect(
					await ApplicationDB.updateCredentials(test_domain, 'new_key', 'new_secret'),
				).toMatchObject({
					domain: test_domain,
					key: 'new_key',
					secret: 'new_secret',
					type: InstanceType.Mastodon,
				} as ApplicationStore);
			});

			it('updating secrets with valid entry, should return entry', async () => {
				expect(!!(await AccountDB.create('test_username', test_domain, 'test_token'))).toBe(
					true,
				);
				expect(
					!!(await AccountDB.create('test_username1', test_domain, 'test_token')),
				).toBe(true);
				expect(
					!!(await AccountDB.create('test_username2', test_domain, 'test_token')),
				).toBe(true);

				expect(
					await ApplicationDB.updateCredentials(test_domain, 'new_key', 'new_secret'),
				).toMatchObject({
					domain: test_domain,
					key: 'new_key',
					secret: 'new_secret',
					type: InstanceType.Mastodon,
				} as ApplicationStore);

				await Promise.all([
					AccountDB.delete('test_username', test_domain),
					AccountDB.delete('test_username1', test_domain),
					AccountDB.delete('test_username2', test_domain),
				]);
			});

			it('nothing to update, should return false', async () => {
				expect(await ApplicationDB.updateLastUsage('nothing')).toBe(null);
			});

			it('nothing to update, should return false', async () => {
				expect(await ApplicationDB.updateLastUsage(undefined as any)).toBe(null);
			});
		});

		describe('delete', () => {
			it('deleting valid entry, should return true', async () => {
				expect(!!(await ApplicationDB.delete(test_domain))).toBe(true);
			});

			it('getting after delete, should return null', async () => {
				expect(await ApplicationDB.get(test_domain)).toBe(null);
			});

			it('nothing to delete, should return false', async () => {
				expect(!!(await ApplicationDB.delete(test_domain))).toBe(false);
			});
		});
	});

	/**
	 * DBAccountService
	 */
	const test_username = 'test';
	const test_token = 'token';

	describe('DBAccountService', () => {
		beforeAll(async () => {
			await ApplicationDB.create(test_domain, InstanceType.Mastodon, 'key', 'secret');
		});

		afterAll(async () => {
			await ApplicationDB.delete(test_domain);
		});

		describe('store', () => {
			it('storing a valid account, should return true', async () => {
				expect(!!(await AccountDB.create(test_username, test_domain, test_token))).toBe(
					true,
				);
			});

			it('already exist should, return false', async () => {
				expect(!!(await AccountDB.create(test_username, test_domain, test_token))).toBe(
					false,
				);
			});

			it('domain not existing, should return false', async () => {
				expect(!!(await AccountDB.create(test_username, 'nothing', test_token))).toBe(
					false,
				);
			});
		});

		describe('update', () => {
			it('update token on a valid account, should return true', async () => {
				expect(!!(await AccountDB.update(test_username, test_domain, test_token))).toBe(
					true,
				);
			});

			it('account not existing, should return false', async () => {
				expect(!!(await AccountDB.update('nothing', test_domain, test_token))).toBe(false);
			});

			it('account not existing, should return false', async () => {
				expect(!!(await AccountDB.update(test_username, 'nothing', test_token))).toBe(
					false,
				);
			});

			it('invalid token, should return false', async () => {
				expect(!!(await AccountDB.update(test_username, test_domain, ''))).toBe(false);
			});
		});

		let uuid: string;
		describe('get', () => {
			it('getting a valid entry, should return true', async () => {
				const result = await AccountDB.get(test_username, test_domain);

				if (!result) {
					expect(!!result).toBe(true);
					return;
				}

				expect(result).toMatchObject({
					username: result.username,
					token: test_token,
					application: {
						domain: test_domain,
					},
				});

				uuid = result.uuid;
			});

			it('getting an invalid entry, should return null', async () => {
				expect(await AccountDB.get('nothing', test_domain)).toBe(null);
			});

			it('getting an invalid entry, should return null', async () => {
				expect(await AccountDB.get(test_username, 'nothing')).toBe(null);
			});

			it('getting an invalid entry, should return null', async () => {
				expect(await AccountDB.get('nothing', 'nothing')).toBe(null);
			});
		});

		describe('getByUUID', () => {
			it('getting a valid entry, should return true', async () => {
				const result = await AccountDB.getByUUID(uuid);

				if (!result) {
					expect(!!result).toBe(true);
					return;
				}

				expect(result).toMatchObject({
					username: result.username,
					token: test_token,
					application: {
						domain: test_domain,
					},
				});
			});

			it('getting an invalid entry, should return null', async () => {
				expect(await AccountDB.getByUUID('nothing')).toBe(null);
			});
		});

		describe('delete', () => {
			it('deleting a valid entry, should return true', async () => {
				expect(!!(await AccountDB.delete(test_username, test_domain))).toBe(true);
			});

			it('getting after delete, get should return null', async () => {
				expect(await AccountDB.get(test_username, test_domain)).toBe(null);
			});

			it('nothing to delete, should return false', async () => {
				expect(!!(await AccountDB.delete('nothing', test_domain))).toBe(false);
			});

			it('nothing to delete, should return false', async () => {
				expect(!!(await AccountDB.delete(test_username, 'nothing'))).toBe(false);
			});

			it('nothing to delete, should return false', async () => {
				expect(!!(await AccountDB.delete('nothing', 'nothing'))).toBe(false);
			});
		});
	});

	/**
	 * DBUserService
	 */
	let test_account: AccountStore;

	describe('DBUserService', () => {
		beforeAll(async () => {
			await ApplicationDB.create(test_domain, InstanceType.Mastodon, 'key', 'secret');
			test_account = (await AccountDB.create(test_username, test_domain, test_token))!;
		});

		afterAll(async () => {
			await UserDB.delete(test_account);
			await AccountDB.delete(test_account.username, test_account.application.domain);
			await ApplicationDB.delete(test_domain);
		});

		describe('store', () => {
			it('storing a valid user, should return true', async () => {
				expect(!!(await UserDB.create(test_account))).toBe(true);
			});

			it('already exist, should return false', async () => {
				expect(!!(await UserDB.create(test_account))).toBe(false);
			});
		});

		describe('addAccount', () => {
			it('add an account to a valid entry, should return true', async () => {
				expect(!!(await UserDB.addAccount(test_account, test_account))).toBe(true);
			});

			it('add an account which already exist, should return false', async () => {
				expect(!!(await UserDB.addAccount(test_account, test_account))).toBe(false);
			});

			it('add an valid account to a invalid entry, should return false', async () => {
				expect(
					!!(await UserDB.addAccount(
						{ ...test_account, uuid: randomUUID() },
						test_account,
					)),
				).toBe(false);
			});

			it('add an invalid account to a valid entry, should return false', async () => {
				expect(
					!!(await UserDB.addAccount(test_account, {
						...test_account,
						uuid: randomUUID(),
					})),
				).toBe(false);
			});
		});

		describe('get', () => {
			it('getting full valid entry, should return true', async () => {
				const result = await UserDB.get(test_account);

				expect(result).toMatchObject({
					main: result?.main,
					secondary: [result?.main],
				});
			});

			it('getting lite valid entry, should return true', async () => {
				const result = await UserDB.getMain(test_account);

				expect(result).toMatchObject({
					main: result?.main,
				});
			});

			it('getting an full invalid entry, should return null', async () => {
				expect(await UserDB.get({ ...test_account, uuid: randomUUID() })).toBe(null);
			});

			it('getting an lite invalid entry, should return null', async () => {
				expect(await UserDB.getMain({ ...test_account, uuid: randomUUID() })).toBe(null);
			});
		});

		describe('deleteAccount', () => {
			it('deleting an account in valid entry, should return true', async () => {
				expect(!!(await UserDB.deleteAccount(test_account, test_account))).toBe(true);
			});

			it('getting after delete, should return false', async () => {
				const result = await UserDB.get(test_account);
				expect(!!result?.secondary.length).toBe(false);
			});

			it('deleting an account in invalid entry, should return true', async () => {
				expect(
					!!(await UserDB.deleteAccount(
						{ ...test_account, uuid: randomUUID() },
						test_account,
					)),
				).toBe(false);
			});

			it('deleting an invalid account in valid entry, should return true', async () => {
				expect(
					!!(await UserDB.deleteAccount(test_account, {
						...test_account,
						uuid: randomUUID(),
					})),
				).toBe(false);
			});
		});

		describe('delete', () => {
			it('deleting a valid entry with no account used elsewhere, should return DeleteUser.RemoveAccount', async () => {
				expect(await UserDB.delete(test_account)).toBe(DeleteUser.RemoveAccount);
			});

			it('getting after delete, get should return null', async () => {
				expect(await UserDB.get(test_account)).toBe(null);
			});

			it('deleting a valid entry account used elsewhere, should return DeleteUser.True', async () => {
				const test_account_1 = await AccountDB.create(
					'test_username',
					test_domain,
					'test_token',
				);
				const test_account_2 = await AccountDB.create(
					'test_username1',
					test_domain,
					'test_token',
				);

				if (!test_account_1 || !test_account_2) {
					expect(!!test_account_1).toBe(true);
					expect(!!test_account_2).toBe(true);
					return;
				}

				expect(!!(await UserDB.create(test_account_1))).toBe(true);
				expect(!!(await UserDB.create(test_account_2))).toBe(true);
				expect(!!(await UserDB.addAccount(test_account_2, test_account_1))).toBe(true);

				expect(await UserDB.delete(test_account_1)).toBe(DeleteUser.True);
				expect(await UserDB.delete(test_account_2)).toBe(DeleteUser.RemoveAccount);

				expect(!!(await AccountDB.delete('test_username', test_domain))).toBe(true);
				expect(!!(await AccountDB.delete('test_username1', test_domain))).toBe(true);
			});

			it('deleting a invalid entry, should return DeleteUser.False', async () => {
				expect(await UserDB.delete({ ...test_account, uuid: randomUUID() })).toBe(
					DeleteUser.False,
				);
			});
		});
	});
});
