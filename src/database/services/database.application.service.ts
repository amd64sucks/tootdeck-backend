import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { WsService } from '../../websocket/ws.service';

import { ApplicationStore } from '../entities/application.entity';
import { AccountStore } from '../entities/account.entity';

import { Logger } from '../../utils/logger';

import { InstanceType } from '../types';
import { WebsocketAPI, ResponseType as WebsocketResponseType } from '../../websocket/types';

@Injectable()
export class DBApplicationService {
	private readonly logger = new Logger(DBApplicationService.name);

	constructor(
		@InjectRepository(ApplicationStore)
		private readonly ApplicationRepository: Repository<ApplicationStore>,
		@InjectRepository(AccountStore)
		private readonly AccountRepository: Repository<AccountStore>,
		private readonly WsService: WsService,
	) {}

	/**
	 * Utils
	 */
	//#region

	private async removeAllTokens(domain: string) {
		const found = await this.AccountRepository.find({
			where: { application: { domain } },
			relations: ['application'],
		}).catch((e) => {
			this.logger.error('removeAllTokens', `Unable to find accounts with ${domain}`, e);
			return null;
		});

		if (!found || !found.length) {
			return;
		}

		const promises: Promise<AccountStore>[] = [];
		for (let account of found) {
			promises.push(this.AccountRepository.save({ ...account, token: '' }));
		}

		await Promise.all(promises);

		if (process.env['NODE_ENV'] !== 'test') {
			// Dispatch info to all connected user with this application
			this.WsService.sendByApp(domain, {
				type: WebsocketResponseType.API,
				data: WebsocketAPI.ResponseData.AppExpired,
				domain,
			} as any);
		}
	}

	private async getApplication(
		args: string | ApplicationStore,
	): Promise<{ entry: ApplicationStore | null; domain: string }> {
		let domain: string = '';
		let entry: ApplicationStore | null = null;
		if (typeof args === 'string') {
			domain = args;
		} else if (args instanceof ApplicationStore) {
			entry = args;
		}
		if (!domain && !entry) {
			this.logger.error('getApplication', `No arguments`);
			return { entry, domain };
		}

		if (!entry) {
			entry = await this.get(domain);
			if (!entry) {
				this.logger.error('getApplication', `No entry to update for ${domain}`);
				return { entry, domain };
			}
		}

		return { entry, domain };
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	async get(domain: string) {
		return this.ApplicationRepository.findOne({
			where: {
				domain,
			},
		}).catch((e) => {
			this.logger.error('get', `Unable to find ${domain}`, e);
			return null;
		});
	}

	async create(domain: string, type: InstanceType, key: string, secret: string) {
		const now = new Date();
		const entity = this.ApplicationRepository.create({
			domain,
			key,
			secret,
			type,
			created_at: now,
			updated_at: now,
		});

		return this.ApplicationRepository.save(entity).catch((e) => {
			this.logger.error('create', `Unable to store ${domain} secrets`, e);
			return null;
		});
	}

	async updateLastUsage(args: string | ApplicationStore): Promise<ApplicationStore | null> {
		const { entry, domain } = await this.getApplication(args);
		if (!entry) {
			return null;
		}

		entry.updated_at = new Date();

		return this.ApplicationRepository.save(entry).catch((e) => {
			this.logger.error('updateCredentials', `Unable to update ${domain}`, e);
			return null;
		});
	}

	async updateCredentials(
		args: string | ApplicationStore,
		key: string,
		secret: string,
	): Promise<ApplicationStore | null> {
		const { entry, domain } = await this.getApplication(args);
		if (!entry) {
			return null;
		}

		entry.key = key;
		entry.secret = secret;
		entry.updated_at = new Date();
		await this.removeAllTokens(domain);

		return this.ApplicationRepository.save(entry).catch((e) => {
			this.logger.error('updateCredentials', `Unable to update ${domain}`, e);
			return null;
		});
	}

	async updateToken(
		args: string | ApplicationStore,
		token: string,
	): Promise<ApplicationStore | null> {
		const { entry, domain } = await this.getApplication(args);
		if (!entry) {
			return null;
		}

		entry.token = token;
		entry.updated_at = new Date();

		return this.ApplicationRepository.save(entry).catch((e) => {
			this.logger.error('updateToken', `Unable to update ${domain}`, e);
			return null;
		});
	}

	async updateStreamingURL(
		args: string | ApplicationStore,
		streaming_url: string,
	): Promise<ApplicationStore | null> {
		const { entry, domain } = await this.getApplication(args);
		if (!entry) {
			return null;
		}

		entry.streaming_url = streaming_url;
		entry.updated_at = new Date();

		return this.ApplicationRepository.save(entry).catch((e) => {
			this.logger.error('updateStreamingURL', `Unable to update ${domain}`, e);
			return null;
		});
	}

	async delete(domain: string) {
		const entry = await this.get(domain);
		if (!entry) {
			return null;
		}

		return this.ApplicationRepository.delete(entry.uuid).catch((e) => {
			this.logger.error('delete', `Unable to delete ${domain} secrets`, e);
			return null;
		});
	}
	//#endregion
}
