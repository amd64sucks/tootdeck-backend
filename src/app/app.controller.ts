import { Controller, Get, HttpCode, NotFoundException, UseGuards } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { readFileSync } from 'fs';

import { AccessGuard } from '../auth/guards/access.guard';

import { TimesService } from './services/times.service';
import { OptionalFeaturesService } from './services/optinalFeatures.service';

import { ApiResponseTime } from './properties/times.property';

@ApiTags('Other')
@Controller()
export class AppController {
	private readonly VERSION: string | undefined;

	constructor(
		private readonly TimesService: TimesService,
		private readonly OptionalFeaturesService: OptionalFeaturesService,
	) {
		try {
			this.VERSION = readFileSync('version', { encoding: 'utf8' });
		} catch (e) {}
	}

	@HttpCode(200)
	@Get('ping')
	ping(): string {
		return 'PONG';
	}

	@HttpCode(200)
	@Get('version')
	version(): string {
		if (!this.VERSION) {
			throw new NotFoundException();
		}
		return this.VERSION;
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200, description: 'API response time', type: ApiResponseTime })
	@HttpCode(200)
	@Get('stats')
	stats() {
		return this.TimesService.getTimes();
	}

	@UseGuards(AccessGuard)
	@ApiResponse({
		status: 200,
		description: 'Show avaliable optional features',
		type: String,
		isArray: true,
	})
	@HttpCode(200)
	@Get('optional_features')
	features() {
		return this.OptionalFeaturesService.get();
	}
}
