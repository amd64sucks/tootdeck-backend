import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { EntityClassOrSchema } from '@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

import { AccountStore } from '../../database/entities/account.entity';
import { ApplicationStore } from '../../database/entities/application.entity';
import { UserStore } from '../../database/entities/user.entity';
import { ConfigStore } from '../../database/entities/config.entity';

export function typeormConfig(entities?: EntityClassOrSchema[]) {
	const features = [ApplicationStore, AccountStore, UserStore, ConfigStore];

	return [
		TypeOrmModule.forRootAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: (configService: ConfigService): any =>
				({
					type: 'postgres',
					host: configService.get<string>('PSQL_HOST'),
					port: configService.get<number>('PSQL_PORT'),
					username: configService.get<string>('PSQL_USERNAME'),
					password: configService.get<string>('PSQL_PASSWORD'),
					database: configService.get<string>('PSQL_DATABASE'),
					entities: ['dist/**/*.entity{.ts,.js}'],
					namingStrategy: new SnakeNamingStrategy(),
					autoLoadEntities: true,
					synchronize: false,
					logging: false,
				}) as TypeOrmModuleOptions,
		}),
		TypeOrmModule.forFeature(entities ?? features),
	];
}
