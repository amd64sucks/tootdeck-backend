import { RedisModule } from '@liaoliaots/nestjs-redis';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { RedisDB, RedisNamespace } from '../redis.config';

export function RedisConfig(db?: RedisDB[]) {
	let config: {
		namespace: RedisNamespace;
		db: RedisDB;
	}[] = [];

	if (db) {
		for (const index of db) {
			switch (index) {
				case RedisDB.AccessToken:
					config.push({
						namespace: RedisNamespace.AccessToken,
						db: RedisDB.AccessToken,
					});
					break;
				case RedisDB.RefreshToken:
					config.push({
						namespace: RedisNamespace.RefreshToken,
						db: RedisDB.RefreshToken,
					});
					break;
				case RedisDB.OAuthToken:
					config.push({
						namespace: RedisNamespace.OAuthToken,
						db: RedisDB.OAuthToken,
					});
					break;
				case RedisDB.OpenSession:
					config.push(
						{
							namespace: RedisNamespace.OpenSession,
							db: RedisDB.OpenSession,
						},
						{
							namespace: RedisNamespace.OpenSessionSubscribe,
							db: RedisDB.OpenSession,
						},
					);
					break;
				case RedisDB.UserDelete:
					config.push({
						namespace: RedisNamespace.UserDelete,
						db: RedisDB.UserDelete,
					});
					break;
				case RedisDB.UserCache:
					config.push({
						namespace: RedisNamespace.UserCache,
						db: RedisDB.UserCache,
					});
					break;
				case RedisDB.AvatarCache:
					config.push({
						namespace: RedisNamespace.AvatarCache,
						db: RedisDB.AvatarCache,
					});
					break;
				case RedisDB.StatusesInteractions:
					config.push({
						namespace: RedisNamespace.StatusesInteractions,
						db: RedisDB.StatusesInteractions,
					});
					break;
				case RedisDB.ExcludedDomain:
					config.push({
						namespace: RedisNamespace.ExcludedDomain,
						db: RedisDB.ExcludedDomain,
					});
					break;
			}
		}
	} else {
		config = [
			{
				namespace: RedisNamespace.AccessToken,
				db: RedisDB.AccessToken,
			},
			{
				namespace: RedisNamespace.RefreshToken,
				db: RedisDB.RefreshToken,
			},
			{
				namespace: RedisNamespace.OAuthToken,
				db: RedisDB.OAuthToken,
			},
			{
				namespace: RedisNamespace.OpenSession,
				db: RedisDB.OpenSession,
			},
			{
				namespace: RedisNamespace.OpenSessionSubscribe,
				db: RedisDB.OpenSession,
			},
			{
				namespace: RedisNamespace.UserDelete,
				db: RedisDB.UserDelete,
			},
			{
				namespace: RedisNamespace.UserCache,
				db: RedisDB.UserCache,
			},
			{
				namespace: RedisNamespace.AvatarCache,
				db: RedisDB.AvatarCache,
			},
			{
				namespace: RedisNamespace.StatusesInteractions,
				db: RedisDB.StatusesInteractions,
			},
			{
				namespace: RedisNamespace.ExcludedDomain,
				db: RedisDB.ExcludedDomain,
			},
		];
	}

	return RedisModule.forRootAsync(
		{
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: (configService: ConfigService): any => ({
				commonOptions: {
					host: configService.get<string>('REDIS_HOST'),
					port: configService.get<number>('REDIS_PORT'),
				},
				config,
			}),
		},
		true,
	);
}
