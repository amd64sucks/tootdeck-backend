import { ApiProperty } from '@nestjs/swagger';

export class ApiResponseTime {
	@ApiProperty()
	count: number;

	@ApiProperty()
	average: number;

	@ApiProperty()
	last: number;
}
