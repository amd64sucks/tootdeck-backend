import { Injectable, NestMiddleware } from '@nestjs/common';
import * as responseTime from 'response-time';

import { TimesService } from '../services/times.service';

@Injectable()
export class ResponseTimeMiddleware implements NestMiddleware {
	constructor(private readonly TimesService: TimesService) {}

	use(req: any, res: any, next: any) {
		// Set times in request object for access in controller
		if (req.originalUrl === '/api/stats') {
			next();
			return;
		}

		responseTime((req, res, time) => {
			this.TimesService.times.push(time);
			this.TimesService.nbr++;

			if (this.TimesService.times.length > 1000) {
				this.TimesService.times.shift();
			}

			const instance = res.getHeader('X-Instance-Time');
			if (instance) {
				const instance_time = +instance;
				if (!isNaN(instance_time)) {
					res.setHeader('X-Instance-Time', instance_time.toFixed(2) + 'ms');
					res.setHeader('X-Mirror-Time', time.toFixed(2) + 'ms');
					res.setHeader(
						'X-Mirror-Added-Latency',
						Math.abs(instance_time - time).toFixed(2) + 'ms',
					);
				} else {
					res.setHeader('X-Response-Time', time.toFixed(2) + 'ms');
				}
			} else {
				res.setHeader('X-Response-Time', time.toFixed(2) + 'ms');
			}
		})(req, res, next);
	}
}
