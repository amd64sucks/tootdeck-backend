import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';

import { AppService } from './services/app.service';

describe('App', () => {
	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [
				ConfigModule.forRoot({
					isGlobal: true,
				}),
			],
		}).compile();
	});

	it('AppService', () => {
		const domain = process.env['DOMAIN'];
		process.env['DOMAIN'] = '';
		new AppService(new ConfigService());
		process.env['DOMAIN'] = domain;
	});
});
