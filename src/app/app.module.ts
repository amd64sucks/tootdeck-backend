import { MiddlewareConsumer, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { HttpModule } from '@nestjs/axios';

import { ResponseTimeMiddleware } from './middlewares/times.middleware';

import { InjectorInterceptor } from '../mirror/interceptors/injector.interceptor';

import { AppController } from './app.controller';
import { AuthController } from '../auth/controllers/auth.controller';
import { OAuthController } from '../auth/controllers/oauth.controller';
import { AuthSessionsController } from '../auth/controllers/auth.sessions.controller';
import { UserController } from '../user/user.controller';
import { MirrorAccountsController } from '../mirror/controllers/accounts/accounts.controller';
import { MirrorAccountsOthersController } from '../mirror/controllers/accounts/accounts.others.controller';
import { MirrorAccountsTagsController } from '../mirror/controllers/accounts/accounts.tags.controller';
import { MirrorAccountsDomainBlocksController } from '../mirror/controllers/accounts/accounts.domainblocks.controller';
import { MirrorAccountsFiltersController } from '../mirror/controllers/accounts/accounts.filters.controller';
import { MirrorAccountsFollowRequestController } from '../mirror/controllers/accounts/accounts.follow_requests.controller';
import { MirrorTimelinesController } from '../mirror/controllers/timelines/timelines.controller';
import { MirrorTimelinesConversationsController } from '../mirror/controllers/timelines/timelines.conversations.controller';
import { MirrorTimelinesMarkersController } from '../mirror/controllers/timelines/timelines.markers.controller';
import { MirrorNotificationsController } from '../mirror/controllers/notifications.contoller';
import { MirrorInstanceController } from '../mirror/controllers/instance/instance.controller';
import { MirrorInstanceTrendsController } from '../mirror/controllers/instance/instance.trends.controller';
import { MirrorInstanceDirectoryController } from '../mirror/controllers/instance/instance.directory.controller';
import { MirrorInstanceEmojisController } from '../mirror/controllers/instance/instance.emojis.controller';
import { MirrorInstanceAnnouncementsController } from '../mirror/controllers/instance/instance.announcements.controller';
import { MirrorStatusesController } from '../mirror/controllers/statuses/statuses.controller';
import { MirrorStatusesMediaController } from '../mirror/controllers/statuses/statuses.media.controller';
import { MirrorStatusesPollsController } from '../mirror/controllers/statuses/statuses.poll.controller';
import { MirrorStatusesScheduledController } from '../mirror/controllers/statuses/statuses.scheduled.controller';
import { MirrorTimelinesListsController } from '../mirror/controllers/timelines/timelines.lists.controller';
import { MirrorSearchController } from '../mirror/controllers/search.contoller';
import { MirrorInjectorController } from '../mirror/controllers/injector.controller';

import { AppService } from './services/app.service';
import { OptionalFeaturesService } from './services/optinalFeatures.service';
import { TimesService } from './services/times.service';
import { DBApplicationService } from '../database/services/database.application.service';
import { DBAccountService } from '../database/services/database.account.service';
import { DBUserService } from '../database/services/database.user.service';
import { OAuthService } from '../auth/services/oauth.service';
import { UserDeleteService } from '../user/services/user.delete.service';
import { JWEService } from '../auth/services/jwe.service';
import { OAuthSessionService } from '../auth/services/sessions/oauth.session.service';
import { JWESessionService } from '../auth/services/sessions/jwe.session.service';
import { OpenSessionService } from '../auth/services/sessions/open.session.service';
import { UserDeleteSessionService } from '../auth/services/sessions/user.delete.session.service';
import { CookieService } from '../auth/services/utils/cookie.service';
import { BrowserFingerprintService } from '../auth/services/utils/fingerprint.service';
import { UserService } from '../user/services/user.service';
import { UserCacheService } from '../user/services/user.cache.service';
import { ApplicationTokenService } from '../mirror/services/applicationToken.service';
import { AvatarCacheService } from '../mirror/services/avatar.service';
import { InteractionsService } from '../mirror/services/Interactions.service';
import { WsService } from '../websocket/ws.service';

import { WsGateway } from '../websocket/ws.gateway';

import { RedisConfig } from './modules/redis.config';
import { typeormConfig } from './modules/typeorm.config';

export const FullAppModule = {
	imports: [
		ConfigModule.forRoot({ isGlobal: true }),
		JwtModule,
		RedisConfig(),
		...typeormConfig(),
		HttpModule,
	],
	controllers: [
		AppController,
		OAuthController,
		AuthController,
		AuthSessionsController,
		UserController,
		MirrorAccountsController,
		MirrorAccountsOthersController,
		MirrorAccountsTagsController,
		MirrorAccountsDomainBlocksController,
		MirrorAccountsFiltersController,
		MirrorAccountsFollowRequestController,
		MirrorTimelinesController,
		MirrorTimelinesConversationsController,
		MirrorTimelinesListsController,
		MirrorTimelinesMarkersController,
		MirrorNotificationsController,
		MirrorInstanceController,
		MirrorInstanceTrendsController,
		MirrorInstanceDirectoryController,
		MirrorInstanceEmojisController,
		MirrorInstanceAnnouncementsController,
		MirrorStatusesController,
		MirrorStatusesScheduledController,
		MirrorStatusesMediaController,
		MirrorStatusesPollsController,
		MirrorSearchController,
		MirrorInjectorController,
	],
	providers: [
		AppService,
		OptionalFeaturesService,
		TimesService,
		DBApplicationService,
		DBAccountService,
		DBUserService,
		OAuthService,
		OAuthSessionService,
		UserService,
		CookieService,
		BrowserFingerprintService,
		JWESessionService,
		JWEService,
		OpenSessionService,
		UserDeleteSessionService,
		UserDeleteService,
		UserCacheService,
		ApplicationTokenService,
		AvatarCacheService,
		InteractionsService,
		InjectorInterceptor,
		WsGateway,
		WsService,
	],
};
@Module(FullAppModule)
export class AppModule {
	constructor(private readonly config: ConfigService) {}

	configure(consumer: MiddlewareConsumer) {
		consumer.apply(ResponseTimeMiddleware).exclude('/ping').forRoutes('*');
	}
}
