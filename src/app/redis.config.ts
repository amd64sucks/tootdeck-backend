export enum RedisNamespace {
	AccessToken = 'access_token_session',
	RefreshToken = 'refresh_token_session',
	OAuthToken = 'oauth_token_session',
	OpenSession = 'open_session',
	OpenSessionSubscribe = 'open_session_subscribe',
	UserDelete = 'user_delete_session',
	UserCache = 'user_cache',
	AvatarCache = 'avatar_cache',
	StatusesInteractions = 'statuses_interactions',
	ExcludedDomain = 'excluded_domain',
}

export enum RedisDB {
	AccessToken = 0,
	RefreshToken = 1,
	OAuthToken = 2,
	OpenSession = 3,
	UserDelete = 4,
	UserCache = 5,
	AvatarCache = 6,
	StatusesInteractions = 7,
	ExcludedDomain = 8,
}
