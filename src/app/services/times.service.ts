import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { Logger } from '../../utils/logger';

export interface ResponseTime {
	count: number;
	average: number;
	last: number;
}

@Injectable()
export class TimesService {
	private readonly logger = new Logger(TimesService.name);

	times: number[] = [];
	nbr: number = 0;

	constructor(private readonly configService: ConfigService) {
		if (!configService.get<string>('DOMAIN')) {
			this.logger.error('constructor', `No domain in environment variables.`);
			if (configService.get<string>('NODE_ENV') !== 'test') {
				process.exit(1);
			}
		}
	}

	calcAverage() {
		let total = 0;
		let length = this.times.length;
		for (const value of this.times) {
			total += value;
		}
		return total / length;
	}

	getTimes(): ResponseTime {
		return {
			count: this.nbr,
			average: this.calcAverage() || -1,
			last: this.times[this.times.length - 1] || -1,
		};
	}
}
