import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { Logger } from '../../utils/logger';

@Injectable()
export class AppService {
	private readonly logger = new Logger(AppService.name);

	constructor(private readonly configService: ConfigService) {
		if (!configService.get<string>('DOMAIN')) {
			this.logger.error('constructor', `No domain in environment variables.`);
			if (configService.get<string>('NODE_ENV') !== 'test') {
				process.exit(1);
			}
		}
	}
}
