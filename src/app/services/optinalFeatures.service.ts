import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class OptionalFeaturesService {
	readonly CACHE_AVATAR: boolean;
	readonly CACHE_STATUS_INTERACTIONS: boolean;

	constructor(private readonly configService: ConfigService) {
		if (this.configService.get<string>('CACHE_AVATAR') === 'true') {
			this.CACHE_AVATAR = true;
		} else {
			this.CACHE_AVATAR = false;
		}

		if (this.configService.get<string>('CACHE_STATUS_INTERACTIONS') === 'true') {
			this.CACHE_STATUS_INTERACTIONS = true;
		} else {
			this.CACHE_STATUS_INTERACTIONS = false;
		}
	}

	get(): string[] {
		const ret = [];

		if (this.CACHE_AVATAR) {
			ret.push('CACHE_AVATAR');
		}
		if (this.CACHE_STATUS_INTERACTIONS) {
			ret.push('CACHE_STATUS_INTERACTIONS');
		}

		return ret;
	}
}
