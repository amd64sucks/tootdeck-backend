#!/bin/bash
status=0

docker compose -f docker-compose.test.yml build

docker compose -f docker-compose.test.yml run --rm backend bash -c "pnpm run typeorm:clean && pnpm run typeorm:generate || exit 0 && pnpm run typeorm:migrate"
docker compose -f docker-compose.test.yml run --rm backend pnpm run typeorm:clean
if [[ "$1" == "init" ]]; then
	exit 0
fi

if [[ "$1" == "coverage" ]]; then
	docker compose -f docker-compose.test.yml run --rm backend pnpm run test:cov || status=1
else
	docker compose -f docker-compose.test.yml run --rm backend pnpm run test $@ || status=1
fi
docker compose -f docker-compose.test.yml down

exit $status
