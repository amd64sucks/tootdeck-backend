#!/bin/bash
filename="package.json"
if [ ! -f "$filename" ]; then
	cd backend
fi

` \
pnpm run typeorm:clean && \
pnpm run typeorm:generate 2> /dev/null || exit 0 && \
pnpm run typeorm:migrate
`

exec "$@"
