const Sequencer = require('@jest/test-sequencer').default;

class CustomSequencer extends Sequencer {
	sort(tests) {
		const ordered = []

		const secret = tests.find(x => x.path.includes("secret.spec.ts"))
		if (secret) {
			ordered.push(secret)
		}

		const database = tests.find(x => x.path.includes("database.services.spec.ts"))
		if (database) {
			ordered.push(database)
		}

		tests = tests.filter(x => !x.path.includes("secret.spec.ts") && !x.path.includes("database.services.spec.ts"))

		return [...ordered, ...tests]
	}
}

module.exports = CustomSequencer;
