import type { Config } from 'jest';

const config: Config = {
	rootDir: '../src',
	testSequencer: '../test/jest.sequencer.js',
	moduleFileExtensions: ['js', 'json', 'ts'],
	testRegex: '.*\\.spec\\.ts$',
	transform: {
		'^.+\\.ts$': 'ts-jest',
	},
	collectCoverageFrom: ['**/*.ts', '!**/main.ts'],
	coveragePathIgnorePatterns: [
		'^.*.module.ts?$',
		'^.*.types.ts?$',
		'^.*.property.ts?$',
		'^.*.logger.ts?$',
		'^.*.config.ts?$',
		'^.*.controller.ts?$',
		'^.*.gateway.ts?$',
		'^.*.guard.ts?$',
		'^.*.middleware.ts?$',
		'mirror',
	],
	coverageDirectory: '../coverage',
	testEnvironment: 'node',
};

export default config;
